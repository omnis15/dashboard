<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['scaffolding_trigger'] = 'scaffolding';
|
| This route lets you set a "secret" word that will trigger the
| scaffolding feature for added security. Note: Scaffolding must be
| enabled in the controller in which you intend to use it.   The reserved 
| routes must come before any wildcard or regular expression routes.
|
*/

$route['default_controller'] = "auth";
$route['404_override'] = '';

$route['home'] = 'site/home_page';
/*
*	Auth Routes
*/
$route['login'] = 'auth/login_user';
$route['logout-admin'] = 'auth/logout';

/*
*	Admin Routes
*/
$route['dashboard'] = 'admin/dashboard';
$route['change-password'] = 'admin/users/change_password';

/*
*	administration Routes
*/
$route['administration/configuration'] = 'admin/configuration';
$route['administration/edit-configuration/(:num)'] = 'admin/edit_configuration/$1';
$route['administration/sections'] = 'admin/sections/index';
$route['administration/sections/(:any)/(:any)'] = 'admin/sections/index/$1/$2';
$route['administration/sections/(:any)/(:any)/(:num)'] = 'admin/sections/index/$1/$2/$3';
$route['administration/add-section'] = 'admin/sections/add_section';
$route['administration/edit-section/(:num)'] = 'admin/sections/edit_section/$1';
$route['administration/edit-section/(:num)/(:num)'] = 'admin/sections/edit_section/$1/$2';
$route['administration/delete-section/(:num)'] = 'admin/sections/delete_section/$1';
$route['administration/delete-section/(:num)/(:num)'] = 'admin/sections/delete_section/$1/$2';
$route['administration/activate-section/(:num)'] = 'admin/sections/activate_section/$1';
$route['administration/activate-section/(:num)/(:num)'] = 'admin/sections/activate_section/$1/$2';
$route['administration/deactivate-section/(:num)'] = 'admin/sections/deactivate_section/$1';
$route['administration/deactivate-section/(:num)/(:num)'] = 'admin/sections/deactivate_section/$1/$2';

#$route['administration/company-profile'] = 'admin/contacts/show_contacts';
$route['administration/branches'] = 'admin/branches/index';
$route['administration/tutorial']='admin/tutorial/index';
$route['administration/branches/(:any)/(:any)/(:num)'] = 'admin/branches/index/$1/$2/$3';
$route['administration/branches/(:any)/(:any)'] = 'admin/branches/index/$1/$2';
$route['administration/add-branch'] = 'admin/branches/add_branch';
$route['administration/edit-branch/(:num)'] = 'admin/branches/edit_branch/$1';
$route['administration/edit-branch/(:num)/(:num)'] = 'admin/branches/edit_branch/$1/$2';
$route['administration/delete-branch/(:num)'] = 'admin/branches/delete_branch/$1';
$route['administration/delete-branch/(:num)/(:num)'] = 'admin/branches/delete_branch/$1/$2';
$route['administration/activate-branch/(:num)'] = 'admin/branches/activate_branch/$1';
$route['administration/activate-branch/(:num)/(:num)'] = 'admin/branches/activate_branch/$1/$2';
$route['administration/deactivate-branch/(:num)'] = 'admin/branches/deactivate_branch/$1';
$route['administration/deactivate-branch/(:num)/(:num)'] = 'admin/branches/deactivate_branch/$1/$2';

//tutorial routes
$route['tutorial/human-resource']='hr/tutorial/view_tutorials';
$route['tutorial/administration/add-human-resource']='admin/tutorial/upload_tutorial';
$route['administration/activate-tutorial/(:num)'] = 'admin/tutorial/activate_tutorial/$1';
$route['administration/deactivate-tutorial/(:num)'] = 'admin/tutorial/deactivate_tutorial/$1';
$route['administration/edit-tutorial/(:num)'] = 'admin/tutorial/edit_tutorial/$1';
$route['administration/delete-tutorial/(:num)'] = 'admin/tutorial/delete_tutorial/$1';
$route['tutorial/payroll']='hr/tutorial/payroll';
$route['tutorial/leave']='hr/tutorial/leave';


/*
*	HR Routes
*/
$route['human-resource/schedules'] = 'hr/schedules/index';
$route['human-resource/delete-schedule/(:num)'] = 'hr/schedules/delete_schedule/$1';
$route['human-resource/delete-schedule/(:num)/(:num)'] = 'hr/schedules/delete_schedule/$1/$2';
$route['human-resource/activate-schedule/(:num)'] = 'hr/schedules/activate_schedule/$1';
$route['human-resource/activate-schedule/(:num)/(:num)'] = 'hr/schedules/activate_schedule/$1/$2';
$route['human-resource/deactivate-schedule/(:num)'] = 'hr/schedules/deactivate_schedule/$1';
$route['human-resource/deactivate-schedule/(:num)/(:num)'] = 'hr/schedules/deactivate_schedule/$1/$2';
$route['human-resource/schedule-personnel/(:num)'] = 'hr/schedules/schedule_personnel/$1';
$route['human-resource/fill-timesheet/(:num)/(:num)'] = 'hr/schedules/fill_timesheet/$1/$2';
$route['human-resource/doctors-schedule'] = 'hr/schedules/doctors_schedule';
$route['human-resource/schedule-personnel/(:num)/(:any)/(:any)/(:num)'] = 'hr/schedules/schedule_personnel/$1/$2/$3/$4';
$route['human-resource/schedule-personnel/(:num)/(:any)/(:any)'] = 'hr/schedules/schedule_personnel/$1/$2/$3';
$route['human-resource/schedules/(:any)/(:any)/(:num)'] = 'hr/schedules/index/$1/$2/$3';
$route['human-resource/schedules/(:any)/(:any)'] = 'hr/schedules/index/$1/$2';

$route['human-resource/my-account'] = 'admin/dashboard';
$route['human-resource/my-account/edit-about/(:num)'] = 'hr/personnel/my_account/update_personnel_about_details/$1';
$route['human-resource/edit-personnel-account/(:num)'] = 'hr/personnel/update_personnel_account_details/$1';
$route['human-resource/configuration'] = 'hr/configuration';
$route['human-resource/add-job-title'] = 'hr/add_job_title';
$route['human-resource/edit-job-title/(:num)'] = 'hr/edit_job_title/$1';
$route['human-resource/delete-job-title/(:num)'] = 'hr/delete_job_title/$1';
$route['human-resource/personnel'] = 'hr/personnel/index';
$route['human-resource/personnel/(:any)/(:any)/(:num)'] = 'hr/personnel/index/$1/$2/$3';
$route['human-resource/add-personnel'] = 'hr/personnel/add_personnel';
$route['human-resource/edit-personnel/(:num)'] = 'hr/personnel/edit_personnel/$1';
$route['human-resource/edit-store-authorize/(:num)'] = 'hr/personnel/edit_store_authorize/$1';
$route['human-resource/edit-order-authorize/(:num)'] = 'hr/personnel/edit_order_authorize/$1';

$route['human-resource/edit-personnel-about/(:num)'] = 'hr/personnel/update_personnel_about_details/$1';
$route['human-resource/edit-personnel-account/(:num)'] = 'hr/personnel/update_personnel_account_details/$1';
$route['human-resource/edit-personnel/(:num)/(:num)'] = 'hr/personnel/edit_personnel/$1/$2';
$route['human-resource/delete-personnel/(:num)'] = 'hr/personnel/delete_personnel/$1';
$route['human-resource/delete-personnel/(:num)/(:num)'] = 'hr/personnel/delete_personnel/$1/$2';
$route['human-resource/activate-personnel/(:num)'] = 'hr/personnel/activate_personnel/$1';
$route['human-resource/activate-personnel/(:num)/(:num)'] = 'hr/personnel/activate_personnel/$1/$2';
$route['human-resource/deactivate-personnel/(:num)'] = 'hr/personnel/deactivate_personnel/$1';
$route['human-resource/deactivate-personnel/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel/$1/$2';
$route['human-resource/reset-password/(:num)'] = 'hr/personnel/reset_password/$1';
$route['human-resource/update-personnel-roles/(:num)'] = 'hr/personnel/update_personnel_roles/$1';
$route['human-resource/add-emergency-contact/(:num)'] = 'hr/personnel/add_emergency_contact/$1';
$route['human-resource/activate-emergency-contact/(:num)/(:num)'] = 'hr/personnel/activate_emergency_contact/$1/$2';
$route['human-resource/deactivate-emergency-contact/(:num)/(:num)'] = 'hr/personnel/deactivate_emergency_contact/$1/$2';
$route['human-resource/delete-emergency-contact/(:num)/(:num)'] = 'hr/personnel/delete_emergency_contact/$1/$2';

$route['human-resource/add-dependant-contact/(:num)'] = 'hr/personnel/add_dependant_contact/$1';
$route['human-resource/activate-dependant-contact/(:num)/(:num)'] = 'hr/personnel/activate_dependant_contact/$1/$2';
$route['human-resource/deactivate-dependant-contact/(:num)/(:num)'] = 'hr/personnel/deactivate_dependant_contact/$1/$2';
$route['human-resource/delete-dependant-contact/(:num)/(:num)'] = 'hr/personnel/delete_dependant_contact/$1/$2';

$route['human-resource/add-personnel-job/(:num)'] = 'hr/personnel/add_personnel_job/$1';
$route['human-resource/activate-personnel-job/(:num)/(:num)'] = 'hr/personnel/activate_personnel_job/$1/$2';
$route['human-resource/deactivate-personnel-job/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel_job/$1/$2';
$route['human-resource/delete-personnel-job/(:num)/(:num)'] = 'hr/personnel/delete_personnel_job/$1/$2';

$route['human-resource/leave'] = 'hr/leave/calender';
$route['human-resource/leave/(:any)/(:any)'] = 'hr/leave/calender/$1/$2';
$route['human-resource/view-leave/(:any)'] = 'hr/leave/view_leave/$1';
$route['human-resource/add-personnel-leave/(:num)'] = 'hr/personnel/add_personnel_leave/$1';
$route['human-resource/add-leave/(:any)'] = 'hr/leave/add_leave/$1';
$route['human-resource/add-calender-leave'] = 'hr/leave/add_calender_leave';
$route['human-resource/activate-leave/(:num)/(:any)'] = 'hr/leave/activate_leave/$1/$2';
$route['human-resource/deactivate-leave/(:num)/(:any)'] = 'hr/leave/deactivate_leave/$1/$2';
$route['human-resource/delete-leave/(:num)/(:any)'] = 'hr/leave/delete_leave/$1/$2';
$route['human-resource/activate-personnel-leave/(:num)/(:num)'] = 'hr/personnel/activate_personnel_leave/$1/$2';
$route['human-resource/deactivate-personnel-leave/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel_leave/$1/$2';
$route['human-resource/delete-personnel-leave/(:num)/(:num)'] = 'hr/personnel/delete_personnel_leave/$1/$2';

$route['human-resource/delete-personnel-role/(:num)/(:num)'] = 'hr/personnel/delete_personnel_role/$1/$2';
$route['human-resource/delete-personnel-approvals_assigned/(:num)/(:num)'] = 'hr/personnel/delete_personnel_approvals_assigned/$1/$2';

/*
*	Accounts Routes
*/
$route['accounts/customers/cust-invoice/(:num)'] = 'accounts/customers/cust_invoice/$1';
$route['accounts/customers/cust-payment/(:num)'] = 'accounts/customers/cust_payment/$1';
$route['accounts/customers/cust-view-ride/(:num)/(:num)'] = 'accounts/customers/view_cust_rides/$1/$2';
$route['accounts/calculate-paye/(:num)'] = 'accounts/payroll/calculate_personnel_paye/$1';
$route['accounts/creditors'] = 'accounts/creditors/index';
$route['accounts/hospital-accounts'] = 'accounts/hospital_accounts/index';
$route['accounts/petty-cash'] = 'accounts/petty_cash/index';
$route['accounts/petty-cash/(:any)/(:any)'] = 'accounts/petty_cash/index/$1/$2';
$route['accounts/petty-cash/(:any)'] = 'accounts/petty_cash/index/$1';
$route['accounts/change-branch'] = 'accounts/payroll/change_branch';
$route['accounts/print-paye-report/(:num)'] = 'accounts/payroll/print_paye_report/$1';
$route['accounts/print-nhif-report/(:num)'] = 'accounts/payroll/print_nhif_report/$1';
$route['accounts/print-nssf-report/(:num)'] = 'accounts/payroll/print_nssf_report/$1';
$route['accounts/print-payroll/(:num)'] = 'accounts/payroll/print_payroll/$1';
$route['accounts/print-month-payslips/(:num)'] = 'accounts/payroll/print_monthly_payslips/$1';
$route['accounts/export-payroll/(:num)'] = 'accounts/payroll/export_payroll/$1';
$route['accounts/print-payroll-pdf/(:num)'] = 'accounts/payroll/print_payroll_pdf/$1';
$route['accounts/payroll/print-payslip/(:num)/(:num)'] = 'accounts/payroll/print_payslip/$1/$2';
$route['accounts/payroll/download-payslip/(:num)/(:num)'] = 'accounts/payroll/download_payslip/$1/$2';
$route['accounts/payroll-payslips/(:num)'] = 'accounts/payroll/payroll_payslips/$1';
$route['accounts/salary-data'] = 'accounts/payroll/salaries';
$route['accounts/search-payroll'] = 'accounts/payroll/search_payroll';
$route['accounts/close-payroll-search'] = 'accounts/payroll/close_payroll_search';
$route['accounts/create-payroll'] = 'accounts/payroll/create_payroll';
$route['accounts/deactivate-payroll/(:num)'] = 'accounts/payroll/deactivate_payroll/$1';
$route['accounts/print-payslips'] = 'accounts/payroll/print_payslips';
$route['accounts/payroll/edit-payment-details/(:num)'] = 'accounts/payroll/edit_payment_details/$1';
$route['accounts/payroll/edit_allowance/(:num)'] = 'accounts/payroll/edit_allowance/$1';
$route['accounts/payroll/delete_allowance/(:num)'] = 'accounts/payroll/delete_allowance/$1';
$route['accounts/payroll/edit_deduction/(:num)'] = 'accounts/payroll/edit_deduction/$1';
$route['accounts/payroll/delete_deduction/(:num)'] = 'accounts/payroll/delete_deduction/$1';
$route['accounts/payroll/edit_saving/(:num)'] = 'accounts/payroll/edit_saving/$1';
$route['accounts/payroll/delete_saving/(:num)'] = 'accounts/payroll/delete_saving/$1';
$route['accounts/payroll/edit_loan_scheme/(:num)'] = 'accounts/payroll/edit_loan_scheme/$1';
$route['accounts/payroll/delete_loan_scheme/(:num)'] = 'accounts/payroll/delete_loan_scheme/$1';
$route['accounts/payroll'] = 'accounts/payroll/payrolls';
$route['accounts/all-payroll'] = 'accounts/payroll/all_payrolls';
$route['accounts/payment-details/(:num)'] = 'accounts/payroll/payment_details/$1';
$route['accounts/save-payment-details/(:num)'] = 'accounts/payroll/save_payment_details/$1';
$route['accounts/update-savings/(:num)'] = 'accounts/payroll/update_savings/$1';
$route['accounts/update-loan-schemes/(:num)'] = 'accounts/payroll/update_loan_schemes/$1';
$route['payroll/configuration'] = 'accounts/payroll/payroll_configuration';
$route['accounts/payroll-configuration'] = 'accounts/payroll/payroll_configuration';
$route['accounts/payroll/edit-nssf/(:num)'] = 'accounts/payroll/edit_nssf/$1';
$route['accounts/payroll/edit-nhif/(:num)'] = 'accounts/payroll/edit_nhif/$1';
$route['accounts/payroll/delete-nhif/(:num)'] = 'accounts/payroll/delete_nhif/$1';
$route['accounts/payroll/edit-paye/(:num)'] = 'accounts/payroll/edit_paye/$1';
$route['accounts/payroll/delete-paye/(:num)'] = 'accounts/payroll/delete_paye/$1';
$route['accounts/payroll/edit-payment/(:num)'] = 'accounts/payroll/edit_payment/$1';
$route['accounts/payroll/delete-payment/(:num)'] = 'accounts/payroll/delete_payment/$1';
$route['accounts/payroll/edit-benefit/(:num)'] = 'accounts/payroll/edit_benefit/$1';
$route['accounts/payroll/delete-benefit/(:num)'] = 'accounts/payroll/delete_benefit/$1';
$route['accounts/payroll/edit-allowance/(:num)'] = 'accounts/payroll/edit_allowance/$1';
$route['accounts/payroll/delete-allowance/(:num)'] = 'accounts/payroll/delete_allowance/$1';
$route['accounts/payroll/edit-deduction/(:num)'] = 'accounts/payroll/edit_deduction/$1';
$route['accounts/payroll/edit-relief/(:num)'] = 'accounts/payroll/edit_relief/$1';
$route['accounts/payroll/delete-deduction/(:num)'] = 'accounts/payroll/delete_deduction/$1';
$route['accounts/payroll/edit-other-deduction/(:num)'] = 'accounts/payroll/edit_other_deduction/$1';
$route['accounts/payroll/delete-other-deduction/(:num)'] = 'accounts/payroll/delete_other_deduction/$1';
$route['accounts/payroll/edit-loan-scheme/(:num)'] = 'accounts/payroll/edit_loan_scheme/$1';
$route['accounts/payroll/delete-loan-scheme/(:num)'] = 'accounts/payroll/delete_loan_scheme/$1';
$route['accounts/payroll/edit-saving/(:num)'] = 'accounts/payroll/edit_saving/$1';
$route['accounts/payroll/delete-saving/(:num)'] = 'accounts/payroll/delete_saving/$1';
$route['accounts/payroll/edit-personnel-payments/(:num)'] = 'accounts/payroll/edit_personnel_payments/$1';
$route['accounts/payroll/edit-personnel-allowances/(:num)'] = 'accounts/payroll/edit_personnel_allowances/$1';
$route['accounts/payroll/edit-personnel-benefits/(:num)'] = 'accounts/payroll/edit_personnel_benefits/$1';
$route['accounts/payroll/edit-personnel-deductions/(:num)'] = 'accounts/payroll/edit_personnel_deductions/$1';
$route['accounts/payroll/edit-personnel-other-deductions/(:num)'] = 'accounts/payroll/edit_personnel_other_deductions/$1';
$route['accounts/payroll/edit-personnel-savings/(:num)'] = 'accounts/payroll/edit_personnel_savings/$1';
$route['accounts/payroll/edit-personnel-loan-schemes/(:num)'] = 'accounts/payroll/edit_personnel_loan_schemes/$1';
$route['accounts/payroll/edit-personnel-relief/(:num)'] = 'accounts/payroll/edit_personnel_relief/$1';
$route['accounts/payroll/view-payslip/(:num)'] = 'accounts/payroll/view_payslip/$1';

$route['accounts/insurance-invoices'] = 'administration/reports/debtors_report_invoices/0';
$route['accounts/insurance-invoices/(:num)'] = 'administration/reports/debtors_report_invoices/$1';
$route['payroll/add-overtime-hours/(:num)'] = 'accounts/payroll/add_overtime_hours/$1';
$route['accounts/create-data-file/(:num)/(:num)'] = 'accounts/payroll/create_data_file/$1/$2';





$route['accounts/customers/import-rides'] = 'accounts/customers/do_rides_import';
$route['accounts/customers/rides-template'] = 'accounts/customers/rides_template';
$route['accounts/customers/view-rides/(:num)'] = 'accounts/customers/view_rides/$1';
$route['accounts/customers/view-rides/(:num)/(:any)/(:any)/(:num)'] = 'accounts/customers/view_rides/$1/$2/$3/$4';
$route['accounts/customers/view-rides/(:num)/(:any)/(:any)'] = 'accounts/customers/view_rides/$1/$2/$3';
$route['accounts/customer/calculate-ride-distance/(:num)/(:num)/(:num)/(:num)/(:num)'] = 'accounts/customers/calculate_ride_distance/$1/$2/$3/$4/$5';
$route['accounts/customers/add-ride/(:num)'] = 'accounts/customers/add_ride/$1';
$route['accounts/customers'] = 'accounts/customers/index';
$route['accounts/customers/(:any)/(:any)/(:num)'] = 'accounts/customers/index/$1/$2/$3';
$route['accounts/customers/(:any)/(:any)'] = 'accounts/customers/index/$1/$2';



/* End of file routes.php */
/* Location: ./system/application/config/routes.php */
//import personnel routes
$route['import/personnel'] = 'hr/personnel/import_personnel';
$route['import/personnel-template'] = 'hr/personnel/import_personnel_template';
$route['import/import-personnel'] = 'hr/personnel/do_personnel_import';

//import personnel emails
$route['import/personnel-emails'] = 'hr/personnel/import_personnel_emails';
$route['import/personnel-emails-template'] = 'hr/personnel/import_personnel_emails_template';
$route['import/import-personnel-emails'] = 'hr/personnel/do_personnel_emails_import';

//import branches routes
$route['import/branches'] = 'admin/branches/import_branches';
$route['import/branches-template'] = 'admin/branches/import_branches_template';
$route['import/import-branches'] = 'admin/branches/do_branches_import';

//payroll data import
$route['import/payroll-data'] = 'hr/import_payroll';
$route['import/payroll-template'] = 'hr/import_payroll_template';
$route['import/import-payroll']= 'hr/do_payroll_import';

//import salary advances
$route['salary-advance/import-salary-advance'] = 'accounts/salary_advance/import_salary_advance';
$route['import/import-salary-advances'] = 'accounts/salary_advance/do_advance_import';
$route['import/advance-template'] = 'accounts/salary_advance/advances_template';
$route['download-salary-advance'] = 'accounts/salary_advance/download_salary_advance';

// p9 form
$route['my-account/p9'] = 'accounts/payroll/generate_p9_form';
$route['accounts/generate_p9_form'] = 'accounts/payroll/p9_form';

//p10 form
$route['accounts/p10'] = 'accounts/payroll/generate_p10_form';
$route['accounts/generate_p10_form'] = 'accounts/payroll/p10_form';

//timesheets
$route['timesheets/add-timesheet'] = 'hr/personnel/add_personnel_timesheet';

//bank reports
$route['accounts/bank'] = 'accounts/payroll/bank';
$route['accounts/generate-bank-report/(:num)'] = 'accounts/payroll/generate_bank_report/$1';

//salary advances
$route['salary-advance'] = 'accounts/salary_advance/index';
$route['accounts/search-advances'] = 'accounts/salary_advance/search_salary_advance';
$route['close-salary-advance-search'] = 'accounts/salary_advance/close_advance_search';
$route['salary-advance/(:any)/(:any)'] = 'accounts/salary_advance/index/$1/$2';

//payroll reports routes
$route['accounts/payroll-reports'] = 'accounts/payroll/payroll_report';
$route['accounts/search-payroll-reports'] = 'accounts/payroll/search_payroll_reports';

//import overtime-hours
$route['import/overtime'] = 'accounts/payroll/import_overtime';
$route['import/overtime-template'] = 'accounts/payroll/import_overtime_template';
$route['import/import-overtime'] = 'accounts/payroll/do_overtime_import';

//send payslips to the specific personnel
$route['accounts/send-month-payslips/(:num)'] = 'accounts/payroll/send_monthly_payslips/$1';
$route['accounts/payroll/access-payslip/(:num)/(:num)'] = 'accounts/payroll/access_payslip/$1/$2';

//consultant routes
$route['accounts/cc-payment'] = 'accounts/cc_payment/index';
$route['accounts/change-cc-branch'] = 'accounts/cc_payment/change_branch';
$route['accounts/create-cc-payment'] = 'accounts/cc_payment/create_cc_payment';
$route['accounts/print-cc-paye-report/(:num)'] = 'accounts/cc_payment/print_paye_report/$1';
$route['accounts/print-cc-payment/(:num)'] = 'accounts/cc_payment/print_cc_payment/$1';
$route['accounts/print-cc-month-summary/(:num)/(:num)'] = 'accounts/cc_payment/month_summary/$1/$2';
$route['accounts/print-cc-month-payslips/(:num)'] = 'accounts/cc_payment/print_monthly_payslips/$1';
$route['accounts/search-cc-payment'] = 'accounts/cc_payment/search_cc_payment';
$route['accounts/close-cc-payment-search'] = 'accounts/cc_payment/close_cc_payment_search';
$route['account/cc-salary-data'] = 'accounts/cc_payment/salaries';
$route['accounts/cc-payment/view-payslip/(:num)'] = 'accounts/cc_payment/view_payslip/$1';
$route['accounts/cc-payment-details/(:num)'] = 'accounts/cc_payment/payment_details/$1';

//Always comes last
$route['accounts/payroll/(:any)/(:any)'] = 'accounts/payroll/payrolls/$1/$2';
$route['accounts/payroll/(:any)/(:any)/(:num)'] = 'accounts/payroll/payrolls/$1/$2/$3';
$route['accounts/salary-data/(:any)/(:any)'] = 'accounts/payroll/salaries/$1/$2';
$route['accounts/salary-data/(:any)/(:any)/(:num)'] = 'accounts/payroll/salaries/$1/$2/$3';
$route['accounts/print-month-summary/(:num)/(:num)'] = 'accounts/payroll/month_summary/$1/$2';


//sms routes
$route['sms'] = 'sms/index';
$route['sms/(:num)'] = 'sms/index/$1';
$route['allocate-sms/(:num)'] ='sms/allocate_sms/$1'; 

//Mpesa routes
$route['mpesa'] = 'mpesa/index';
$route['mpesa/add-contact'] = 'mpesa/add_contact';
$route['disburse-financial/(:num)'] ='mpesa/disburse_mpesa/$1'; 


//Mpesa routes
$route['airtime'] = 'mpesa/airtime_index';
$route['airtime/add-airtime'] = 'mpesa/add_airtime';
//$route['disburse-financial/(:num)'] ='mpesa/disburse_mpesa/$1'; 

//Mpesa routes
$route['settings/county-locations'] = 'mpesa/county_index';
$route['settings/add-county-locations'] = 'mpesa/add_county';
$route['settings/delete-location/(:num)'] = 'mpesa/delete_county/$1';


//Mpesa routes
$route['settings/key-words'] = 'mpesa/keyword_index';
$route['settings/add-keywords'] = 'mpesa/add_keyword';
$route['settings/delete-keyword/(:num)'] = 'mpesa/demote_keyword/$1';





/*
*	Messaging Routes
*/

$route['messaging/dashboard'] = 'messaging/dashboard';
$route['messages'] = 'messaging/unsent_messages';
$route['messaging/unsent-messages'] = 'messaging/unsent_messages';
$route['messaging/unsent-messages/(:num)'] = 'messaging/unsent_messages/$1';
$route['messaging/sent-messages'] = 'messaging/sent_messages';
$route['messaging/sent-messages/(:num)'] = 'messaging/sent_messages/$1';
$route['messaging/spoilt-messages'] = 'messaging/spoilt_messages';
$route['messaging/spoilt-messages/(:num)'] = 'messaging/spoilt_messages/$1';
// import functions of messages
$route['messaging/validate-import/(:num)'] = 'messaging/do_messages_import/$1';
$route['messaging/import-template'] = 'messaging/import_template';
$route['messaging/import-messages'] = 'messaging/import_messages';
$route['messaging/send-messages'] = 'messaging/send_messages';

$route['add-contact'] = 'administration/contacts/add_contact';
$route['edit-contact/(:num)'] = 'administration/contacts/edit_contact/$1';
$route['contacts'] = 'administration/contacts/index';
$route['contacts/(:num)'] = 'administration/contacts/index/$1';
$route['delete-contact/(:num)'] = 'administration/contacts/delete_contact/$1';
$route['contacts/validate-import/(:num)'] = 'administration/contacts/do_messages_import/$1';
$route['contacts/import-template'] = 'administration/contacts/import_template';
$route['contacts/import-messages'] = 'administration/contacts/import_messages';

$route['messaging/message-templates'] = 'messaging/message_templates';
$route['messaging/add-template'] = 'messaging/add_message_template';
$route['messaging/edit-message-template/(:num)'] = 'messaging/edit_message_template/$1';
$route['messaging/activate-message-template/(:num)'] = 'messaging/activate_message_template/$1';
$route['messaging/deactivate-message-template/(:num)'] = 'messaging/deactivate_message_template/$1';
$route['template-detail/(:num)'] = 'messaging/template_detail/$1';
$route['set-search-parameters/(:num)'] = 'messaging/set_search_parameters/$1';
$route['create-batch-items/(:num)'] = 'messaging/create_batch_items/$1';

$route['send-messages/(:num)/(:num)'] = 'messaging/send_batch_messages/$1/$2';
$route['view-senders/(:num)/(:num)'] = 'messaging/view_persons_for_batch/$1/$2';
$route['view-schedules/(:num)/(:num)'] = 'messaging/view_schedules/$1/$2';
$route['messaging/dashboard'] = 'messaging/dashboard';
$route['delete-message-contact/(:num)/(:num)/(:num)'] = 'messaging/delete_contact/$1/$2/$3';
$route['create-new-schedule/(:num)/(:num)'] = 'messaging/create_new_schedule/$1/$2';
$route['bulk-delete-contacts/(:num)'] = 'administration/contacts/bulk_delete_contacts/$1';
