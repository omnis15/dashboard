<?php

class Site_model extends CI_Model 
{
	public function get_slides()
	{
  		$table = "slideshow";
		$where = "slideshow_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_partners()
	{
  		$table = "partners";
		$where = "partners_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_corporates()
	{
  		$table = "corporates";
		$where = "corporates_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function get_all_services()
	{
		$table = "service";
		$where = "service_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;

	}
	public function get_resource($limit = NULL)
	{
		$table = "resource";
		$where = "resource_status = 1";
		if($limit !=NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->where($where);
		$this->db->order_by('resource_id', 'DESC');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function display_page_title()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$name = $this->site_model->decode_web_name($page[$last]);
		
		if(is_numeric($name))
		{
			$last = $last - 1;
			$name = $this->site_model->decode_web_name($page[$last]);
		}
		$page_url = ucwords(strtolower($name));
		
		return $page_url;
	}
	
	function generate_price_range()
	{
		$max_price = $this->products_model->get_max_product_price();
		//$min_price = $this->products_model->get_min_product_price();
		
		$interval = $max_price/5;
		
		$range = '';
		$start = 0;
		$end = 0;
		
		for($r = 0; $r < 5; $r++)
		{
			$end = $start + $interval;
			$value = 'KES '.number_format(($start+1), 0, '.', ',').' - KES '.number_format($end, 0, '.', ',');
			$range .= '
			<label class="radio-fancy">
				<input type="radio" name="agree" value="'.$start.'-'.$end.'">
				<span class="light-blue round-corners"><i class="dark-blue round-corners"></i></span>
				<b>'.$value.'</b>
			</label>';
			
			$start = $end;
		}
		
		return $range;
	}
	
	public function get_account_navigation()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		
		$name = strtolower($page[1]);
		
		$resources = '';
		$events = '';
		$destinations = '';
		$notifications = '';
		$offers = '';
		$invoices = '';
		$profile = '';
		
		if($name == 'resources')
		{
			$resources = 'current';
		}
		
		if($name == 'events')
		{
			$events = 'current';
		}
		if($name == 'notifications')
		{
			$notifications = 'current';
		}
		if($name == 'offers')
		{
			$offers = 'current';
		}
		if($name == 'invoices')
		{
			$invoices = 'current';
		}
		if($name == 'profile')
		{
			$profile = 'current';
		}
		if($name == 'destinations')
		{
			$destinations = 'current';
		}
		$navigation = 
		'
			<li class="'.$resources.'">
				<a href="'.site_url().'member/resources">
					<i class="fa fa-book"></i>
					Resources
				</a>
			</li>
			<li class="'.$events.'">
				<a href="'.site_url().'member/events">
					<i class="fa fa-newspaper-o"></i>
					Events
				</a>
			</li>
			<li class="'.$notifications.'">
				<a href="'.site_url().'member/notifications">
					<i class="fa fa-bell"></i>
					Notifications
				</a>
			</li>
			<li class="'.$offers.'">
				<a href="'.site_url().'member/offers">
					<i class="fa fa-plus-square"></i>
					Offers
				</a>
			</li>
			<li class="'.$invoices.'">
				<a href="'.site_url().'member/invoices">
					<i class="fa fa-money"></i>
					Invoices
				</a>
			</li>
			<li class="'.$profile.'">
				<a href="'.site_url().'member/profile">
					<i class="fa fa-user"></i>
					Profile
				</a>
			</li>
			
			';
		
		return $navigation;
	}
	
	public function get_home_navigation($page = NULL)
	{
		$class = 'dropdown';
		$page = explode("/",uri_string());
		$total = count($page);
		
		$name = strtolower($page[0]);
		
		$home = '';
		$blog = '';
		$about = '';
		$contact = '';
		$gallery = '';
		$services = '';
		
		if($name == 'home')
		{
			 $home = 'current';
		}
		if($name == 'about')
		{
			$about = 'current';
		}
		if($name == 'contact')
		{
			$contact = 'current';
		}
		if($name == 'gallery')
		{
			$gallery = 'current';
		}
		if($name == 'blog')
		{
			$blog = 'current';
		}
		if($name == 'services')
		{
			$services = 'current';
		}
		
		$navigation = 
		'
			<li class="'.$class.' '.$home.'" ><a href="'.site_url().'home">Home</a></li>
			<li class="'.$class.' '.$about.'"><a  href="'.site_url().'about">About</a></li>
			<li class="'.$class.'"'.$services.'><a  href="'.site_url().'services">Services</a></li>
			<!--<li class="'.$class.' '.$gallery.'"><a  href="'.site_url().'gallery">Gallery</a></li>-->
			<li class="'.$class.' '.$blog.'" ><a href="'.site_url().'blog">Blog</a></li>
			<li class="last '.$class.' '.$contact.'" ><a href="'.site_url().'contact">Contact Us</a></li>
		';
		
		return $navigation;
	}
	
	public function get_navigation($page = NULL)
	{
		$class = 'dropdown pad';
		$page = explode("/",uri_string());
		$total = count($page);
		
		$name = strtolower($page[0]);
		
		$home = 'lightblue ';
		$blog = 'brown ';
		$about = 'orange ';
		$contact = 'red ';
		$gallery = 'green ';
		$services = 'black ';
		
		if($name == 'home')
		{
			 $home .= 'current-menu-item';
		}
		if($name == 'about')
		{
			$about .= 'current-menu-item';
		}
		if($name == 'contact')
		{
			$contact .= 'current-menu-item';
		}
		if($name == 'gallery')
		{
			$gallery .= 'current-menu-item';
		}
		if($name == 'blog')
		{
			$blog .= 'current-menu-item';
		}
		if($name == 'services')
		{
			$services .= 'current-menu-item';
		}
		
		$navigation = 
		'
			<li class="'.$class.' '.$home.'" ><a href="'.site_url().'home" class="dark">Home</a></li>
			<li class="'.$class.' '.$about.'"><a  href="'.site_url().'about" class="dark">About</a></li>
			<li class="'.$class.' '.$services.'"><a  href="'.site_url().'services" class="dark">Services</a></li>
			<!--<li class="'.$class.' '.$gallery.'"><a  href="'.site_url().'gallery" class="dark">Gallery</a></li>-->
			<li class="'.$class.' '.$blog.'" ><a href="'.site_url().'blog" class="dark">Blog</a></li>
			<li class="last '.$class.' '.$contact.'" ><a href="'.site_url().'contact" class="dark">Contact</a></li>
		';
		
		return $navigation;
	}
	
	public function get_active_departments($service_name)
	{
  		$table = "service, department";
		$where = "service.service_status = 1 AND department.department_status = 1 AND service.department_id = department.department_id AND department.department_name = '".$service_name."'";
		
		$this->db->select('service.*');
		$this->db->where($where);
		$this->db->group_by('service_name');
		$this->db->order_by('service_name', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}

	public function get_active_services()
	{
  		$table = "service";
		$where = "service.service_status = 1 AND department_id = 2";
		
		$this->db->select('service.*');
		$this->db->where($where);
		$this->db->group_by('service_name');
		$this->db->order_by('service_name', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_active_service_gallery($service_id)
	{
		$table = "service_gallery";
		$where = "service_id = ".$service_id;
		
		$this->db->select('service_gallery.*');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_active_gallery()
	{
		$table = "gallery";
		$where = "gallery_status > 0";
		
		$this->db->select('gallery.*');
		$this->db->where($where);
		
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_active_service_gallery_names()
	{
		$table = "gallery";
		$where = "gallery_status > 0";
		
		$this->db->select('gallery.*');
		$this->db->where($where);
		$this->db->group_by('gallery_name');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function create_web_name($field_name)
	{
		$web_name = str_replace(" ", "-", $field_name);
		
		return $web_name;
	}
	public function get_services($table, $where, $limit = NULL)
	{
		$this->db->where($where);
		$this->db->select('service.*');
		$this->db->order_by('service_name', 'ASC');
		$query = $this->db->get($table);
		
		
		return $query;
	}
	
	public function decode_web_name($web_name)
	{
		$field_name = str_replace("-", " ", $web_name);
		
		return $field_name;
	}
	
	public function image_display($base_path, $location, $image_name = NULL)
	{
		$default_image = 'http://placehold.it/300x300&text=IOD';
		$file_path = $base_path.'/'.$image_name;
		//echo $file_path.'<br/>';
		
		//Check if image was passed
		if($image_name != NULL)
		{
			if(!empty($image_name))
			{
				if((file_exists($file_path)) && ($file_path != $base_path.'\\'))
				{
					return $location.$image_name;
				}
				
				else
				{
					return $default_image;
				}
			}
			
			else
			{
				return $default_image;
			}
		}
		
		else
		{
			return $default_image;
		}
	}
	
	public function get_contacts()
	{
  		$table = "branch";
		$this->db->where('branch_parent', 0);
		$query = $this->db->get($table);
		$contacts = array();
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$contacts['email'] = $row->branch_email;
			$contacts['phone'] = $row->branch_phone;
			$contacts['facebook'] ='';
			$contacts['twitter'] = '';
			$contacts['linkedin'] = '';
			$contacts['company_name'] = $row->branch_name;
			$contacts['logo'] = $row->branch_image_name;
			$contacts['address'] = $row->branch_address;
			$contacts['city'] = $row->branch_city;
			$contacts['post_code'] = $row->branch_post_code;
			$contacts['building'] = $row->branch_building;
			$contacts['floor'] = $row->branch_floor;
			$contacts['location'] = $row->branch_location;
			$contacts['working_weekend'] = $row->branch_working_weekend;
			$contacts['working_weekday'] = $row->branch_working_weekday;
			$contacts['mission'] = '';
			$contacts['vision'] = '';
			$contacts['motto'] = '';
			$contacts['about'] = '';
			$contacts['objectives'] = '';
			$contacts['core_values'] = '';
			$contacts['corporate_values'] = '';
		}
		return $contacts;
	}
	
	public function get_breadcrumbs()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$crumbs = '<li><a href="'.site_url().'home"><i class="fa fa-home"></i> Home</a></li>';
		
		for($r = 0; $r < $total; $r++)
		{
			$name = $this->decode_web_name($page[$r]);
			if($r == $last)
			{
				$crumbs .= '<li><a href="#!" class="active-trail">'.strtoupper($name).'</a></li>';
			}
			else
			{
				if($total == 3)
				{
					if($r == 1)
					{
						$crumbs .= '<li><a href="'.site_url().$this->create_web_name($page[$r-1]).'/'.$this->create_web_name(strtolower($name)).'">'.strtoupper($name).'</a></li>';
					}
					else
					{
						$crumbs .= '<li><a href="'.$this->create_web_name(site_url().strtolower($name)).'">'.strtoupper($name).'</a></li>';
					}
				}
				else
				{
					$crumbs .= '<li><a href="'.$this->create_web_name(site_url().strtolower($name)).'">'.strtoupper($name).'</a></li>';
				}
			}
		}
		
		return $crumbs;
	}
	
	public function contact() 
	{
		$this->load->model('site/email_model');
		$contacts = $this->site_model->get_contacts();
		$message['contacts'] = $contacts;
		if(count($contacts) > 0)
		{
			$email = $contacts['email'];
			$company_name = $contacts['company_name'];
		}
		$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$subject = $this->input->post('sender_name')." has requested a quote";
		$message_text = '
				<p>A help message was sent on '.$date.' saying:</p> 
				<p>'.$this->input->post('message').'</p>
				<p>Their contact details are:</p>
				<p>
					Name: '.$this->input->post('sender_name').'<br/>
					Email: '.$this->input->post('sender_email').'<br/>
					Phone: '.$this->input->post('sender_phone').'
				</p>
				';
		
		$message['subject'] = $subject;
		$message['text'] = $message_text;
		$message['text'] = $this->load->view('compose_mail', $message, TRUE);
		$sender['email'] = $this->input->post('sender_email'); 
		$sender['name'] = $this->input->post('sender_name');
		$receiver['email'] = $email;
		$receiver['name'] = $company_name;
		
		$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
		
		return $response;
	}
	
	public function request_quote() 
	{
		$this->load->model('site/email_model');
		$contacts = $this->site_model->get_contacts();
		$message['contacts'] = $contacts;
		if(count($contacts) > 0)
		{
			$email = $contacts['email'];
			$company_name = $contacts['company_name'];
		}
		$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$subject = $this->input->post('fname')." has requested a quote";
		$message_text = '
				<p>A help message was sent on '.$date.' saying:</p> 
				<p>'.$this->input->post('message').'</p>
				<p>Their contact details are:</p>
				<p>
					Name: '.$this->input->post('fname').'<br/>
					Email: '.$this->input->post('email').'<br/>
					Phone: '.$this->input->post('tel').'
				</p>
		';
		
		$message['subject'] = $subject;
		$message['text'] = $message_text;
		$message['text'] = $this->load->view('compose_mail', $message, TRUE);
		$sender['email'] = $this->input->post('email'); 
		$sender['name'] = $this->input->post('fname');
		$receiver['email'] = $email;
		$receiver['name'] = $company_name;
		
		$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
		
		return $response;
	}
	
	public function get_testimonials()
	{
		$this->db->where('post.blog_category_id = blog_category.blog_category_id AND (blog_category.blog_category_name LIKE "%testimonials%") AND post.post_status = 1');
		$this->db->order_by('post.created','ASC');
		return $this->db->get('post,blog_category');
	}
	public function get_faqs()
	{
		$this->db->where('post.blog_category_id = blog_category.blog_category_id AND (blog_category.blog_category_name LIKE "%faqs%") AND post.post_status = 1');
		$this->db->order_by('post.created','ASC');
		return $this->db->get('post,blog_category');
	}
	public function get_front_end_items()
	{
		$this->db->where('post.blog_category_id = blog_category.blog_category_id AND (blog_category.blog_category_name LIKE "%front%") AND post.post_status = 1');
		$this->db->order_by('post.created','ASC');
		$this->db->limit(1);
		return $this->db->get('post,blog_category');
	}
	
	public function valid_url($url)
	{
		$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
		//$pattern = "/^((ht|f)tp(s?)\:\/\/|~/|/)?([w]{2}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?/";
        if (!preg_match($pattern, $url))
		{
            return FALSE;
        }
 
        return TRUE;
	}
	
	public function get_days($date)
	{
		$now = time(); // or your date as well
		$your_date = strtotime($date);
		$datediff = $now - $your_date;
		return floor($datediff/(60*60*24));
	}
	
	public function limit_text($text, $limit) 
	{
		$pieces = explode(" ", $text);
		$total_words = count($pieces);
		
		if ($total_words > $limit) 
		{
			$return = "<i>";
			$count = 0;
			for($r = 0; $r < $total_words; $r++)
			{
				$count++;
				if(($count%$limit) == 0)
				{
					$return .= $pieces[$r]."</i><br/><i>";
				}
				else{
					$return .= $pieces[$r]." ";
				}
			}
		}
		
		else{
			$return = "<i>".$text;
		}
		return $return.'</i><br/>';
    }

    public function get_all_resources($table, $where, $per_page, $page)
	{
		//retrieve all trainings
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('resource_category.resource_category_id', 'DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	public function view_single_post($post_title)
	{
		$this->db->where('post_title = $post_title');
		$this->db->select('*');
		return $query = $this->db->get('post');
	}
	public function get_event($training_id)
	{
		$this->db->where('training_id  = '.$training_id);
		$this->db->select('*');
		return $query = $this->db->get('training');
	}

	public function get_event_id($training_name)
	{
		//retrieve all users
		$this->db->from('training');
		$this->db->select('training_id');
		$this->db->where('training_name', $training_name);
		$query = $this->db->get();
		$training_id = FALSE;
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$training_id = $row->training_id;
		}
		
		return $training_id;
	}

	public function get_crumbs()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		
		$crumb[0]['name'] = ucwords(strtolower($page[0]));
		$crumb[0]['link'] = $page[0];
		
		if($total > 1)
		{
			$sub_page = explode("-",$page[1]);
			$total_sub = count($sub_page);
			$page_name = '';
			
			for($r = 0; $r < $total_sub; $r++)
			{
				$page_name .= ' '.$sub_page[$r];
			}
			$crumb[1]['name'] = ucwords(strtolower($page_name));
			
			if($page[1] == 'category')
			{
				$category_id = $page[2];
				$category_details = $this->categories_model->get_category($category_id);
				
				if($category_details->num_rows() > 0)
				{
					$category = $category_details->row();
					$category_name = $category->category_name;
				}
				
				else
				{
					$category_name = 'No Category';
				}
				
				$crumb[1]['link'] = 'products/all-products/';
				$crumb[2]['name'] = ucwords(strtolower($category_name));
				$crumb[2]['link'] = 'products/category/'.$category_id;
			}
			
			else if($page[1] == 'brand')
			{
				$brand_id = $page[2];
				$brand_details = $this->brands_model->get_brand($brand_id);
				
				if($brand_details->num_rows() > 0)
				{
					$brand = $brand_details->row();
					$brand_name = $brand->brand_name;
				}
				
				else
				{
					$brand_name = 'No Brand';
				}
				
				$crumb[1]['link'] = 'products/all-products/';
				$crumb[2]['name'] = ucwords(strtolower($brand_name));
				$crumb[2]['link'] = 'products/brand/'.$brand_id;
			}
			
			else if($page[1] == 'view-product')
			{
				$product_id = $page[2];
				$product_details = $this->products_model->get_product($product_id);
				
				if($product_details->num_rows() > 0)
				{
					$product = $product_details->row();
					$product_name = $product->product_name;
				}
				
				else
				{
					$product_name = 'No Product';
				}
				
				$crumb[1]['link'] = 'products/all-products/';
				$crumb[2]['name'] = ucwords(strtolower($product_name));
				$crumb[2]['link'] = 'products/view-product/'.$product_id;
			}
			
			else
			{
				$crumb[1]['link'] = '#';
			}
		}
		
		return $crumb;
	}
	
	public function get_resource_category_id($resource_category_name)
	{
		//retrieve all users
		$this->db->from('resource_category');
		$this->db->where('resource_category_name', $resource_category_name);
		$query = $this->db->get();
		$return['resource_category_id'] = FALSE;
		$return['member_only'] = FALSE;
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$return['resource_category_id'] = $row->resource_category_id;
			$return['member_only'] = $row->member_only;
		}
		
		return $return;
	}
	public function get_service_id($about_name)
	{
		$this->db->from('service');
		$this->db->select('service_id');
		$this->db->where('service_name', $about_name);
		$query = $this->db->get();
		$service_id = FALSE;
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$service_id = $row->service_id;
		}
		
		return $service_id;
	}
	
	public function get_author_name($created_by)
	{
		$this->db->from('users');
		$this->db->select('other_names');
		$this->db->where('user_id', $created_by);
		$query = $this->db->get();
		$author_name = '';
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$author_name = $row->other_names;
		}
		
		return $author_name;
	}
	
	public function get_event_type_id($event_type_name)
	{
		//retrieve all users
		$this->db->from('event_type');
		$this->db->select('event_type_id');
		$this->db->where('event_type_name', $event_type_name);
		$query = $this->db->get();
		$event_type_id = FALSE;
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$event_type_id = $row->event_type_id;
		}
		
		return $event_type_id;
	}

	public function get_event_item($event_type_id)
	{
		$this->db->where('event_type.event_type_id = event.event_type_id AND event_start_time >= CURDATE() AND event_type.event_type_id = '.$event_type_id);
		$this->db->select('*');
		return $query = $this->db->get('event, event_type');
	}

	public function get_resource_item($resource_category_id)
	{
		$this->db->where('resource_category_id  = '.$resource_category_id);
		$this->db->select('*');
		return $query = $this->db->get('resource');
	}
	public function get_resource_category($resource_category_id)
	{

		$this->db->where('resource_category_id  = '.$resource_category_id);
		$this->db->select('*');
		return $query = $this->db->get('resource_category');

	}
	public function get_about_item($service_id)
	{
		$this->db->where('service_id  = '.$service_id);
		$this->db->select('*');
		return $query = $this->db->get('service');
	}
	public function get_directors()
	{
		$table = "directors";
		$where = "directors_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_member_details($directors_name)
	{
  		$table = "directors";
		$where = "directors_name = '".$directors_name."'";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_facilitators()
	{
		$table = "facilitators";
		$where = "facilitators_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function get_tweets_old()
	{
		$this->load->library('twitterfetcher');
	
		$tweets = $this->twitterfetcher->getTweets(array(
			'consumerKey'       => 'fZvEA9Mw24i2jT3VIn1sIz92y',
			'consumerSecret'    => 'NW3rzs0jEv39JdSmNeurZvKL577vxPVLuV95vedROczQtQIbDp',
			'accessToken'       => '588425913-dHNleDnlFPdfHGYjZpUnph7MEhKTXqULJ6OaP6IP',
			'accessTokenSecret' => 'iMmuv0bADX4CbG3i0T1vDvGo1uRYlAWfb5khB9Qfm7v3m',
			'usecache'          => true,
			'count'             => 5,  //this how many tweets to fectch
			'numdays'           => 3000
		));
		
		return $tweets;
	}
	
	public function get_tweets()
	{
		$this->load->library('twitteroauth');
		$consumer = 'fZvEA9Mw24i2jT3VIn1sIz92y';
		$consumer_secret = 'NW3rzs0jEv39JdSmNeurZvKL577vxPVLuV95vedROczQtQIbDp';
		$access_token = '588425913-dHNleDnlFPdfHGYjZpUnph7MEhKTXqULJ6OaP6IP';
		$access_token_secret = 'iMmuv0bADX4CbG3i0T1vDvGo1uRYlAWfb5khB9Qfm7v3m';
		
		//Create an instance
		$connection = $this->twitteroauth->create($consumer, $consumer_secret, $access_token, $access_token_secret);
	
		//Verify your authentication details
		$content = $connection->get('account/verify_credentials');
	}
	
	/*
	*	Retrieve latest safaris
	*
	*/
	public function get_latest_locations($num = 6)
	{
		$this->db->select('location.*');
		$this->db->where('location_status = 1 AND parent_id = 0');
		$this->db->order_by('created', 'DESC');
		$query = $this->db->get('location', $num);
		
		return $query;
	}
	
	/*
	*	Retrieve latest safaris
	*
	*/
	public function get_latest_safaris($num = 6)
	{
		$this->db->select('safari.*, location.location_name');
		$this->db->where('safari_status = 1 AND safari.location_id = location.location_id');
		$this->db->order_by('created', 'DESC');
		$query = $this->db->get('safari, location', $num);
		
		return $query;
	}
	
	public function get_all_available_safaries()
	{
		$this->db->select('safari.*, location.location_name');
		$this->db->where('safari_status = 1 AND safari.location_id = location.location_id');
		$this->db->order_by('created', 'DESC');
		$query = $this->db->get('safari, location');
		
		return $query;
	}
	public function get_all_available_locations($num = 6)
	{
		$this->db->select('*');
		$this->db->where('location_status = 1');
		$this->db->order_by('created', 'DESC');
		$query = $this->db->get('location', $num);
		
		return $query;
	}
	public function get_number_of_location_safaries($location_id)
	{
		$this->db->select('COUNT(location_id) as no_of_safaries');
		$this->db->where('location_id = '.$location_id);
		$query = $this->db->get('safari');
		$number_of_safaries = 0;
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $location_safaries )
			{
				$number_of_safaries = $location_safaries->no_of_safaries;
				return $number_of_safaries;
			}
		}
		return $number_of_safaries;
	}
	public function get_all_destinations()
	{
		$this->db->select('COUNT(location_id) as no_of_destinations');
		$this->db->where('location_status = 1');
		$query = $this->db->get('location');
		$no_of_destinations = 0;
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $location_safaries )
			{
				$no_of_destinations = $location_safaries->no_of_destinations;
				return $no_of_destinations;
			}
		}
		return $no_of_destinations;
	}
	public function get_all_safaries()
	{
		$this->db->select('COUNT(safari_id) as number_of_safaries');
		$this->db->where('safari_status = 1 AND safari.location_id = location.location_id');
		$query = $this->db->get('safari, location');
		$no_of_safaris = 0;
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $all_safaries )
			{
				$no_of_safaris = $all_safaries->number_of_safaries;
				return $no_of_safaris;
			}
		}
		return $no_of_safaris;
	}
	public function get_all_available_posts($num = 4)
	{
		$this->db->select('*');
		$this->db->where('post_status = 1');
		$this->db->order_by('created', 'DESC');
		$query = $this->db->get('post', $num);
		
		return $query;
	}
	
	public function get_safari_details($safari_name)
	{
		$safariname = str_replace('-', ' ', $safari_name);
		$this->db->select('*');
		$this->db->where('safari_status = 1 AND safari_name = "'.$safariname.'"');
		//$this->db->order_by('created', 'DESC');
		$query = $this->db->get('safari');
		return $query;
	}
	
	public function get_all_location_safaries($location_name)
	{
		$locationname = str_replace('-', ' ', $location_name);
		$this->db->select('*');
		$this->db->where('location_status = 1  AND location.location_id = safari.location_id AND location_name = "'.$locationname.'"');
		//$this->db->order_by('created', 'DESC');
		$query = $this->db->get('location,safari');
		
		return $query;
	}
	public function get_location_name($location_id)
	{
		$name = '';
		$this->db->select('location_name');
		$this->db->where('location_id = '.$location_id);
		$query = $this->db->get('location');
		
		if($query->num_rows()>0)
		{
			foreach($query->result() as $location)
			{
				$name = $location->location_name;
			}
		}
		return $name;
	}
}

?>