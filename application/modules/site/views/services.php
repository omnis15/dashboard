<?php $services = $this->site_model->get_active_services();?>
                    <!-- page-banner-section 
						================================================== -->
					<section class="page-banner-section">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<h2><?php echo $title?></h2>
								</div>
								<div class="col-md-6">
									<ul class="page-depth">
										<?php echo $this->site_model->get_breadcrumbs();?>
									</ul>
								</div>
							</div>
						</div>
					</section>
                    <!-- End page-banner section -->
					
                    <!-- End page-banner section -->
					<div class="container">
						<div class="row">
							<div class="block block-system">
								<!-- services-page section 
									================================================== -->
								<section class="services-page-section">
									<div class="container">
										<div class="row">
											<div class="col-md-3">
												<div class="side-navigation">
													<ul class="side-navigation-list">
                                                        <?php
															$checking_items = '';
															echo '<li><a href="'.site_url().'services">Our Services</a>';
															if($services->num_rows() > 0)
															{   $count = 0;
																foreach($services->result() as $res)
																{
																	$service_name = $res->service_name;
																	$service_description = $res->service_description;
																	$mini_desc = implode(' ', array_slice(explode(' ', $service_description), 0, 100));
																	 $maxi_desc = implode(' ', array_slice(explode(' ', $service_description), 0, 40));
																	$web_name = $this->site_model->create_web_name($service_name);
										
																	$count ++;
										
																	echo '<li><a href="'.site_url().'services/'.$web_name.'">'.$service_name.'</a></li>'; 
																   
																}
															}
														?>
													</ul>
												</div>
											</div>
											<div class="col-md-9">
												<div class="services-wrapp">
													<div class="row">
                                                    	<?php
															$checking_items = '';
															if($services->num_rows() > 0)
															{   $count = 0;
																foreach($services->result() as $res)
																{
																	$service_name = $res->service_name;
																	$service_image_name = $res->service_image_name;
																	$service_description = $res->service_description;
																	$mini_desc = implode(' ', array_slice(explode(' ', strip_tags($service_description)), 0, 100));
																	$maxi_desc = implode(' ', array_slice(explode(' ', strip_tags($service_description)), 0, 40));
																	$web_name = $this->site_model->create_web_name($service_name);
										
																	$count ++;
										
																	$checking_items .=
																	'
																	<div class="col-md-6">
																		<div class="services-post">
																			<img src="'.$service_location.$service_image_name.'" alt="'.$service_name.'">
																			<h2>'.$service_name.'</h2>
																			<p>'.$maxi_desc.'</p>
																			<a href="'.site_url().'services/'.$web_name.'">Read More</a>
																		</div>
																	</div>'; 
																   
																}
															}
															echo $checking_items;
														?>
												</div>
											</div>
										</div>
									</div>
								</section>
								<!-- End services-page section -->
							</div>
							<!-- End content -->
						</div>
					</div>
                   