<?php
$contacts = $this->site_model->get_contacts();
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		$linkedin = $contacts['linkedin'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
        $address = $contacts['address'];
        $post_code = $contacts['post_code'];
        $city = $contacts['city'];
        $building = $contacts['building'];
        $floor = $contacts['floor'];
        $location = $contacts['location'];
		$about = $contacts['about'];
		$mission = $contacts['mission'];

        $working_weekday = $contacts['working_weekday'];
        $working_weekend = $contacts['working_weekend'];
	}
	//$tweets = $this->site_model->get_tweets();
?>
		<!-- START FOOTER -->
        <footer class="footer" id="links">
            <div class="container-fluid">

                <h2 class="reveal reveal-top">Ubiker</h2>
                
                <!-- START FOOTER NAVIGATION -->
                <ul class="footer-nav reveal reveal-left">
                    <li><a href="#banner" class="active">Home</a></li>
                    <li><a href="#download">Download</a></li>
                    <li><a href="#reviews">Reviews</a></li>
                    <li><a href="#features">Features</a></li>
                    <li><a href="#screenshots">Screenshots</a></li>
                    <li><a href="#contacts">Contact</a></li>
                </ul>
                <!-- END FOOTER NAVIGATION -->
                
                <!-- START FOOTER SOCIAL BLOCK -->
                <ul class="footer-social reveal reveal-right">
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-youtube-play"></a></li>
                </ul>
                  <!-- START COPYRIGHTS -->
                <div class="footer-copyrights">
                  <p>Jumuia place, Lenana Road</p>
                   <p>P.O BOX 1631-0502 Karen Nairobi</p>
                   <p>Karen, Nairobi</p>
                  <p>Landline: 0202516564 </p>
                  <p>Cell:0731289449 and 0705479747</p>
                </div>
                <!-- END FOOTER SOCIAL BLOCK -->
                
                <!-- START COPYRIGHTS -->
                <div class="footer-copyrights">
                    <span class="copyrights-date">&#169; Ubiker <?php echo date('Y');?>.</span>
                    <span class="copyrights-text">All Rights Reserved. Powered by <a href="http://www.omnis.co.ke" target="_blank">Omnis Limited</a></span>
                </div>
                <!-- END COPYRIGHTS -->

            </div>
        </footer>
        <!-- END FOOTER -->
        