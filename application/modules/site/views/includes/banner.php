<section style="margin-top: 82px;" class="banner">
    <div style="background-position: 50% -41px;" class="bg-parallax bg-1"></div>
    <div class="container">
        <div style="display: none;" class="logo-banner text-center">
            <a href="<?php echo base_url()."assets/themes/kiboko/";?>index.html" title=""><img src="<?php echo base_url()."assets/themes/kiboko/";?>images/logo-banner.png" alt="">
            </a>
        </div>
        <div class="banner-cn">
            <ul class="tabs-cat text-center row">
                <li class="cate-item col-xs-2"><a data-toggle="tab" href="<?php echo base_url()."assets/themes/kiboko/";?>index.html#form-flight" title=""><span>flight</span> <img src="<?php echo base_url()."assets/themes/kiboko/";?>images/icon-flight.png" alt=""></a>
                </li>
                <li class="cate-item active col-xs-2"><a data-toggle="tab" href="<?php echo base_url()."assets/themes/kiboko/";?>index.html#form-hotel" title=""><span>Hotel</span><img src="<?php echo base_url()."assets/themes/kiboko/";?>images/icon-hotel.png" alt=""></a>
                </li>
                <li class="cate-item col-xs-2"><a data-toggle="tab" href="<?php echo base_url()."assets/themes/kiboko/";?>index.html#form-car" title=""><span>Car</span><img src="<?php echo base_url()."assets/themes/kiboko/";?>images/icon-car.png" alt=""></a>
                </li>
                <li class="cate-item col-xs-2"><a data-toggle="tab" href="<?php echo base_url()."assets/themes/kiboko/";?>index.html#form-package" title=""><span>package deals</span><img src="<?php echo base_url()."assets/themes/kiboko/";?>images/icon-tour.png" alt=""></a>
                </li>
                <li class="cate-item col-xs-2"><a data-toggle="tab" href="<?php echo base_url()."assets/themes/kiboko/";?>index.html#form-cruise" title=""><span>cruise</span><img src="<?php echo base_url()."assets/themes/kiboko/";?>images/icon-cruise.png" alt=""></a>
                </li>
                <li class="cate-item col-xs-2"><a data-toggle="tab" href="<?php echo base_url()."assets/themes/kiboko/";?>index.html#form-tour" title=""><span>TOUR</span><img src="<?php echo base_url()."assets/themes/kiboko/";?>images/icon-vacation.png" alt=""></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="form-cn form-hotel tab-pane active in" id="form-hotel">
                    <h2>Where would you like to go?</h2>
                    <div class="form-search clearfix">
                        <div class="form-field field-destination">
                            <label for="destination"><span>Destination:</span> Country, City, Airport, Area, Landmark</label>
                            <input id="destination" class="field-input" type="text">
                        </div>
                        <div class="form-field field-date">
                            <input id="dp1470067323753" class="field-input calendar-input hasDatepicker" placeholder="Check in" type="text">
                        </div>
                        <div class="form-field field-date">
                            <input id="dp1470067323754" class="field-input calendar-input hasDatepicker" placeholder="Check out" type="text">
                        </div>
                        <div class="form-field field-select">
                            <div class="select"><span>Guest</span>
                                <select>
                                    <option>Guest</option>
                                    <option>1 Guest</option>
                                    <option>2 Guest</option>
                                    <option>3 Guest</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-lager awe-search">Search <i class="fa search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="form-cn form-car tab-pane" id="form-car">
                    <h2>Where would you like to go?</h2>
                    <div class="form-search clearfix">
                        <div class="form-field field-picking">
                            <label for="picking"><span>Picking up:</span> City, airport...</label>
                            <input id="picking" class="field-input" type="text">
                        </div>
                        <div class="form-field field-droping">
                            <input class="field-input" placeholder="Droping off" type="text">
                        </div>
                        <div class="form-field field-date">
                            <input id="dp1470067323755" class="field-input calendar-input hasDatepicker" placeholder="Pink up date" type="text">
                        </div>
                        <div class="form-field field-date">
                            <input id="dp1470067323756" class="field-input calendar-input hasDatepicker" placeholder="Drop off date" type="text">
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-lager awe-search">Search</button>
                        </div>
                    </div>
                </div>
                <div class="form-cn form-cruise tab-pane" id="form-cruise">
                    <h2>Where would you like to go?</h2>
                    <ul class="form-radio">
                        <li>
                            <div class="radio-checkbox">
                                <input name="radio-2" id="radio-5" class="radio" type="radio">
                                <label for="radio-5">Popular Cruises</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input name="radio-2" id="radio-6" class="radio" type="radio">
                                <label for="radio-6">Luxury Cruises</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input name="radio-2" id="radio-7" class="radio" type="radio">
                                <label for="radio-7">River Cruises</label>
                            </div>
                        </li>
                    </ul>
                    <div class="form-search clearfix">
                        <div class="form-field field-destination">
                            <label for="destination2"><span>Destination:</span> Asia...</label>
                            <input id="destination2" class="field-input" type="text">
                        </div>
                        <div class="form-field field-select field-lenght">
                            <div class="select"><span>Length of Cruise</span>
                                <select>
                                    <option>Length of Cruise</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-month">
                            <div class="select"><span>Month</span>
                                <select>
                                    <option>Month</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-port">
                            <div class="select"><span>Cruise Departure Port</span>
                                <select>
                                    <option>Cruise Departure Port</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-line">
                            <div class="select"><span>Cruise Line</span>
                                <select>
                                    <option>Cruise Line</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-medium awe-search">Search</button>
                        </div>
                    </div>
                </div>
                <div class="form-cn form-flight tab-pane" id="form-flight">
                    <h2>Where would you like to go?</h2>
                    <div class="form-search clearfix">
                        <div class="form-field field-from">
                            <label for="flight-from"><span>Flight from:</span> Airport</label>
                            <input name="flightfrom" id="flight-from" class="field-input" type="text">
                        </div>
                        <div class="form-field field-to">
                            <label for="flight-to"><span>To :</span> Country, Airpor</label>
                            <input id="flight-to" class="field-input" type="text">
                        </div>
                        <div class="form-field field-date">
                            <input id="dp1470067323757" class="field-input calendar-input hasDatepicker" placeholder="Departing" type="text">
                        </div>
                        <div class="form-field field-date">
                            <input id="dp1470067323758" class="field-input calendar-input hasDatepicker" placeholder="Returning" type="text">
                        </div>
                        <div class="form-field field-select field-adult">
                            <div class="select"><span>Adults</span>
                                <select>
                                    <option>Adults</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-children">
                            <div class="select"><span>Children</span>
                                <select>
                                    <option>Children</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-medium awe-search">Search</button>
                        </div>
                    </div>
                </div>
                <div class="form-cn form-package tab-pane" id="form-package">
                    <h2>Where would you like to go?</h2>
                    <ul class="form-radio">
                        <li>
                            <div class="radio-checkbox">
                                <input name="radio-1" id="radio-1" class="radio" type="radio">
                                <label for="radio-1">Flight + Hotel</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input name="radio-1" id="radio-2" class="radio" type="radio">
                                <label for="radio-2">Flight + Hotel +Car</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input name="radio-1" id="radio-3" class="radio" type="radio">
                                <label for="radio-3">Hotel +Car</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input name="radio-1" id="radio-4" class="radio" type="radio">
                                <label for="radio-4">Flight +Car</label>
                            </div>
                        </li>
                    </ul>
                    <div class="form-search clearfix">
                        <div class="form-field field-from">
                            <label for="filghtfrom"><span>Flight From:</span> Airport...</label>
                            <input id="filghtfrom" class="field-input" type="text">
                        </div>
                        <div class="form-field field-to">
                            <label for="flightto"><span>To:</span> Country, Airport</label>
                            <input id="flightto" class="field-input" type="text">
                        </div>
                        <div class="form-field field-date">
                            <input id="dp1470067323759" class="field-input calendar-input hasDatepicker" placeholder="Departing" type="text">
                        </div>
                        <div class="form-field field-date">
                            <input id="dp1470067323760" class="field-input calendar-input hasDatepicker" placeholder="Returning" type="text">
                        </div>
                        <div class="form-field field-select field-adults">
                            <div class="select"><span>Adults</span>
                                <select>
                                    <option>Adults</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-children">
                            <div class="select"><span>Children</span>
                                <select>
                                    <option>Children</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-medium awe-search">Search</button>
                        </div>
                    </div>
                </div>
                <div class="form-cn form-tour tab-pane" id="form-tour">
                    <h2>Where would you like to go?</h2>
                    <div class="form-search clearfix">
                        <div class="form-field field-select field-region">
                            <div class="select"><span>Region: <small>Wourldwide, africa..</small></span>
                                <select>
                                    <option>Africa</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-country">
                            <div class="select"><span>Country</span>
                                <select>
                                    <option>Country</option>
                                    <option>Viet Nam</option>
                                    <option>Thai Lan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-style">
                            <div class="select"><span>Tour Style</span>
                                <select>
                                    <option>Style One</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-medium awe-search">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>