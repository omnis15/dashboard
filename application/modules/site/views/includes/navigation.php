<?php
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		$linkedin = $contacts['linkedin'];
		$logo = $contacts['logo'];
		$building = $contacts['building'];
		$floor = $contacts['floor'];
		$location = $contacts['location'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
        $working_weekday = $contacts['working_weekday'];
		
		if(!empty($facebook))
		{
			//$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>
		
		<header class="header">
            <!-- START PAGE NAVIGATION -->
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="nav-holder">
                        
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-nav">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- Set your logo here  -->
                            <a id="" class="navbar-brand small" href="#"><img src="<?php echo base_url()."assets/img/logo-small.jpg";?>" alt="Ubiker"/></a>
                        </div>
                        
                        <!-- START NAVIGATION LINKS -->
                        <div class="nav-wrapper closed navbar-right">
                            <ul class="nav navbar-nav collapse navbar-collapse">
                                <!--<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Versions <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="">IOS</a></li>
                                        <li><a href="index.android.html">Android</a></li>
                                        <li><a href="index.desktop.html">Desktop</a></li>
                                    </ul>
                                </li>-->
                                <li><a href="#features">Features</a></li>
                                <li><a href="#download">Download</a></li>
                                <li><a href="#screenshots">Screenshots</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#reviews">Reviews</a></li>
                                        <li><a href="#social">Social</a></li>
                                        <li><a href="#updates">How it Works</a></li>
                                        <li><a href="#contacts">Contacts</a></li>
                                    </ul>
                                </li>
                                <!-- START CLOSE MENU BUTTON -->
                                <li>
                                    <button class="btn btn-nav btn-nav-close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </li>
                                <!-- END CLOSE MENU BUTTON -->
                            </ul>
                        </div>
                        <!-- END NAVIGATION LINKS -->
                        
                        <!-- START OPEN MENU BUTTON -->
                        <button class="btn btn-nav btn-nav-open" type="button">
                            <span class="btn-nav-text">Menu</span>
                            <span class="btn-nav-icon">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
                        <!-- END OPEN MENU BUTTON -->
                        
                    </div>
                </div>
            </nav>
            <!-- END PAGE NAVIGATION -->
            
        </header>
        