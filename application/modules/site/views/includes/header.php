		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- FAVICON -->
        <link type="image/x-icon" rel="shortcut icon" href="<?php echo base_url()."assets/img/biker.png";?>" />
        
        <!-- PAGE TITLE -->
        <title>Ubiker | <?php echo $title;?></title>
        
        <!-- FONTS -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        
        <!-- LIBS STYLESHEETS -->
        <link href="<?php echo base_url()."assets/themes/theme/";?>assets/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/themes/theme/assets/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url()."assets/themes/theme/";?>assets/libs/owl.carousel.2.0.0-beta.2.4/owl.carousel.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url()."assets/themes/theme/";?>assets/libs/video.js/dist/video-js.min.css" rel="stylesheet" type="text/css"/>
        
        <!-- PROMINENT STYLESHEETS -->
        <link href="<?php echo base_url()."assets/themes/theme/";?>assets/css/styles.css" rel="stylesheet" type="text/css"/>