<?php

$result = '';

//if users exist display them

if ($query->num_rows() > 0)
{	
	//get all administrators
	$administrators = $this->users_model->get_all_administrators();
	if ($administrators->num_rows() > 0)
	{
		$admins = $administrators->result();
	}
	
	else
	{
		$admins = NULL;
	}
	
	foreach ($query->result() as $row)
	{
		$post_id = $row->post_id;
		$blog_category_name = $row->blog_category_name;
		$blog_category_id = $row->blog_category_id;
		$post_title = $row->post_title;
		$web_name = $this->site_model->create_web_name($post_title);
		$post_status = $row->post_status;
		$post_views = $row->post_views;
		$image = base_url().'assets/images/posts/'.$row->post_image;
		$created_by = $row->created_by;
		$modified_by = $row->modified_by;
		$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', strip_tags($description)), 0, 20));
		$created = $row->created;
		$day = date('j',strtotime($created));
		$month = date('M',strtotime($created));
		//$created_on = date('jS M Y',strtotime($row->created));
		$created_on = date('M Y',strtotime($row->created));
		$post_video = $row->post_video;
		
		if(empty($post_video))
		{
			$image = '<img src="'.$image.'" alt="'.$post_title.'" class="pic"/>';
		}
		
		else
		{
			$image = '<div class="youtube" id="'.$post_video.'"></div>';
		}
		
		$categories = '';
		$count = 0;
		//get all administrators
			$administrators = $this->users_model->get_all_administrators();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
				
				if($admins != NULL)
				{
					foreach($admins as $adm)
					{
						$user_id = $adm->user_id;
						
						if($user_id == $created_by)
						{
							$created_by = $adm->first_name;
						}
					}
				}
			}
			
			else
			{
				$admins = NULL;
			}
		
			foreach($categories_query->result() as $res)
			{
				$count++;
				$category_name = $res->blog_category_name;
				$category_id = $res->blog_category_id;
				$category_web_name = $this->site_model->create_web_name($category_name);
				
				if($count == $categories_query->num_rows())
				{
					$categories .= '<li><a href="'.site_url().'blog/category/'.$category_web_name.'">'.$category_name.'</a></li>';
				}
				
				else
				{
					$categories .= '<li><a href="'.site_url().'blog/category/'.$category_web_name.'">'.$category_name.'</a></li>';
				}
			}
			$comments_query = $this->blog_model->get_post_comments($post_id);
			//comments
			$comments = 'No Comments';
			$total_comments = $comments_query->num_rows();
			if($total_comments == 1)
			{
				$title = 'comment';
			}
			else
			{
				$title = 'comments';
			}
			
			if($comments_query->num_rows() > 0)
			{
				$comments = '';
				foreach ($comments_query->result() as $row)
				{
					$post_comment_user = $row->post_comment_user;
					$post_comment_description = $row->post_comment_description;
					$date = date('jS M Y',strtotime($row->comment_created));
					
					$comments .= 
					'
						<div class="user_comment">
							<h5>'.$post_comment_user.' - '.$date.'</h5>
							<p>'.$post_comment_description.'</p>
						</div>
					';
				}
			}
			$result .= '
			
                            <!-- blog post image -->
                            <article class="post">
								<h6>&nbsp;</h6>
                                <div class="post-content nomgr">
									<div class="img">
										<div class="date">'.$created_on.'</div>
										<a href="'.site_url().'blog/'.$web_name.'">'.$image.'</a>
									</div>                              
									<h2><a href="'.site_url().'blog/'.$web_name.'">'.$post_title.'</a></h2>
									<div class="meta">
										<ul>
											<li><a href="#">Posted by Admin</a></li>
											'.$categories.'
											<li><a href="#">'.$total_comments.' '.$title.'</a></li>
										</ul>
									</div>    
									<p>'.$mini_desc.'</p>
									<p class="t-mgr10"><a href="'.site_url().'blog/'.$web_name.'">read more &rsaquo;</a></p>
                                        
        
                                </div>
                            </article>
                            <!--/ blog post image -->
            ';
        }
	}
	else
	{
		$result .= "There are no posts :-(";
	}
   
  ?> 
			<!-- inner banner -->
            <section class="bg-white inner-banner pages">

                <div class="container pad-container">
                    <div class="col-md-12">
                        <h1>Blog</h1>
                    </div>
                </div>

            </section>
                    
			
            <section>

                <div class="container pad-container2">
					<h6>&nbsp;</h6>
                    <div class="row">

                        <!-- blog posts -->
                        <div class="col-sm-8 content">
							<?php echo $result;?>
							
                            <!--/ pagination -->
                            <?php if(isset($links)){echo $links;}?>

                        </div>
                        <!--/ blog posts -->

                        <!-- sidebar -->
                        <div class="col-sm-4 sidebar">
							<?php echo $this->load->view('blog/includes/side_bar', '', TRUE);?>
                        </div>
                        <!--/ sidebar -->
                    </div>

                </div>

            </section>
            