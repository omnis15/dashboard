<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Map extends MX_Controller 
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('map_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}


	public function index()
	{
		# code...

		$data['title'] = 'Customers';
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('map_view', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);



	}



}
?>
