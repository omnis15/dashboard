<?php 
	
	$contacts = $this->site_model->get_contacts();
	$data['contacts'] = $contacts;
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$building = $contacts['building'];
		$floor = $contacts['floor'];
		$location = $contacts['location'];
		$phone = $contacts['phone'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>
<!doctype html>
<html class="fixed">
	<head>
        <?php echo $this->load->view('admin/includes/header', $data, TRUE); ?>
    </head>

	<body class="blank">

		<!-- Simple splash screen-->
		<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1><?php echo $company_name;?></h1><p><?php echo $building;?>, <?php echo $floor;?>, <?php echo $location;?> </p><p><?php echo $phone;?>, <?php echo $email;?></p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
		<!--[if lt IE 7]>
		<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

		<div class="color-line"></div>

		<div class="login-container">
		    <div class="row">
		        <div class="col-md-12">
		        	<?php
						$login_error = $this->session->userdata('login_error');
						$this->session->unset_userdata('login_error');
						
						if(!empty($login_error))
						{
							echo '<div class="alert alert-danger">'.$login_error.'</div>';
						}
					?>
		            <div class="text-center m-b-md">
		                <h3>LOGIN TO <?php echo strtoupper($company_name);?></h3>
		            </div>
		            <div class="hpanel">
		                <div class="panel-body">
		                        <form action="<?php echo site_url().$this->uri->uri_string();?>" method="post">
		                            <div class="form-group">
		                                <label class="control-label" for="username">Username</label>
		                                <input type="text" name="personnel_username" placeholder="tmartin" title="Please enter you username" required value="" name="username" id="username" class="form-control">
		                                <span class="help-block small">Your unique username to app</span>
		                            </div>
		                            <div class="form-group">
		                                <label class="control-label" for="password">Password</label>
		                                <input type="password" title="Please enter your password" placeholder="******" required name="personnel_password" value="" name="password" id="password" class="form-control">
		                                <span class="help-block small">Your strong password</span>
		                            </div>
		                            <!-- <div class="checkbox">
		                                <input type="checkbox" class="i-checks" checked>
		                                     Remember login
		                                <p class="help-block small">(if this is a private computer)</p>
		                            </div> -->
		                            <button class="btn btn-success btn-block">Login</button>
		                            <!-- <a class="btn btn-default btn-block" href="login.html#">Register</a> -->
		                        </form>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-md-12 text-center">
		            <strong><?php echo $company_name?></strong>  <br/> Designed by Omnis Limited <br/> <?php echo date('Y');?> Copyright
		        </div>
		    </div>
		</div>
		
		<!-- Vendor scripts -->
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery/dist/jquery.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery-ui/jquery-ui.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/slimScroll/jquery.slimscroll.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/metisMenu/dist/metisMenu.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/iCheck/icheck.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/sparkline/index.js"></script>

		<!-- App scripts -->
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>scripts/homer.js"></script>


	</body>
</html>
