<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('sms_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index() 
	{
		
		$where = 'sms.sms_delete = 0';
		$table = 'sms';


		$order = 'sms_status,created';
		$order_method = 'ASC';
		
		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'sms';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->sms_model->get_all_sms($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'SMS\'s';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$consumble_where = 'location.location_status = 1';
		$account_order = 'location.location_name';
		$account_table = 'location';
		$join = null;

		$account_query = $this->sms_model->get_items_list($account_table, $consumble_where, $account_order,$join);
		$rs8 = $account_query->result();
		$locations = '';
		foreach ($rs8 as $account_rs) :
			$location_id = $account_rs->location_id;
			$location_name = $account_rs->location_name;

			$locations .="<option value='".$location_id."'> ".$location_name." </option>";

		endforeach;

		$v_data['locations'] = $locations;

		$category_where = 'category_status = 1';
		$category_order = 'category_name';
		$category_table = 'sms_categories';
		$join = null;

		$category_query = $this->sms_model->get_items_list($category_table, $category_where, $category_order,$join);
		$rs8 = $category_query->result();
		$categories = '';
		foreach ($rs8 as $category_rs) :
			$category_id = $category_rs->category_id;
			$category_name = $category_rs->category_name;

			$categories .="<option value='".$category_id."'> ".$category_name." </option>";

		endforeach;

		$v_data['categories'] = $categories;


		$department_where = 'department_status = 1';
		$department_order = 'department_name';
		$department_table = 'departments';
		$join = null;

		$department_query = $this->sms_model->get_items_list($department_table, $department_where, $department_order,$join);
		$rs8 = $department_query->result();
		$departments = '';
		foreach ($rs8 as $department_rs) :
			$department_id = $department_rs->department_id;
			$department_name = $department_rs->department_name;

			$departments .="<option value='".$department_id."'> ".$department_name." </option>";

		endforeach;

		$v_data['departments'] = $departments;



		$data['content'] = $this->load->view('all_sms', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function allocate_sms($sms_id)
	{
		if (!empty($sms_id))
		{
			if($this->sms_model->add_sms_allocation($sms_id))
			{
				$this->session->set_userdata('success_message', 'Successfully allocated the sms');
				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not allocate sms. Please try again');
			}
		}

		redirect('sms');
	}

}
?>