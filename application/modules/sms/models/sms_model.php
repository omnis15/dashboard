<?php

class Sms_model extends CI_Model 
{	
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_sms($table, $where, $per_page, $page, $order = 'created', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_items_list($table, $where, $order,$join = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');
		
		return $query;
	}

	public function add_sms_allocation($sms_id)
	{
		$location_id = $this->input->post('location_id'.$sms_id);
		$department_id = $this->input->post('department_id'.$sms_id);
		$category_id = $this->input->post('category_id'.$sms_id);
		$personnel_id = $this->session->userdata('personnel_id');

		$insert_data  = array('location_id' =>$location_id ,'department_id' => $department_id,'category_id'=> $category_id,'sms_id'=>$sms_id,'created'=> date('Y-m-d'),'created_by'=>$personnel_id);

		if($this->db->insert('sms_allocation',$insert_data))
		{
				// update 

				$update_array = array('sms_status' => 2);
				$this->db->where('sms_id',$sms_id);
				$this->db->update('sms',$update_array);

				return TRUE;

		}
		else
		{
			return FALSE;
		}

	
	}
}
?>