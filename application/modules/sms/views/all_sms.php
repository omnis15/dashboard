<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Created</th>
						<th>Sender</th>
						<th>SMS</th>
						<th>Status</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$sms_id = $row->sms_id;
				$sms = $row->sms;
				$created = $row->created;
				$sms_status = $row->sms_status;
				$phone_number = $row->phone_number;
				//status
				if($sms_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($sms_status == 2)
				{
					$status = '<span class="label label-success">Allocated</span>';
					$button = '<a class="btn btn-sm btn-info" href="'.site_url().'customers/activate-customer/'.$sms_id.'" onclick="return confirm(\'Do you want to activate ?\');" title="Activate "><i class="fa fa-arrows"></i> Allocation</a>';
				}
				//create activated status display
				else if($sms_status == 1)
				{
					$status = '<span class="label label-default">Not Allocated</span>';
					
				}
				$button = '<a class="btn btn-sm btn-default" data-toggle="modal" data-target="#allocate_sms'.$sms_id.'" onclick="get_finders('.$sms_id.')"><i class="fa fa-arrows"></i> Allocate</a>
						<div class="modal fade" id="allocate_sms'.$sms_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header modal-success">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">SMS Allocation </h4>

									</div>
									'. form_open_multipart('allocate-sms/'.$sms_id, array("class" => "form-horizontal", "role" => "form")).'
									<div class="modal-body">
										<div class="row">
						                    <div class="col-md-12">
					                            <div class="col-lg-6 col-md-6 col-sm-6">
								                  <div class="form-group">
								                  	<label class="col-lg-3 control-label">Location: </label>
       			 									<div class="col-lg-9">
									                    <select id="location_id'.$sms_id.'" name="location_id'.$sms_id.'" class="form-control custom-select">
									                      <option value="">None - Please Select a location</option>
									                       	'.$locations.'
									                    </select>
									                </div>
								                  </div>
								                  <div class="form-group">
								                  	<label class="col-lg-3 control-label">Category: </label>
       			 									<div class="col-lg-9">
									                    <select id="category_id'.$sms_id.'" name="category_id'.$sms_id.'" class="form-control custom-select">
									                      <option value="">None - Please Select a category</option>
									                       	'.$categories.'
									                    </select>
									                </div>
								                  </div>
								                </div>
								                <div class="col-lg-6 col-md-6 col-sm-6">
								                  <div class="form-group">
								                  	<label class="col-lg-3 control-label">Department: </label>
       			 									<div class="col-lg-9">
									                    <select id="department_id'.$sms_id.'" name="department_id'.$sms_id.'" class="form-control custom-select">
									                      <option value="">None - Please Select a department</option>
									                       	'.$departments.'
									                    </select>
									                </div>
								                  </div>
								                </div>
						                    </div>
					                    </div>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-sm btn-success" >Allocate Message</button>
										<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
									</div>
									'.form_close().'
								</div>
							</div>
						</div>

						';
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$created.'</td>
						<td>'.$phone_number.'</td>
						<td>'.$sms.'</td>
						<td>'.$status.'</td>
						<td>'.$button.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no messages";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				
				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>
<script type="text/javascript">

	function get_finders(sms_id)
	{
		$("#location_id"+sms_id).customselect();
		$("#department_id"+sms_id).customselect();
		$("#category_id"+sms_id).customselect();
	}

	

</script>