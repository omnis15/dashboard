<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    <a class="closebox"><i class="fa fa-times"></i></a>
                </div>
                Dashboard information and statistics
            </div>
            <div class="panel-body h-400">
                <div class="row">
                    <div class="col-md-2 text-center" style="padding-top: 5%;">
                        <div class="small">
                            <i class="fa fa-bolt"></i> Messages Received
                        </div>
                        <div>
                            <h1 class="font-extra-bold m-t-xl m-b-xs">
                                <?php
                                $received_where = 'sms.sms_id IN (SELECT sms_id FROM sms_allocation) AND sms_delete = 0 AND sms.created = "'.date('Y-m-d').'" ';
                                $received_table = 'sms';

                                $total_received_sms = $this->dashboard_model->count_items($received_table, $received_where);

                                echo $total_received_sms;

                                ?>
                                
                            </h1>
                            <small>Messages received Today </small>

                        </div>
                        <div class="small m-t-xl">
                            <i class="fa fa-clock-o"></i> For  <?php echo date('jS M Y',strtotime(date('Y-m-d')));?>
                        </div>
                        
                    </div>
                    <div class="col-md-8">
                        <div class="text-center small">
                            <i class="fa fa-laptop"></i> Category Activity this week
                        </div>
                        <div class="flot-chart" >
                            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                            <?php
                                // Start date
                            $end_date = date('Y-m-d');
                            // End date
                            $year = date('Y');
                            $date = date ("Y-m-d", strtotime("-6 day", strtotime($end_date)));
                            
                            $dashboard_where = 'category_status = 1 ';
                            $dashboard_table = 'sms_categories';
                            $dashboard_select = '*';
                            $item_select  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select);
                            $parameters2 = '["Day"';
                            if($item_select->num_rows() > 0)
                            {
                                foreach ($item_select->result() as $key ) {
                                    # code...
                                    $category_id = $key->category_id;
                                    $category_name = $key->category_name;
                                    $parameters2 .= ',"'.$category_name.'"';

                                }
                            } 
                            $parameters2 .= ']';
                            $parameters = '';
                            while (strtotime($date) <= strtotime($end_date)) {
                                
                                  
                                    $checked_date = date('jS M',strtotime($date));
                                    $parameters .= '["'.$checked_date.'"';

                                    $dashboard_where = 'category_status = 1 ';
                                    $dashboard_table = 'sms_categories';
                                    $dashboard_select = '*';
                                    $item_select  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select);
                                    if($item_select->num_rows() > 0)
                                    {
                                        foreach ($item_select->result() as $key ) {
                                            # code...
                                            $category_idd = $key->category_id;
                                            $category_namee = $key->category_name;

                                            $items_where = 'sms_allocation.category_id = '.$category_idd.' AND  created = "'.$date.'" ';
                                            $items_table = 'sms_allocation';
                                            $total_items = $this->users_model->count_items($items_table, $items_where);

                                            $parameters .=','.$total_items.'';

                                        }
                                    }
                                    $parameters .= '],';
                                           

                                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));

                            }

                            ?>


                            <script type="text/javascript">
                              google.charts.load('current', {'packages':['corechart']});
                              google.charts.setOnLoadCallback(drawChart);

                              function drawChart() {
                                var data = google.visualization.arrayToDataTable([
                                  <?php echo $parameters2?>,
                                  <?php echo $parameters?>
                                ]);

                                // var data = google.visualization.arrayToDataTable([
                                //   ['Day', 'Sales', 'Expenses'],
                                //   ['2013',  1000,      400],
                                //   ['2014',  1170,      460],
                                //   ['2015',  660,       1120],
                                //   ['2016',  1030,      540]
                                // ]);
                                 

                                var options = {
                                  // title: 'Company Performance',
                                  hAxis: {title: 'Year (<?php echo $year?>)',  titleTextStyle: {color: '#333'}},
                                  vAxis: {minValue: 0},
                                  legend: {position: 'top', maxLines: 3},

                                };

                                var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                                chart.draw(data, options);
                              }
                            </script>
                            <div id="chart_div" style="width: 100%;height: 100%;"></div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center" style="padding-top: 5%;">
                        <div class="small">
                            <i class="fa fa-bolt"></i> Messages Sent
                        </div>
                        <div>
                            <h1 class="font-extra-bold m-t-xl m-b-xs">
                                <?php
                                $sent_where = 'created = "'.date('Y-m-d').'" ';
                                $sent_table = 'messages';

                                $total_sent_sms = $this->dashboard_model->count_items($sent_table, $sent_where);

                                echo $total_sent_sms;

                                ?>
                            </h1>
                            <small>Messages sent Today </small>
                        </div>
                        <div class="small m-t-xl">
                            <i class="fa fa-clock-o"></i> For  <?php echo date('jS M Y',strtotime(date('Y-m-d')));?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="panel-footer">
            <span class="pull-right">
                  You have two new messages from <a href="index.html">Monica Bolt</a>
            </span>
                Last update: 21.05.2015
            </div> -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="hpanel">
            <div class="panel-body text-center h-400">
                <div class="text-center small">
                    <i class="fa fa-laptop"></i> Categories Variance
                </div>

                <?php
                $categories = '';

                $dashboard_where = 'category_status = 1 ';
                $dashboard_table = 'sms_categories';
                $dashboard_select = '*';
                $item_select  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select);
                if($item_select->num_rows() > 0)
                {
                    foreach ($item_select->result() as $key ) {
                        # code...
                        $category_idd = $key->category_id;
                        $category_namee = $key->category_name;

                        $items_where = 'sms_allocation.category_id = '.$category_idd.' AND  created = "'.date('Y-m-d').'" ';
                        $items_table = 'sms_allocation';
                        $total_items = $this->users_model->count_items($items_table, $items_where);

                        $categories .=',["'.$category_namee.'", '.$total_items.']';

                    }
                }

                ?>
               <script type="text/javascript">


                  google.charts.load('current', {'packages':['corechart']});
                  google.charts.setOnLoadCallback(drawChart);

                  function drawChart() {

                    var data = google.visualization.arrayToDataTable([
                      ['Category', 'Messages']
                      <?php echo $categories;?>
                    ]);

                    var options = {
                        legend: {position: 'top', maxLines: 3},
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                    chart.draw(data, options);
                  }
                </script>
                <div id="piechart" style="width: 100%; height:400px;"></div>
            </div>
            
        </div>
    </div>
    
    <div class="col-lg-6">
        <div class="hpanel">
            <div class="panel-body text-center h-400">
                <div class="text-center small">
                    <i class="fa fa-laptop"></i> Departments Variance
                </div>
                 <?php
                $departments = '';

                $departments_where = 'department_status = 1 ';
                $departments_table = 'departments';
                $departments_select = '*';
                $item_select  = $this->dashboard_model->get_content($departments_table, $departments_where,$departments_select);
                if($item_select->num_rows() > 0)
                {
                    foreach ($item_select->result() as $key ) {
                        # code...
                        $department_id = $key->department_id;
                        $department_name = $key->department_name;

                        $items_where = 'sms_allocation.department_id = '.$department_id.' AND  created = "'.date('Y-m-d').'" ';
                        $items_table = 'sms_allocation';
                        $total_items = $this->dashboard_model->count_items($items_table, $items_where);

                        $departments .=',["'.$department_name.'", '.$total_items.']';

                    }
                }

                ?>
               <script type="text/javascript">
                  google.charts.load('current', {'packages':['corechart']});
                  google.charts.setOnLoadCallback(drawChart);

                  function drawChart() {

                    var data = google.visualization.arrayToDataTable([
                      ['Department', 'Messages']
                      <?php echo $departments;?>
                    ]);

                    var options = {
                        legend: {position: 'top', maxLines: 3},
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('departments_variance'));

                    chart.draw(data, options);
                  }
                </script>
                <div id="departments_variance" style="width: 100%; height:400px;"></div>
            </div>
            
        </div>
    </div>
    
</div>
<div class="row">
  
    <div class="col-lg-9">
        <div class="hpanel">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    <a class="closebox"><i class="fa fa-times"></i></a>
                </div>
                Top Ten Locations
            </div>
            <div class="panel-body">
                <?php

                    // title
                    $location_where = 'category_status = 1 ';
                    $location_table = 'sms_categories';
                    $location_select = '*';
                    $location_select  = $this->dashboard_model->get_content($location_table, $location_where,$location_select);
                    $location_categories = '';
                    if($location_select->num_rows() > 0)
                    {
                        foreach ($location_select->result() as $key ) {
                            # code...
                            $category_id = $key->category_id;
                            $category_name = $key->category_name;
                            $location_categories .= '"'.$category_name.'",';

                        }
                    } 

                    // items

                    $dashboard_where = 'location_status = 1 ';
                    $dashboard_table = 'location';
                    $dashboard_select = '*';
                    $dashboard_group = NULL;
                    $dashboard_limit= 10;

                    $item_select  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select,$dashboard_group,$dashboard_limit);
                    $location_parameters ='';
                    if($item_select->num_rows() > 0)
                    {
                        foreach ($item_select->result() as $key ) {
                            # code...
                            $location_id = $key->location_id;
                            $location_name = $key->location_name;

                            $location_parameters .='["'.$location_name.'"';


                            if($location_select->num_rows() > 0)
                            {
                                foreach ($location_select->result() as $key ) {
                                    # code...
                                    $category_id = $key->category_id;
                                    $category_name = $key->category_name;

                                    $items_where = 'sms_allocation.category_id = '.$category_id.' AND sms_allocation.location_id = '.$location_id.' AND  created = "'.date('Y-m-d').'" ';
                                    $items_table = 'sms_allocation';
                                    $total_items_count = $this->users_model->count_items($items_table, $items_where);

                                    $location_parameters .=','.$total_items_count.'';
                                }
                            }
                            $location_parameters .=',""],';

                        }
                    }



                ?>
               <script type="text/javascript">
                google.charts.load("current", {packages:['corechart']});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                  var data = google.visualization.arrayToDataTable([
                    ['Locations', <?php echo $location_categories;?>  { role: 'annotation' } ],
                    <?php echo $location_parameters;?>
                  ]);

                  // var data = google.visualization.arrayToDataTable([
                  //   ['Genre', 'Fantasy & Sci Fi', 'Romance', 'Mystery/Crime', 'General',
                  //    'Western', 'Literature', { role: 'annotation' } ],
                  //   ['2010', 10, 24, 20, 32, 18, 5, ''],
                  //   ['2020', 16, 22, 23, 30, 16, 9, ''],
                  //   ['2030', 28, 19, 29, 30, 12, 13, '']
                  // ]);

                 



                  var view = new google.visualization.DataView(data);
                  // view.setColumns([0, 1,2,3,
                  //                  { calc: "stringify",
                  //                    sourceColumn: 1,
                  //                    type: "string",
                  //                    role: "annotation" },
                  //                  4]);

                  var options = {
                        legend: { position: 'top', maxLines: 3 },
                        bar: { groupWidth: '75%' },
                        isStacked: true,
                      };
                  var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
                  chart.draw(view, options);
              }
              </script>
              <div id="columnchart_values" style="width: 100%; height: 350px;"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="hpanel">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    <a class="closebox"><i class="fa fa-times"></i></a>
                </div>
                Messaging Activity
            </div>
            <div class="panel-body list">
                <div class="panel-title">Last Activity</div>
                <div class="list-item-container">
                    <div class="list-item">
                        <h3 class="no-margins font-extra-bold text-success"><?php echo number_format($total_sent_sms,0);?></h3>
                        <small>Messages Sent</small>
                        <div class="pull-right font-bold">98% <i class="fa fa-level-up text-success"></i></div>
                    </div>
                    <div class="list-item">
                        <h3 class="no-margins font-extra-bold text-success"><?php echo number_format($total_received_sms,0);?></h3>
                        <small>Messages Received</small>
                        <div class="pull-right font-bold">44% <i class="fa fa-level-up text-success"></i></div>
                    </div>
                    <div class="list-item">
                        <?php
                        $complaint_where = 'sms_categories.category_id = sms_allocation.category_id AND sms_allocation.created ="'.date('Y-m-d').'" AND sms_categories.category_name = "Complaint" ';
                        $complaint_table = 'sms_allocation,sms_categories';
                        $total_complaint = $this->users_model->count_items($complaint_table, $complaint_where);
                        ?>
                        <h3 class="no-margins font-extra-bold text-color3"><?php echo number_format($total_complaint,0);?></h3>
                        <small>Complaints</small>
                        <div class="pull-right font-bold">13% <i class="fa fa-level-down text-color3"></i></div>
                    </div>
                    <div class="list-item">
                        <?php
                        $appraisal_where = 'sms_categories.category_id = sms_allocation.category_id AND sms_allocation.created ="'.date('Y-m-d').'" AND sms_categories.category_name = "Appraisal" ';
                        $appraisal_table = 'sms_allocation,sms_categories';
                        $total_appraisal = $this->users_model->count_items($appraisal_table, $appraisal_where);
                        ?>
                        <h3 class="no-margins font-extra-bold text-color3"><?php echo number_format($total_appraisal,0);?></h3>
                        <small>Appraisals</small>
                        <div class="pull-right font-bold">22% <i class="fa fa-bolt text-color3"></i></div>
                    </div>
                    <div class="list-item">
                        <?php
                        $info_where = 'sms_categories.category_id = sms_allocation.category_id AND sms_allocation.created ="'.date('Y-m-d').'" AND sms_categories.category_name = "Info" ';
                        $info_table = 'sms_allocation,sms_categories';
                        $total_info = $this->users_model->count_items($info_table, $info_where);
                        ?>
                        <h3 class="no-margins font-extra-bold text-color3"><?php echo number_format($total_info,0);?></h3>
                        <small>Info</small>
                        <div class="pull-right font-bold">22% <i class="fa fa-bolt text-color3"></i></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
		