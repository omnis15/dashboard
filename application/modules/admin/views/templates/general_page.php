<?php 
	
	$contacts = $this->site_model->get_contacts();
	$data['contacts'] = $contacts;
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$building = $contacts['building'];
		$floor = $contacts['floor'];
		$location = $contacts['location'];
		$phone = $contacts['phone'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>
<!doctype html>
<html >
	<head>
        <?php echo $this->load->view('admin/includes/header', $data, TRUE); ?>
    </head>

	<body class="fixed-navbar fixed-sidebar">
    	<input type="hidden" id="base_url" value="<?php echo site_url();?>">
    	<input type="hidden" id="config_url" value="<?php echo site_url();?>">

		<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1><?php echo $company_name;?></h1><p><?php echo $building;?>, <?php echo $floor;?>, <?php echo $location;?> </p><p><?php echo $phone;?>, <?php echo $email;?></p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
    	<!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->

        <!-- header navigation -->
        <?php echo $this->load->view('admin/includes/top_navigation', $data, TRUE); ?>
        <!-- end of header navigation -->

        <?php echo $this->load->view('admin/includes/sidebar', $data, TRUE); ?>
    	<!-- Main Wrapper -->

    	<div id="wrapper">
    		<div class="content animate-panel">
		    	<?php echo $content;?>
		    </div>

		    <!-- Right sidebar -->
		    <div id="right-sidebar" class="animated fadeInRight">

		        <div class="p-m">
		            <button id="sidebar-close" class="right-sidebar-toggle sidebar-button btn btn-default m-b-md"><i class="pe pe-7s-close"></i>
		            </button>
		            <div>
		                <span class="font-bold no-margins"> Analytics </span>
		                <br>
		                <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.</small>
		            </div>
		            <div class="row m-t-sm m-b-sm">
		                <div class="col-lg-6">
		                    <h3 class="no-margins font-extra-bold text-success">300,102</h3>

		                    <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
		                </div>
		                <div class="col-lg-6">
		                    <h3 class="no-margins font-extra-bold text-success">280,200</h3>

		                    <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
		                </div>
		            </div>
		            <div class="progress m-t-xs full progress-small">
		                <div style="width: 25%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" role="progressbar"
		                     class=" progress-bar progress-bar-success">
		                    <span class="sr-only">35% Complete (success)</span>
		                </div>
		            </div>
		        </div>
		        <div class="p-m bg-light border-bottom border-top">
		            <span class="font-bold no-margins"> Social talks </span>
		            <br>
		            <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.</small>
		            <div class="m-t-md">
		                <div class="social-talk">
		                    <div class="media social-profile clearfix">
		                        <a class="pull-left">
		                            <img src="images/a1.jpg" alt="profile-picture">
		                        </a>

		                        <div class="media-body">
		                            <span class="font-bold">John Novak</span>
		                            <small class="text-muted">21.03.2015</small>
		                            <div class="social-content small">
		                                Injected humour, or randomised words which don't look even slightly believable.
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="social-talk">
		                    <div class="media social-profile clearfix">
		                        <a class="pull-left">
		                            <img src="images/a3.jpg" alt="profile-picture">
		                        </a>

		                        <div class="media-body">
		                            <span class="font-bold">Mark Smith</span>
		                            <small class="text-muted">14.04.2015</small>
		                            <div class="social-content">
		                                Many desktop publishing packages and web page editors.
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="social-talk">
		                    <div class="media social-profile clearfix">
		                        <a class="pull-left">
		                            <img src="images/a4.jpg" alt="profile-picture">
		                        </a>

		                        <div class="media-body">
		                            <span class="font-bold">Marica Morgan</span>
		                            <small class="text-muted">21.03.2015</small>

		                            <div class="social-content">
		                                There are many variations of passages of Lorem Ipsum available, but the majority have
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="p-m">
		            <span class="font-bold no-margins"> Sales in last week </span>
		            <div class="m-t-xs">
		                <div class="row">
		                    <div class="col-xs-6">
		                        <small>Today</small>
		                        <h4 class="m-t-xs">$170,20 <i class="fa fa-level-up text-success"></i></h4>
		                    </div>
		                    <div class="col-xs-6">
		                        <small>Last week</small>
		                        <h4 class="m-t-xs">$580,90 <i class="fa fa-level-up text-success"></i></h4>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="col-xs-6">
		                        <small>Today</small>
		                        <h4 class="m-t-xs">$620,20 <i class="fa fa-level-up text-success"></i></h4>
		                    </div>
		                    <div class="col-xs-6">
		                        <small>Last week</small>
		                        <h4 class="m-t-xs">$140,70 <i class="fa fa-level-up text-success"></i></h4>
		                    </div>
		                </div>
		            </div>
		            <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.
		                Many desktop publishing packages and web page editors.
		            </small>
		        </div>

		    </div>

		    <!-- Footer-->
		    <footer class="footer">
		        <span class="pull-right">
		            <?php echo $company_name;?>
		        </span>
		        Company 2015-2020
		    </footer>

		</div>
		
       <!-- Vendor scripts -->
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery/dist/jquery.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery-ui/jquery-ui.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/slimScroll/jquery.slimscroll.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery-flot/jquery.flot.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery-flot/jquery.flot.resize.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery-flot/jquery.flot.pie.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/flot.curvedlines/curvedLines.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery.flot.spline/index.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/metisMenu/dist/metisMenu.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/iCheck/icheck.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/peity/jquery.peity.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/sparkline/index.js"></script>
		<script src="<?php echo base_url()."assets/themes/custom_select/";?>jquery-customselect.js"></script>
        <link href="<?php echo base_url()."assets/themes/custom_select/";?>jquery-customselect.css" rel="stylesheet" />

		<!-- App scripts -->
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>scripts/homer.js"></script>
		<script src="<?php echo base_url()."assets/themes/ubiker/";?>scripts/charts.js"></script>

		<script>
			

		    $(function () {

		        /**
		         * Flot charts data and options
		         */
		        var data1 = [ [0, 55], [1, 48], [2, 40], [3, 36], [4, 40], [5, 60], [6, 50], [7, 51] ];
		        var data2 = [ [0, 56], [1, 49], [2, 41], [3, 38], [4, 46], [5, 67], [6, 57], [7, 59] ];

		        var chartUsersOptions = {
		            series: {
		                splines: {
		                    show: true,
		                    tension: 0.4,
		                    lineWidth: 1,
		                    fill: 0.4
		                },
		            },
		            grid: {
		                tickColor: "#f0f0f0",
		                borderWidth: 1,
		                borderColor: 'f0f0f0',
		                color: '#6a6c6f'
		            },
		            colors: [ "#62cb31", "#efefef"],
		        };

		        $.plot($("#flot-line-chart"), [data1, data2], chartUsersOptions);

		        /**
		         * Flot charts 2 data and options
		         */
		        var chartIncomeData = [
		            {
		                label: "line",
		                data: [ [1, 10], [2, 26], [3, 16], [4, 36], [5, 32], [6, 51] ]
		            }
		        ];

		        var chartIncomeOptions = {
		            series: {
		                lines: {
		                    show: true,
		                    lineWidth: 0,
		                    fill: true,
		                    fillColor: "#64cc34"

		                }
		            },
		            colors: ["#62cb31"],
		            grid: {
		                show: false
		            },
		            legend: {
		                show: false
		            }
		        };

		        $.plot($("#flot-income-chart"), chartIncomeData, chartIncomeOptions);



		    });

		</script>
		            
	</body>
</html>
