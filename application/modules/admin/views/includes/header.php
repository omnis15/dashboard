<?php


?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Page title -->
<title><?php echo $contacts['company_name'];?> | <?php echo $title;?></title>

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

<!-- Vendor styles -->
<!-- <link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>vendor/fontawesome/css/font-awesome.css" /> -->
<link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.css">

<link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>vendor/metisMenu/dist/metisMenu.css" />
<link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>vendor/animate.css/animate.css" />
<link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>vendor/bootstrap/dist/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>vendor/datatables.net-bs/css/dataTables.bootstrap.min.css" />

<!-- App styles -->
<link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
<link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>fonts/pe-icon-7-stroke/css/helper.css" />
<link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>styles/style.css">

<script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script> <!-- jQuery -->