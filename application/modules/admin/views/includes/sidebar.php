<?php
	$personnel_id = $this->session->userdata('personnel_id');
	
	if($personnel_id == 0)
	{
		$parents = $this->sections_model->all_parent_sections('section_position');
	}
	
	else
	{
		$personnel_roles = $this->personnel_model->get_personnel_roles($personnel_id);
		
		$parents = $personnel_roles;
	}
	$children = $this->sections_model->all_child_sections();
	
	$sections = '';
	
	if($parents->num_rows() > 0)
	{
		foreach($parents->result() as $res)
		{
			$section_parent = $res->section_parent;
			$section_id = $res->section_id;
			$section_name = $res->section_name;
			$section_icon = $res->section_icon;
			
			if($section_parent == 0)
			{
				$web_name = strtolower($this->site_model->create_web_name($section_name));
				$link = site_url().$web_name;
				$section_children = $this->admin_model->check_children($children, $section_id, $web_name);
				$total_children = count($section_children);
				
				if($total_children == 0)
				{
					if($title == $section_name)
					{
						$sections .= '<li >';
					}
					
					else
					{
						$sections .= '<li>';
					}
					$sections .= '
						<a href="'.$link.'">
							<i class="fa fa-'.$section_icon.'" aria-hidden="true"></i>
							<span>'.$section_name.'</span>
						</a>
					</li>
					';
				}
				
				else
				{
					if($title == $section_name)
					{
						$sections .= '<li>';
					}
					
					else
					{
						$sections .= '<li>';
					}
					$sections .= '
						<a>
							<i class="fa fa-'.$section_icon.'" aria-hidden="true"></i>
							<span>'.$section_name.'</span>
						</a>
						<ul class="nav nav-second-level">';
					
					//children
					for($r = 0; $r < $total_children; $r++)
					{
						$name = $section_children[$r]['section_name'];
						$link = $section_children[$r]['link'];
						
						$sections .= '
							<li>
								<a href="'.$link.'">
									 '.$name.'
								</a>
							</li>
						';
					}
					
					$sections .= '
					</ul></li>
					';
				}
			}
			
			else
			{
				//get parent section
				$parent_query = $this->sections_model->get_section($section_parent);
				
				$parent_row = $parent_query->row();
				$parent_name = $parent_row->section_name;
				$section_icon = $parent_row->section_icon;
				
				$web_name = strtolower($this->site_model->create_web_name($parent_name));
				$link = site_url().$web_name.'/'.strtolower($this->site_model->create_web_name($section_name));
				
				$sections .= '
				<li>
					<a href="'.$link.'">
						<i class="fa fa-'.$section_icon.'" aria-hidden="true"></i>
						<span>'.$section_name.'</span>
					</a>
				</li>
				';
			}
		}
	}
	
?>


<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">
          

            <div class="stats-label text-color">

                <span class="font-extra-bold font-uppercase">
                	<?php 
					//salutation
					if(date('a') == 'am')
					{
						echo 'Good morning, ';
					}
					
					else if((date('H') >= 12) && (date('H') < 17))
					{
						echo 'Good afternoon, ';
					}
					
					else
					{
						echo 'Good evening, ';
					}
					echo $this->session->userdata('first_name');
					
					?>
                </span>

                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <small class="text-muted"><?php echo $this->session->userdata('branch_code');?> <b class="caret"></b></small>
                    </a>
                    <ul class="dropdown-menu animated flipInX m-t-xs">
                        <li><a href="#">Contacts</a></li>
                        <li><a href="#">Profile</a></li>
                        <li><a href="#">Analytics</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url()."logout-admin";?>">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <ul class="nav" id="side-menu">
        	<?php echo $sections;?>
           

        </ul>
    </div>
</aside>				
                