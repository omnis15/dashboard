<div class="list-block">
	<ul>
		<li>
			<a class="item-link smart-select" data-searchbar-placeholder="Search fruits" data-searchbar="true" href="#">
				<select name="fruits">
					<option selected="" value="apple">Apple</option>
					<option value="pineapple">Pineapple</option>
					<option value="pear">Pear</option>
					<option value="orange">Orange</option>
					<option value="melon">Melon</option>
					<option value="peach">Peach</option>
					<option value="banana">Banana</option>
				</select>
				<div class="item-content">
					<div class="item-inner">
						<div class="item-title">Fruit</div>
						<div class="item-after">Apple</div>
					</div>
				</div>
			</a>
		</li>
		<li>
			<a class="item-link smart-select" data-searchbar-placeholder="Search cars" data-searchbar="true" href="#">
				<select name="car">
					<option value="mercedes">Mercedes</option>
					<option value="bmw">BMW</option>
					<option value="volvo">Volvo</option>
					<option selected="" value="vw">Volkswagen</option>
					<option value="toyota">Toyota</option>
					<option value="cadillac">Cadillac</option>
					<option value="nissan">Nissan</option>
					<option value="mazda">Mazda</option>
					<option value="ford">Ford</option>
					<option value="chrysler">Chrysler</option>
					<option value="dodge">Dodge</option>
				</select>
				<div class="item-content">
					<div class="item-inner">
						<div class="item-title">Car</div>
						<div class="item-after">Volkswagen</div>
					</div>
				</div>
			</a>
		</li>
	</ul>
</div>
