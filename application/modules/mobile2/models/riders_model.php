<?php
define( 'API_ACCESS_KEY', 'AIzaSyA7jKwgBQ3JBSE7-pCmaJQDihl22QTEOwE');

class Riders_model extends CI_Model 
{
	public function send_push_notification($to, $title, $message, $source = NULL)
	{
		// API access key from Google API's Console
		// replace API
		
		$registrationIds = array($to);
		
		$msg = array
		(
			'message' => $message,
			'title' => $title,
			'vibrate' => 1,
			'sound' => 1,
			'additionalData' => $source
			// you can also add images, additionalData
		);
		
		$fields = array
		(
			'registration_ids' => $registrationIds,
			'data' => $msg
		);
		
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		
		$result = curl_exec($ch );
		
		curl_close( $ch );
		
		return $result;
	}
	
	public function notify_riders($message_title = 'Ubiker', $message = NULL)
	{
		if($message != NULL)
		{
			//get riders
			$where = 'personnel_status.personnel_id = personnel.personnel_id AND personnel_status.personnel_status_status = 1 AND personnel_status.personnel_status_availability = 1 AND personnel.registration_id IS NOT NULL';
			$this->db->where($where);
			$query = $this->db->get('personnel, personnel_status');
			
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $res)
				{
					$to = $res->registration_id;
					$title = $message_title;
					$result = $this->send_push_notification($to, $title, $message);
				}
			}
		}
	}
}
?>