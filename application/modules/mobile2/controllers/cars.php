<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cars extends MX_Controller 
{
	var $battery_path;
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) 
		{
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}

		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') 
		{
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); 
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
			exit(0);
		}
		$this->load->model('cars_model');
		$this->battery_path = realpath(APPPATH . '../assets/batteries');
		$this->battery_location = base_url().'assets/batteries/';
	}
	
	/*
	*
	*	Default action is to go to the home page
	*
	*/
	public function get_car_makes()
	{
		$query = $this->cars_model->get_car_makes();
		//echo $query->num_rows();
		$response['result'] = $query->result();
		$response['message'] = 'success';
		echo json_encode($response);
	}
	
	public function get_car_models()
	{
		$this->form_validation->set_rules('brand_name', 'Car Model', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$brand_name = $this->input->post('brand_name');
			$query = $this->cars_model->get_car_models($brand_name);
			$response['result'] = $query->result();
			$response['message'] = 'success';
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = validation_errors();
		}
		echo json_encode($response);
	}
	
	public function upload_battery_image($customer_id = 1)
	{
		$this->load->library('image_lib');
		if(!empty($_FILES['file']['tmp_name']))
		{
			$battery_path = $this->battery_path;
			
			//Upload image
			$new_image_name = md5(date('Y-m-d H:i:s')).".jpg";
			if(move_uploaded_file($_FILES["file"]["tmp_name"], $this->battery_path."/".$new_image_name))
			{
				//resize the image
				$resize['width'] = 500;
				$resize['height'] = 500;
				$master_dim = 'width';
				
				$resize_conf = array(
						'source_image'  => $this->battery_path."/".$new_image_name, 
						'width' => $resize['width'],
						'height' => $resize['height'],
						'master_dim' => $master_dim,
						'maintain_ratio' => TRUE
					);
	
				// initializing
				$this->image_lib->initialize($resize_conf);
				// do it!
				if ( ! $this->image_lib->resize())
				{
					$v_data['response'] = 'failure';
					$v_data['message'] =  $this->image_lib->display_errors();
				}
				else
				{
					$width = $height = 150;
					$resize_conf2 = array(
						'source_image'  => $this->battery_path."/".$new_image_name,
						'new_image'     => $this->battery_path."/thumbnail_".$new_image_name,
						'create_thumb'  => FALSE,
						'width' => $width,
						'height' => $height,
						'maintain_ratio' => true,
					);
					
					$this->image_lib->initialize($resize_conf2);
					 
					 if ( ! $this->image_lib->resize())
					{
						$v_data['response'] = 'failure';
						$v_data['message'] =  $this->image_lib->display_errors();
					}
					
					else
					{
							$v_data['response'] = 'success';
							$v_data['message'] =  $this->battery_location.$new_image_name;
						//update db
						/*$update_data['client_image'] = $new_image_name;
						$update_data['client_thumb'] = "thumbnail_".$new_image_name;
						
						$this->db->where('client_id', $this->client_id);
						
						if($this->db->update('client', $update_data))
						{
							$v_data['response'] = 'success';
							$v_data['message'] =  $this->profile_image_location.'/'.$new_image_name;
						}
						
						else
						{
							$v_data['response'] = 'fail';
							$v_data['message'] =  'Unable to set your profile image. Please try again';
						}*/
					}
				}
			}
		}
		
		//unable to upload image
		else
		{
			$v_data['response'] = 'failure';
			$v_data['message'] = 'Unable to upload image. Please try again';
		}
		
		echo json_encode($v_data);
	}
	
}