<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('customers/customers_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('drivers/driver_model');
		$this->load->model('admin/admin_model');
		$this->load->model('accounts/ride_model');
		$this->load->model('vehicles/vehicle_model');
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index($order = 'customer_first_name', $order_method = 'ASC') 
	{
		$where = 'customer.customer_type_id = customer_type.customer_type_id';
		$table = 'customer, customer_type';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/customers/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_customers($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//drivers
		$all_drivers = $this->driver_model->get_drivers();
		$drivers = '';
		
		if($all_drivers->num_rows() > 0)
		{
			foreach ($all_drivers->result() as $row) :
				$personnel_id = $row->personnel_id;
				$personnel_number = $row->personnel_number;
				$personnel_fname = $row->personnel_fname;
				$personnel_onames = $row->personnel_onames;
				$personnel_phone = $row->personnel_phone;
				
				//$name = $personnel_first_name.' '.$personnel_surname.' | '.$personnel_phone." | ".$personnel_number;
				$name = $personnel_fname.' '.$personnel_onames;
				$drivers .= "<option value='".$personnel_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data["drivers"] = $drivers;
		
		//customers
		$all_customers = $this->customers_model->get_customers();
		$customers = '';
		
		if($all_customers->num_rows() > 0)
		{
			foreach ($all_customers->result() as $row) :
				$customer_id = $row->customer_id;
				$customer_first_name = $row->customer_first_name;
				$customer_number = $row->customer_number;
				$customer_surname = $row->customer_surname;
				$customer_phone = $row->customer_phone;
				$company_status = $row->company_status;
				
				if($company_status == 1)
				{
					$customer_surname = '';
				}
				//$name = $customer_first_name.' '.$customer_surname.' | '.$customer_phone." | ".$customer_number;
				$name = $customer_first_name.' '.$customer_surname;
				$customers .= "<option value='".$customer_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data["customers"] = $customers;
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		//ride statuses
		$ride_statuses = $this->ride_model->get_ride_statuses();
		$statuses = '';
		
		if($ride_statuses->num_rows() > 0)
		{
			foreach ($ride_statuses->result() as $row) :
				$ride_status_id = $row->ride_status_id;
				$ride_status_name = $row->ride_status_name;
				
				$name = $ride_status_name;
				$statuses .= "<option value='".$ride_status_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data['statuses'] = $statuses;
		
		//vehicles
		$vehicles_query = $this->ride_model->get_vehicles();
		$vehicles = '';
		
		if($vehicles_query->num_rows() > 0)
		{
			foreach ($vehicles_query->result() as $row) :
				$vehicle_id = $row->vehicle_id;
				$vehicle_plate = $row->vehicle_plate;
				
				$name = $vehicle_plate;
				$vehicles .= "<option value='".$vehicle_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data['vehicles'] = $vehicles;
		
		$data['title'] = 'Customers';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('accounts/customers/all_customers', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function add_ride($customer_id)
	{
		//form validation rules
		$this->form_validation->set_rules('ride_date', 'Ride Date', 'required|xss_clean');
		//$this->form_validation->set_rules('customer_id', 'Customer', 'required|xss_clean');
		$this->form_validation->set_rules('personnel_id', 'Driver', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_id', 'Vehicle', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('ride_distance', 'Distance', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('ride_duration', 'Duration', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('ride_status_id', 'Duration', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('from_route', 'Pick Up', 'required|xss_clean');
		$this->form_validation->set_rules('to_route', 'Drop Off', 'required|xss_clean');
		$this->form_validation->set_rules('end_time', 'End Time', 'required|xss_clean');
		$this->form_validation->set_rules('start_time', 'Start Time', 'required|xss_clean');
		$this->form_validation->set_rules('base_kms', 'Base Kms', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('destination_kms', 'Destination Kms', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('picking_kms', 'Picking Kms', 'numeric|required|xss_clean');
		$this->form_validation->set_rules('start_kms', 'Start Kms', 'numeric|required|xss_clean');
		$this->form_validation->set_rules('passenger', 'Passenger', 'xss_clean');
		$this->form_validation->set_rules('approved_by', 'Approved By', 'xss_clean');
		$this->form_validation->set_rules('total_waiting_time', 'Total Waiting Time', 'numeric|required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->ride_model->add_ride($customer_id))
			{
				$this->session->set_userdata('success_message', 'Vehicle added successfully');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Vehicle. Please try again');
			}
		}
		
		redirect('accounts/customers/view-rides/'.$customer_id);
	}
	
	function rides_template()
	{
		//export products template in excel 
		$this->ride_model->rides_template();
	}
	//do the customers import
	function do_rides_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->ride_model->import_csv_rides($this->csv_path);
				
				if($response == FALSE)
				{
					$this->session->set_userdata('error_message', 'Something went wrong. Please try again.');
				}
				
				else
				{
					if($response['check'])
					{
						$this->session->set_userdata('success_message', $response['response']);
					}
					
					else
					{
						$this->session->set_userdata('error_message', $response['response']);
					}
				}
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Please select a file to import.');
			}
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Please select a file to import.');
		}
		
		redirect('accounts/customers');
	}
	
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function view_rides($customer_id, $order = 'ride_date', $order_method = 'DESC') 
	{
		$where = 'ride.ride_status_id = ride_status.ride_status_id AND ride.vehicle_id = vehicle.vehicle_id AND ride.personnel_id = personnel.personnel_id AND ride.customer_id = '.$customer_id;
		// var_dump($where); die();
		$table = 'ride, ride_status, vehicle, personnel';
		//pagination
		$segment = 7;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/customers/view-rides/'.$customer_id.'/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_customers($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//drivers
		$all_drivers = $this->driver_model->get_drivers();
		$drivers = '';
		
		if($all_drivers->num_rows() > 0)
		{
			foreach ($all_drivers->result() as $row) :
				$personnel_id = $row->personnel_id;
				$personnel_number = $row->personnel_number;
				$personnel_fname = $row->personnel_fname;
				$personnel_onames = $row->personnel_onames;
				$personnel_phone = $row->personnel_phone;
				
				//$name = $personnel_first_name.' '.$personnel_surname.' | '.$personnel_phone." | ".$personnel_number;
				$name = $personnel_fname.' '.$personnel_onames;
				$drivers .= "<option value='".$personnel_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data["drivers"] = $drivers;
		
		$v_data["customer_id"] = $customer_id;
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		//ride statuses
		$ride_statuses = $this->ride_model->get_ride_statuses();
		$statuses = '';
		
		if($ride_statuses->num_rows() > 0)
		{
			foreach ($ride_statuses->result() as $row) :
				$ride_status_id = $row->ride_status_id;
				$ride_status_name = $row->ride_status_name;
				
				$name = $ride_status_name;
				$statuses .= "<option value='".$ride_status_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data['statuses'] = $statuses;
		
		//vehicles
		$vehicles_query = $this->ride_model->get_vehicles();
		$vehicles = '';
		
		if($vehicles_query->num_rows() > 0)
		{
			foreach ($vehicles_query->result() as $row) :
				$vehicle_id = $row->vehicle_id;
				$vehicle_plate = $row->vehicle_plate;
				
				$name = $vehicle_plate;
				$vehicles .= "<option value='".$vehicle_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data['vehicles'] = $vehicles;
		$v_data['customer_details'] = $this->customers_model->get_customer($customer_id);
		
		$data['title'] = 'Rides';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('accounts/customers/rides', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function cust_invoice($customer_id) 
	{

		//form validation rules
		$this->form_validation->set_rules('start_date', 'Start Date', 'required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_date', 'Invoice Date', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->customers_model->add_invoice($customer_id))
			{
				$this->session->set_userdata('success_message', 'Invoice Created  successfully');
				redirect('accounts/customers');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Invoice. Please try again');
			}
		}
		
		//open the add new vehicle
		
		$data['title'] = 'Generate Invoice';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $this->customers_model->get_invoices($customer_id);
		$v_data['customer_id'] = $customer_id;
		$v_data['page'] = 20;
		$v_data['owners'] = $this->customers_model->get_personnel_type(4);
		$v_data['drivers'] = $this->customers_model->get_personnel_type(3);
		$data['content'] = $this->load->view('accounts/customers/generate_invoice', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function view_cust_rides($invoice_id,$customer_id) 
	{

	
		//open the add new vehicle
		
		$data['title'] = 'Customer Rides Based On Invoice';

		$v_data['title'] = $data['title'];
		$v_data['query'] = $this->customers_model->get_cust_rides($invoice_id,$customer_id);
		$v_data['customer_id'] = $customer_id;
		$v_data['invoice_id'] = $invoice_id;
	
		$data['content'] = $this->load->view('accounts/customers/customer_rides', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function calculate_ride_distance($customer_id, $start_kms, $picking_kms, $destination_kms, $base_kms)
	{
		$ride_distance = $this->ride_model->calculate_ride_distance($start_kms, $picking_kms, $destination_kms, $base_kms, $customer_id);
		
		$result['ride_distance'] = $ride_distance;
		
		echo json_encode($result);
	}
	public function cust_payment($customer_id) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('paid_by', 'Paid By', 'required|xss_clean');
		
		if(!empty($payment_method))
		{
			if($payment_method == 1)
			{
				// check for cheque number if inserted
				$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');
			
			}
			else if($payment_method == 5)
			{
				//  check for mpesa code if inserted
				$this->form_validation->set_rules('mpesa_code', 'Amount', 'is_unique[payments.transaction_code]|trim|required|xss_clean');
			}
		}

		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			
				$this->customers_model->cat_payment($customer_id);

				$this->session->set_userdata("success_message", 'Customer Payment successfully added');
			
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		
		//open the add new Customers
		
		$data['title'] = 'Add Customer Payments';
		$v_data['title'] = $data['title'];
		//$v_data['products'] = $this->accounting_model->all_products();
		$v_data['sales'] = $this->customers_model->get_invoices($customer_id);
		$v_data['query'] = $this->customers_model->get_customer($customer_id);
		$v_data['payment_methods'] = $this->customers_model->get_payment_methods();
		$v_data['payments'] = $this->customers_model->get_payments($customer_id);
			
		$data['content'] = $this->load->view('accounts/customers/add_payment', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
}