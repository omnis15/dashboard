<?php
class Ride_model extends CI_Model 
{
	public function add_ride($customer_id)
	{
		$vehicle_id = $this->input->post('vehicle_id');
		$ride_distance = $this->input->post('ride_distance');
		$total_waiting_time = $this->input->post('total_waiting_time');
		
		$data = array(
			'ride_status_id' => $this->input->post('ride_status_id'),
			'customer_id' => $customer_id,
			'personnel_id' => $this->input->post('personnel_id'),
			'distance' => $this->input->post('ride_distance').' km',
			'duration' => $this->input->post('ride_duration').' mins',
			'ride_date' => $this->input->post('ride_date'),
			'vehicle_id' => $this->input->post('vehicle_id'),
			'from_route' => $this->input->post('from_route'),
			'to_route' => $this->input->post('to_route'),
			'end_time' => $this->input->post('end_time'),
			'start_time' => $this->input->post('start_time'),
			'base_kms' => $this->input->post('base_kms'),
			'destination_kms' => $this->input->post('destination_kms'),
			'picking_kms' => $this->input->post('picking_kms'),
			'start_kms' => $this->input->post('start_kms'),
			'approved_by' => $this->input->post('approved_by'),
			'passenger' => $this->input->post('passenger'),
			'cost' => $this->calcluate_ride_cost($vehicle_id, $customer_id, $ride_distance, $total_waiting_time),
			'created' => date('jS M Y H:i a'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id'),
		);
		
		if($this->db->insert('ride', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_ride_statuses()
	{
		$this->db->from('ride_status');
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_vehicles()
	{
		$this->db->order_by('vehicle_plate');
		$this->db->from('vehicle');
		$query = $this->db->get();
		
		return $query;
	}
	
	//import Company template
	function rides_template()
	{
		$query = $this->get_ride_statuses();
		$types = '(';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$ride_status_id = $res->ride_status_id;
				$ride_status_name = $res->ride_status_name;
				
				$types .= $ride_status_name." - ".$ride_status_id.", ";
			}
		}
		$types .= ')';
		
		$this->load->library('Excel');
		
		$title = 'Companies Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Customer number';
		$report[$row_count][1] = 'Personnel number';
		$report[$row_count][2] = 'Ride Date (yyyy-mm-dd)';
		$report[$row_count][3] = 'Distance (KM)';
		$report[$row_count][4] = 'Duration (Minutes)';
		$report[$row_count][5] = 'Ride Status '.$types;
		$report[$row_count][6] = 'Vehicle Licence Plate';
		$report[$row_count][7] = 'Pick Up';
		$report[$row_count][8] = 'Drop Off';
		$report[$row_count][9] = 'Start Kms';
		$report[$row_count][10] = 'Picking Kms';
		$report[$row_count][11] = 'Destination Kms';
		$report[$row_count][12] = 'Base Kms';
		$report[$row_count][13] = 'Start Time (hh:mm)';
		$report[$row_count][14] = 'End Time (hh:mm)';
		$report[$row_count][15] = 'Passenger';
		$report[$row_count][16] = 'Approved By';
		$report[$row_count][17] = 'Total Waiting Time (Minutes)';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	//import customers
	public function import_csv_rides($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_rides_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	//sort customers imported data
	public function sort_rides_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 18))
		{
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Customer Number</th>
						  <th>Personnel Number</th>
						  <th>Ride Date</th>
						  <th>Distance (KM)</th>
						  <th>Duration (Minutes)</th>
						  <th>Status</th>
						  <th>Comment</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				$items = array();
				$customer_number = $array[$r][0];
				
				if(!empty($customer_number))
				{
					$customer_id = $this->get_customer_id($customer_number);
					
					if($customer_id > 0)
					{
						$personnel_number = $array[$r][1];
				
						if(!empty($personnel_number))
						{
							$personnel_number = sprintf('%04d', $personnel_number);
							$personnel_id = $this->get_personnel_id($personnel_number);
							
							if($personnel_id > 0)
							{
								$vehicle_plate = $array[$r][6];
								
								if(!empty($vehicle_plate))
								{
									$vehicle_id = $this->get_vehicle_id($vehicle_plate);
									
									if($vehicle_id > 0)
									{
										$items['personnel_id'] = $personnel_id;
										$items['customer_id'] = $customer_id;
										$items['vehicle_id'] = $vehicle_id;
										$items['ride_date'] = $array[$r][2];
										$items['ride_distance'] = $array[$r][3];
										$items['ride_duration'] = $array[$r][4];
										$items['ride_status_id'] = $array[$r][5];
										$items['from_route'] = $array[$r][7];
										$items['to_route'] = $array[$r][8];
										$items['start_kms'] = $array[$r][9];
										$items['picking_kms'] = $array[$r][10];
										$items['destination_kms'] = $array[$r][11];
										$items['base_kms'] = $array[$r][12];
										$items['start_time'] = $array[$r][13];
										$items['end_time'] = $array[$r][14];
										$items['passenger'] = $array[$r][15];
										$items['approved_by'] = $array[$r][16];
										$items['total_waiting_time'] = $array[$r][17];
										$items['cost'] = $this->calcluate_ride_cost($vehicle_id, $customer_id, $array[$r][3], $array[$r][17]);
										$items['created'] = date('Y-m-d H:i:s');
										$items['created_by'] = $this->session->userdata('personnel_id');
										$items['modified_by'] = $this->session->userdata('personnel_id');
										
										$comment = '';
										if($this->db->insert('ride', $items))
										{
											$comment .= '<br/>Ride successfully added to the database';
											$class = 'success';
										}
										
										else
										{
											$comment .= '<br/>Internal error. Could not add ride to the database. Please contact the site administrator';
											$class = 'danger';
										}
									}
								}
							}
				
							else
							{
								$comment .= '<br/>Not saved. Personnel number not valid';
								$class = 'danger';
							}
						}
						
						else
						{
							$comment .= '<br/>Not saved ensure you have a personnel number entered';
							$class = 'danger';
						}
					}
				
					else
					{
						$comment .= '<br/>Not saved. Customer number not valid';
						$class = 'danger';
					}
				}
				
				else
				{
					$comment .= '<br/>Not saved ensure you have a customer number entered';
					$class = 'danger';
				}				
				
				$response .= '
					
						<tr class="'.$class.'">
							<td colspan="7">'.$comment.'</td>
						</tr> 
				';
			}
			
			$response .= '</table>';
			
			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Rides data not found ';
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	
	public function get_customer_id($customer_number)
	{
		$this->db->where('customer_number', $customer_number);
		$query = $this->db->get('customer');
		$customer_id = NULL;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$customer_id = $row->customer_id;
		}
		
		return $customer_id;
	}
	
	public function get_personnel_id($personnel_number)
	{
		$this->db->where('personnel_number', $personnel_number);
		$query = $this->db->get('personnel');
		$personnel_id = NULL;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$personnel_id = $row->personnel_id;
		}
		
		return $personnel_id;
	}
	
	public function calculate_ride_distance($start_kms, $picking_kms, $destination_kms, $base_kms, $customer_id)
	{
		$this->db->where('customer_id', $customer_id);
		$query = $this->db->get('customer');
		$total_distancce = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$customer_type_id = $row->customer_type_id;
			
			if($customer_type_id == 3)
			{
				$total_distancce = ($picking_kms - $start_kms) + ($destination_kms - $picking_kms) + ($base_kms - $destination_kms);
			}
			
			else if($customer_type_id == 2)
			{
				$total_distancce = ($destination_kms - $picking_kms);
			}
			
			else if($customer_type_id == 1)
			{
				$total_distancce = ($destination_kms - $picking_kms);
			}
		}
		
		return $total_distancce;
	}
	
	public function calcluate_ride_cost($vehicle_id, $customer_id, $distance, $total_waiting_time)
	{
		//get ride cost
		$this->db->where('service_charge.service_id = 35 AND service_charge.customer_type_id = (SELECT customer_type_id FROM customer WHERE customer_id = '.$customer_id.') AND service_charge.vehicle_capacity_id = (SELECT vehicle_capacity_id FROM vehicle WHERE vehicle_id = '.$vehicle_id.')');
		$query = $this->db->get('service_charge');
		$total_cost = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$total_cost = $row->service_charge_amount * $distance;
		}
		
		//add waiting time
		if($total_waiting_time > 30)
		{
			//check for full day usage
			if($total_waiting_time >= 480)
			{
				//get cost
				$this->db->where('service_charge.service_id = 36 AND service_charge.service_charge_name LIKE \'Full Day%\' AND service_charge.customer_type_id = (SELECT customer_type_id FROM customer WHERE customer_id = '.$customer_id.') AND service_charge.vehicle_capacity_id = (SELECT vehicle_capacity_id FROM vehicle WHERE vehicle_id = '.$vehicle_id.')');
				$query_other = $this->db->get('service_charge');
		
				if($query_other->num_rows() > 0)
				{
					$row = $query_other->row();
					$service_charge_amount = $row->service_charge_amount;
					
					$total_cost = $service_charge_amount;
				}
			}
			
			//check for half day usage
			if(($total_waiting_time >= 240) && ($total_waiting_time < 480))
			{
				//get cost
				$this->db->where('service_charge.service_id = 36 AND service_charge.service_charge_name LIKE \'Half Day%\' AND service_charge.customer_type_id = (SELECT customer_type_id FROM customer WHERE customer_id = '.$customer_id.') AND service_charge.vehicle_capacity_id = (SELECT vehicle_capacity_id FROM vehicle WHERE vehicle_id = '.$vehicle_id.')');
				$query_other = $this->db->get('service_charge');
		
				if($query_other->num_rows() > 0)
				{
					$row = $query_other->row();
					$service_charge_amount = $row->service_charge_amount;
					
					$total_cost = $service_charge_amount;
				}
			}
			
			else
			{
				//get cost per minute
				$this->db->where('service_charge.service_id = 36 AND service_charge.service_charge_name = \'Waiting charge\' AND service_charge.customer_type_id = (SELECT customer_type_id FROM customer WHERE customer_id = '.$customer_id.') AND service_charge.vehicle_capacity_id = (SELECT vehicle_capacity_id FROM vehicle WHERE vehicle_id = '.$vehicle_id.')');
				$query_other = $this->db->get('service_charge');
		
				if($query_other->num_rows() > 0)
				{
					$row = $query_other->row();
					$service_charge_amount = $row->service_charge_amount;
					
					$total_time = $total_waiting_time - 30;
					$total_cost += ($total_time * $service_charge_amount);
				}
			}
		}
		
		return $total_cost;
	}
	public function get_ride_customer_id($ride_id)
	{
		$this->db->where('ride_id', $ride_id);
		$query = $this->db->get('ride');
		$customer_id = NULL;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$customer_id = $row->customer_id;
		}
		
		return $customer_id;
	}
	public function get_ride_vehicle_id($ride_id)
	{
		$this->db->where('ride_id', $ride_id);
		$query = $this->db->get('ride');
		$customer_id = NULL;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$customer_id = $row->vehicle_id;
		}
		
		return $customer_id;
	}
	public function get_total_waiting_time($ride_id)
	{
		$this->db->where('ride_id', $ride_id);
		$query = $this->db->get('ride_waiting');
		$start_time = NULL;
		$stop_time = NULL;
		$row_waiting_time = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$start_time = $row->start_waiting_time;
			$stop_time = $row->stop_waiting_time;
			$row_waiting_time = $row_waiting_time + ($stop_time-$start_time);
			
			

		}
		
		return $row_waiting_time;
	}
}