<?php
//personnel data
$start_date = set_value('start_date');
$end_date = set_value('end_date');
$invoice_date = set_value('invoice_date');






?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                             <a href="<?php echo site_url();?>accounts/customers" class="btn btn-info pull-right">Back to Customers </a>
                        </div>
                    </div>
                        
                   

                <?php
        
        $result = '';
        
        //if users exist display them
       
        if ($query->num_rows() > 0)
        {
            $count = 0;
            
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                      
						<th>#</th>
						<th>Date</th>
						<th>Pick Up</th>
						<th>Drop Off</th>
						<th>Distance</th>
						<th>Duration</th>
						<th>Vehicle</th>
						
						<th>Cost</th>
						
                        
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
            //get all administrators
            $administrators = $this->users_model->get_active_users();
            if ($administrators->num_rows() > 0)
            {
                $admins = $administrators->result();
            }
            
            else
            {
                $admins = NULL;
            }
            
            foreach ($query->result() as $row)
            {

  
                $ride_id = $row->ride_id;
				$ride_date = $row->ride_date;
				$from_route = $row->from_route;
				$to_route = $row->to_route;
				$distance = $row->distance;
				$duration = $row->duration;
				$vehicle_id = $row->vehicle_id;
				$vehicle_plate = $this->vehicle_model->get_vehicle_plate($vehicle_id);
				
				$cost = $row->cost;
				
				
                
               
              
                
             
               
                
                //status
               
                
                
                
                $count++;
                $result .= 
                '
                 
                    <tr>
                      <td>'.$count.'</td>
						<td>'.date('jS M Y H:i a',strtotime($ride_date)).'</td>
						<td>'.$from_route.'</td>
						<td>'.$to_route.'</td>
						<td>'.$distance.'</td>
						<td>'.$duration.'</td>
						<td>'.$vehicle_plate.'</td>
						
						<td>'.$cost.'</td>
						
						
                        
                        
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no invoices";
        }
        echo $result;
?>
 
 </div>
            </section>
