<?php
//personnel data
$start_date = set_value('start_date');
$end_date = set_value('end_date');
$invoice_date = set_value('invoice_date');






?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                             <a href="<?php echo site_url();?>accounts/customers" class="btn btn-info pull-right">Back to Customers </a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
        
         <div class="form-group">
            <label class="col-lg-5 control-label">Invoice Date: </label>
            
            <div class="col-lg-7">
                <div class="input-group date" id="">
                <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </span>
                <input id="invoice_date" type="text" name="invoice_date" class="form-control" placeholder="Date" value="<?php echo $invoice_date;?>">
                </div>
            </div>
        </div>
        
        
        
        
    </div>
    
    <div class="col-md-6">
        
       
        <div class="form-group">
            <label class="col-lg-5 control-label">Start Date: </label>
            
            <div class="col-lg-7">
                <div class="input-group date" id="">
                <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </span>
                <input id="start_date" type="text" name="start_date" class="form-control" placeholder="Date" value="<?php echo $start_date;?>">
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">End Date: </label>
            
            <div class="col-lg-7">
               <div class="input-group date" id="">
                <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </span>
                <input id="end_date" type="text" name="end_date" class="form-control" placeholder="Date" value="<?php echo $end_date;?>">
                </div>
            </div>
        </div>
        
        
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Invoice
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>

                <?php
        
        $result = '';
        
        //if users exist display them
       
        if ($query->num_rows() > 0)
        {
            $count = 0;
            
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Invoice Date</th>
                        <th>Invoice Number</th>
                        <th>Total Cost</th>
                        <th>Status</th>
                        <th colspan="2">Actions</th>
                        
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
            //get all administrators
            $administrators = $this->users_model->get_active_users();
            if ($administrators->num_rows() > 0)
            {
                $admins = $administrators->result();
            }
            
            else
            {
                $admins = NULL;
            }
            
            foreach ($query->result() as $row)
            {

  
                $invoice_date = $row->invoice_date;
                $invoice_id = $row->invoice_id;
                $amount = $row->amount;
                $invoice_status = $row->invoice_status;
                $invoive_number = $row->invoive_number;
                
                //status
                if($invoice_status == 1)
                {
                    $status = 'Active';
                }
                else
                {
                    $status = 'Disabled';
                }
                
                
                //create deactivated status display
                if($invoice_status == 0)
                {
                    $status = '<span class="label label-default">Deactivated</span>';
                    
                }
                //create activated status display
                else if($invoice_status == 1)
                {
                    $status = '<span class="label label-success">Active</span>';
                   
                }
                
                
                
                
                $count++;
                $result .= 
                '
                 
                    <tr>
                       <td>'.$count.'</td>
                        <td>'.date('jS M Y H:i a',strtotime($invoice_date)).'</td>
                        <td>'.$invoive_number.'</td>
                        <td>'.$amount.'</td>
                        <td>'.$status.'</td>
                        <td><a href="'.site_url().'accounts/customers/cust-view-ride/'.$invoice_id.'/'.$customer_id.'" class="btn btn-sm btn-success" >View Rides</a></td>
                        
                        
                        
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no invoices";
        }
        echo $result;
?>
 
 </div>
            </section>
