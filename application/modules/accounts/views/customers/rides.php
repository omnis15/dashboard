<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Date</th>
						<th>Pick Up</th>
						<th>Drop Off</th>
						<th>Distance</th>
						<th>Duration</th>
						<th>Vehicle</th>
						<th>Driver</th>
						<th>Cost</th>
						<th>Status</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				<tbody>
				  
			';
			
			foreach ($query->result() as $row)
			{
				$ride_id = $row->ride_id;
				$ride_date = $row->ride_date;
				$from_route = $row->from_route;
				$to_route = $row->to_route;
				$distance = $row->distance;
				$duration = $row->duration;
				$vehicle_plate = $row->vehicle_plate;
				$personnel_fname = $row->personnel_fname;
				$personnel_onames = $row->personnel_onames;
				$cost = $row->cost;
				$ride_status_name = $row->ride_status_name;
				$driver = $personnel_fname.' '.$personnel_onames;
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.date('jS M Y',strtotime($ride_date)).'</td>
						<td>'.$from_route.'</td>
						<td>'.$to_route.'</td>
						<td>'.$distance.'</td>
						<td>'.$duration.'</td>
						<td>'.$vehicle_plate.'</td>
						<td>'.$driver.'</td>
						<td>'.number_format($cost, 2).'</td>
						<td>'.$ride_status_name.'</td>
						<td><a href="'.site_url().'accounts/customers/edit-ride/'.$ride_id.'/'.$customer_id.'" class="btn btn-sm btn-success" >Edit</a></td>
						<td><a href="'.site_url().'accounts/customers/delete-ride/'.$ride_id.'/'.$customer_id.'" class="btn btn-sm btn-danger" onClick="return confirm(Are you sure you want to delete this ride? This action cannot be undone.)">Delete</a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no rides";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $validation = validation_errors();

				if(!empty($validation))
				{
					echo '<div class="alert alert-danger"> <strong>Oops!</strong> '.$validation.' </div>';
				}
				
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                    <div class="col-sm-2 col-lg-offset-10">
                        <a href="#" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#add_payments">Add Ride</a>
                     </div>
                </div>
				 
				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_payments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Ride</h4>
			</div>
			<div class="modal-body">
				<section class="panel">
                    <!-- Widget content -->
                    <div class="panel-body">
						<?php echo form_open('accounts/customers/add-ride/'.$customer_id, array("class" => "form-horizontal", "role" => "form"));?>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Ride Date: </label>
                                
                                <div class="col-md-8">
                                    <div class="input-group date" id="">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                        <input id="datapicker2" type="text" name="ride_date" class="form-control" placeholder="Date">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Ride Status: </label>
                                
                                <div class="col-md-8">
                                    <select name="ride_status_id" class="form-control" >
                                        <option value="">None - Please Select a status</option>
                                        <?php echo $statuses;?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Vehicle: </label>
                                
                                <div class="col-md-8">
                                    <select id="vehicle_id" name="vehicle_id" class="custom-select" >
                                        <option value="">None - Please Select a vehicle</option>
                                        <?php echo $vehicles;?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Driver: </label>
                                
                                <div class="col-md-8">
                                    <select id="personnel_id" name="personnel_id" class="custom-select" >
                                        <option value="">None - Please Select a driver</option>
                                        <?php echo $drivers;?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Approved By: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" name="approved_by" class="form-control" placeholder="Approved By">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Passenger: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" name="passenger" class="form-control" placeholder="Passenger">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Pick Up: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" name="from_route" class="form-control" placeholder="Pick Up">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Drop Off: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" name="to_route" class="form-control" placeholder="Drop Off">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Start Kms: </label>
                                
                                <div class="col-md-8">
                                	<input type="text" id="start_kms" name="start_kms" class="form-control" placeholder="Start Kms">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Picking Kms: </label>
                                
                                <div class="col-md-8">
                                	<input type="text" id="picking_kms" name="picking_kms" class="form-control" placeholder="Picking Kms">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Destination Kms: </label>
                                
                                <div class="col-md-8">
                                	<input type="text" id="destination_kms" name="destination_kms" class="form-control" placeholder="Destination Kms">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Base Kms: </label>
                                
                                <div class="col-md-8">
                                	<input type="text" id="base_kms" name="base_kms" class="form-control" placeholder="Base Kms">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Distance (KM): </label>
                                
                                <div class="col-md-8">
                                	<input type="text" id="ride_distance" name="ride_distance" class="form-control" placeholder="Distance">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Start Time: </label>
                                
                                <div class="col-md-8">
                                	<div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input id="timepicker1" class="form-control" name="start_time" type="text" placeholder="Start Time">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">End Time: </label>
                                
                                <div class="col-md-8">
                                	<div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input id="timepicker2" class="form-control" name="end_time" type="text" placeholder="End Time">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Duration (Minutes): </label>
                                
                                <div class="col-md-8">
                                	<input type="text" name="ride_duration" class="form-control" placeholder="Duration">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Total Waiting Time (Minutes): </label>
                                
                                <div class="col-md-8">
                                	<input type="text" name="total_waiting_time" class="form-control" placeholder="Total Waiting Time">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-6">
                                    <button type="submit" class="btn btn-sm btn-success">Add Ride</button>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
            	</section>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Companies Modal -->
<div class="modal fade" id="import_payments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Import Rides</h4>
			</div>
			<div class="modal-body">
				<section class="panel">
                    <!-- Widget content -->
                    <div class="panel-body">
                    <div class="padd">
                        
                    <div class="row">
                    <div class="col-md-12">
                    
                        <?php echo form_open_multipart('accounts/customers/import-rides', array("class" => "form-horizontal", "role" => "form"));?>
                        <div class="row">
                            <div class="col-md-12">
                                <ul>
                                    <li>Download the import template <a href="<?php echo site_url().'accounts/customers/rides-template';?>">here.</a></li>
                                    
                                    <li>Save your file as a <strong>CSV (Comma Delimited)</strong> file before importing</li>
                                    <li>After adding your customers to the import template please import them using the button below</li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="row">
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <div class="fileUpload btn btn-primary">
                                    <span>Import Rides</span>
                                    <input type="file" class="upload"  name="import_csv"/>
                                </div>
                            </div>
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <input type="submit" onChange="this.form.submit();" class="btn btn-warning" onclick="return confirm('Do you really want to upload the selected file?')" value="Import">
                            </div>
                        </div>
                               
                                
                    </div>
                    </div>
                        <?php echo form_close();?>
                    </div>
                    </div>
            
            </section>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
	
<script text="javascript">

	$(function() {
		$("#customer_id").customselect();
		$("#personnel_id").customselect();
		$("#vehicle_id").customselect();
		// DateTimePicker
        $('#datapicker2').datepicker({
			format: 'yyyy-mm-dd'
		 });
	});
	
	var customer_id = '<?php echo $customer_id;?>';
	
	$(document).on("focusout","#start_kms",function()
	{
		var start_kms = $('#start_kms').val();
		var picking_kms = $('#picking_kms').val();
		var destination_kms = $('#destination_kms').val();
		var base_kms = $('#base_kms').val();
		
		calculate_ride_distance(customer_id, start_kms, picking_kms, destination_kms, base_kms);
	});
	$(document).on("focusout","#picking_kms",function()
	{
		var start_kms = $('#start_kms').val();
		var picking_kms = $('#picking_kms').val();
		var destination_kms = $('#destination_kms').val();
		var base_kms = $('#base_kms').val();
		
		calculate_ride_distance(customer_id, start_kms, picking_kms, destination_kms, base_kms);
	});
	$(document).on("focusout","#destination_kms",function()
	{
		var start_kms = $('#start_kms').val();
		var picking_kms = $('#picking_kms').val();
		var destination_kms = $('#destination_kms').val();
		var base_kms = $('#base_kms').val();
		
		calculate_ride_distance(customer_id, start_kms, picking_kms, destination_kms, base_kms);
	});
	$(document).on("focusout","#base_kms",function()
	{
		var start_kms = $('#start_kms').val();
		var picking_kms = $('#picking_kms').val();
		var destination_kms = $('#destination_kms').val();
		var base_kms = $('#base_kms').val();
		
		calculate_ride_distance(customer_id, start_kms, picking_kms, destination_kms, base_kms);
	});
	
	function calculate_ride_distance(customer_id, start_kms, picking_kms, destination_kms, base_kms)
	{
		if((start_kms != '') && (picking_kms != '') && (destination_kms != '') && (base_kms != ''))
		{
			$.ajax({
				type:'GET',
				url: '<?php echo site_url().'accounts/customer/calculate-ride-distance/';?>'+customer_id+'/'+start_kms+'/'+picking_kms+'/'+destination_kms+'/'+base_kms,
				dataType: 'json',
				success:function(data)
				{
					//alert(data.message);
					$('#ride_distance').val( data.ride_distance );
				},
				error: function(xhr, status, error) 
				{
					alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				}
			});
		}
		
		else
		{
			$('#ride_distance').val( 0 );
		}
	}
</script>