<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Customer number</th>
						<th>Customer name</th>
						<th>Phone Number</th>
						<th>Email</th>
						<th>Registration Date</th>
						<th>Type</th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				<tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$customer_id = $row->customer_id;
				$customer_first_name = $row->customer_first_name;
				$customer_surname = $row->customer_surname;
				$customer_phone = $row->customer_phone;
				$customer_email = $row->customer_email;
				$customer_status = $row->customer_status;
				$customer_created = $row->customer_created;
				$company_status = $row->company_status;
				$customer_type_id = $row->customer_type_id;
				$customer_number = $row->customer_number;
				$customer_type_name = $row->customer_type_name;
				
				$customer_name = $customer_first_name.' '.$customer_surname;
				
				//status
				if($customer_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$customer_number.'</td>
						<td>'.$customer_first_name.'</td>
						<td>'.$customer_phone.'</td>
						<td>'.$customer_email.'</td>
						<td>'.date('jS M Y H:i a',strtotime($row->customer_created)).'</td>
						<td>'.$customer_type_name.'</td>
						<td><a href="'.site_url().'accounts/customers/view-rides/'.$customer_id.'" class="btn btn-sm btn-success" title="Rides for '.$customer_name.'">Rides</a></td>
						<td><a href="'.site_url().'accounts/customers/cust-invoice/'.$customer_id.'" class="btn btn-sm btn-primary" title="Invoices for '.$customer_name.'">Invoices</a></td>
						<td><a href="'.site_url().'accounts/customers/cust-payment/'.$customer_id.'" class="btn btn-sm btn-warning" title="Payments for '.$customer_name.'">Payments</a></td>
				
					
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no customers";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $validation = validation_errors();

				if(!empty($validation))
				{
					echo '<div class="alert alert-danger"> <strong>Oops!</strong> '.$validation.' </div>';
				}
				
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                    <div class="col-sm-2 col-lg-offset-10">
                       <a href="#" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#import_payments">Import Rides</a>
                    </div>
                </div>
				 
				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_payments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Ride</h4>
			</div>
			<div class="modal-body">
				<section class="panel">
                    <!-- Widget content -->
                    <div class="panel-body">
						<?php echo form_open('accounts/customers/add-ride', array("class" => "form-horizontal", "role" => "form"));?>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Ride Date: </label>
                                
                                <div class="col-md-8">
                                    <div class="input-group date" id="">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                        <input id="datapicker2" type="text" name="ride_date" class="form-control" placeholder="Date">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Ride Status: </label>
                                
                                <div class="col-md-8">
                                    <select name="ride_status_id" class="form-control" >
                                        <option value="">None - Please Select a status</option>
                                        <?php echo $statuses;?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Vehicle: </label>
                                
                                <div class="col-md-8">
                                    <select id="vehicle_id" name="vehicle_id" class="custom-select" >
                                        <option value="">None - Please Select a vehicle</option>
                                        <?php echo $vehicles;?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Customer: </label>
                                
                                <div class="col-md-8">
                                    <select id="customer_id" name="customer_id" class="custom-select" >
                                        <option value="">None - Please Select a customer</option>
                                        <?php echo $customers;?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Driver: </label>
                                
                                <div class="col-md-8">
                                    <select id="personnel_id" name="personnel_id" class="custom-select" >
                                        <option value="">None - Please Select a driver</option>
                                        <?php echo $drivers;?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Pick Up: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" name="from_route" class="form-control" placeholder="Pick Up">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Drop Off: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" name="to_route" class="form-control" placeholder="Drop Off">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Distance (KM): </label>
                                
                                <div class="col-md-8">
                                	<input type="text" name="ride_distance" class="form-control" placeholder="Distance">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Duration (Minutes): </label>
                                
                                <div class="col-md-8">
                                	<input type="text" name="ride_duration" class="form-control" placeholder="Duration">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-6">
                                    <button type="submit" class="btn btn-sm btn-success">Add Ride</button>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
            	</section>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Companies Modal -->
<div class="modal fade" id="import_payments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Import Rides</h4>
			</div>
			<div class="modal-body">
				<section class="panel">
                    <!-- Widget content -->
                    <div class="panel-body">
                    <div class="padd">
                        
                    <div class="row">
                    <div class="col-md-12">
                    
                        <?php echo form_open_multipart('accounts/customers/import-rides', array("class" => "form-horizontal", "role" => "form"));?>
                        <div class="row">
                            <div class="col-md-12">
                                <ul>
                                    <li>Download the import template <a href="<?php echo site_url().'accounts/customers/rides-template';?>">here.</a></li>
                                    
                                    <li>Save your file as a <strong>CSV (Comma Delimited)</strong> file before importing</li>
                                    <li>After adding your customers to the import template please import them using the button below</li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="row">
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <div class="fileUpload btn btn-primary">
                                    <span>Import Rides</span>
                                    <input type="file" class="upload"  name="import_csv"/>
                                </div>
                            </div>
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <input type="submit" onChange="this.form.submit();" class="btn btn-warning" onclick="return confirm('Do you really want to upload the selected file?')" value="Import">
                            </div>
                        </div>
                               
                                
                    </div>
                    </div>
                        <?php echo form_close();?>
                    </div>
                    </div>
            
            </section>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
	
<script text="javascript">

	$(function() {
		$("#customer_id").customselect();
		$("#personnel_id").customselect();
		$("#vehicle_id").customselect();
		// DateTimePicker
        $('#datapicker2').datepicker({
			format: 'yyyy-mm-dd'
		 });
	});
</script>