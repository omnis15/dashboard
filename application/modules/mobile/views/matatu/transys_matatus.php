<?php
if($query->num_rows() > 0)
{
	$expenses = '<ul>';
	foreach ($query->result() as $key) {
		# code...
		$vehicle_id = $key->vehicle_id;
		$vehicle_plate = $key->vehicle_plate;
		$vehicle_name = $key->vehicle_name;

		$vehicle_owner = $key->vehicle_owner;
		$owner_name = '';
		$driver_name = '';
		$conductor_name = '';
		if(!empty($vehicle_owner))
		{
			$owner_name = $this->matatu_model->get_personnel_name($vehicle_owner);
		}
		$vehicle_driver = $key->vehicle_driver;
		if(!empty($vehicle_driver))
		{
			$driver_name = $this->matatu_model->get_personnel_name($vehicle_driver);
		}
		$vehicle_conductor = $key->vehicle_conductor;
		if(!empty($vehicle_conductor))
		{
			$conductor_name = $this->matatu_model->get_personnel_name($vehicle_conductor);
		}

		$vehicle_routes = $this->matatu_model->get_assigned_matatu_routes($vehicle_plate);
		$routes = '';
		if($vehicle_routes->num_rows() > 0)
		{
			foreach ($vehicle_routes->result() as $key_routes) {
				# code...
				$route_start = $key_routes->route_start;
				$route_end = $key_routes->route_end;

				$routes .= ''.$route_start.' - '.$route_end.' <br>';
			}
		}
		$expenses .= '
						<li>
					      <a href="dist/expense/vehicle-expense.html" onclick="get_vehicle_expenses('.$vehicle_id.')" class="item-link item-content">
					        <div class="item-inner">
					          <div class="item-title-row align-center">
					            <div class="item-title ">'.$vehicle_plate.'</div>
					             <div class="item-after ">Owner : '.$owner_name.'  <br> Driver: '.$driver_name.' <br> Tout : '.$conductor_name.' <br></div>
					          </div>
					           <div class="item-text">'.$routes.' </div>
					        </div>
					      </a>
					    </li>
				   ';
	}
	$expenses .="</ul>";
}
echo $expenses;
?>