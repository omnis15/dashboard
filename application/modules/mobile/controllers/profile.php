<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
		
		$this->load->model('login_model');
	}
	
	public function get_client_profile($member_id)
	{
		$v_data['job_seeker_details'] = $this->profile_model->get_profile_details($member_id);

		$response['message'] = 'success';
		$v_data['member_id'] = $member_id;
		$response['result'] = $this->load->view('job_seeker/seekers_profile', $v_data, true);

		echo json_encode($response);
	}

	public function login_member() 
	{
		$member_no = $this->input->post('member_no');
		$password = $this->input->post('member_password');
		
		if(empty($member_no) || empty($password))
		{
			$result_other['message'] = 'fail';
			$result_other['result'] = 'Please enter member number';
		}
		else
		{
			$response = $this->login_model->validate_member($member_no, $password);
			
			if($response['status'] == TRUE)
			{
				$result_other['message'] = 'success';
				$result_other['result'] = $response['message'];

			}
			else
			{
				$result_other['message'] = 'fail';
				$result_other['result'] = 'Unable to log in. Please ensure you have entered your correct Member No. and Password';
			}
		
			
		}	
		
		echo json_encode($result_other);
	}
	public function register_seeker()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('email_address', 'Email', 'trim|valid_email|required|xss_clean');
		$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required|xss_clean');
		$this->form_validation->set_rules('fullname', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('over_age', 'Age', 'trim|required|xss_clean');
		$this->form_validation->set_rules('terms', 'Terms & Conditions', 'trim|required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$return = $this->profile_model->register_member_details();
			//var_dump($return);
			if($return['status'] == TRUE)
			{
				$response['message'] = 'success';
			 	$response['result'] = 'Registration was successfull';
			 	
			 	$job_seeker_phone = $this->input->post('phone_number');
			 	$fullname = $this->input->post('fullname');
			 	
			 	$delivery_message = "Hello $fullname, Thank you for registering to Choto. Please watch as many ads as possible to be rewarded. Note that all transactions shall be made to $job_seeker_phone. Enjoy";

				/*$result = $this->advertising_model->sms($job_seeker_phone,$delivery_message);
				
				if($result == 'Success')
				{
					$this->db->where('job_seeker_id', $return['job_seeker_id']);
					$this->db->update('job_seeker', array('welcome_sms' => 1));
				}*/

			 	$response['job_seeker_id'] = $return['job_seeker_id'];
			}
			else
			{
				$response['message'] = 'fail';
			 	$response['result'] = $return['message'];
			}

		}
		else
		{
			$validation_errors = validation_errors();
			
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				$response['message'] = 'fail';
			 	$response['result'] = $validation_errors;
			}
			
			//populate form data on initial load of page
			else
			{
				$response['message'] = 'fail';
				$response['result'] = 'Ensure that you have entered all the values in the form provided';
			}
		}
		echo json_encode($response);
	}
	public function request_for_payment($job_seeker_id)
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('accept', 'Check box', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_to_withdraw', 'Phone Number', 'trim|required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// check the current balance 
			$total_invoiced = $this->advertising_model->calculate_amount_invoices($job_seeker_id);
		  	$total_receipted = $this->advertising_model->calculate_amount_receipted($job_seeker_id);
		  	$account_balance = $total_invoiced - $total_receipted;

		  	$amount_to_withdraw = $this->input->post('amount_to_withdraw');
		  	$actual_balance = $account_balance - $amount_to_withdraw;
		  	$response['items'] = $amount_to_withdraw;
		  	$checker = $this->advertising_model->check_for_requested($job_seeker_id);
		  	$response['actual_balance'] = $account_balance;
		  	$profile_rs = $this->profile_model->get_profile_details($job_seeker_id);
		  	if($profile_rs->num_rows() > 0)
		  	{
		  		foreach ($profile_rs->result() as $key) {
		  			# code...
		  			$job_seeker_id = $key->job_seeker_id;
		  			$job_seeker_last_name = $key->job_seeker_last_name;
		  			$job_seeker_phone = $key->job_seeker_phone;
		  		}
		  	}
		  	if($checker > 0)
		  	{
		  		$response['message'] = 'fail';
				 $response['result'] = 'Sorry there is a pending transaction. Please wait for it to be serviced then do the request';

				 $delivery_message = "Hello $job_seeker_last_name, Sorry there is a pending transaction. Please wait for it to be serviced then do the request";

				$this->advertising_model->sms($job_seeker_phone,$delivery_message);
		  	}
		  	else
		  	{
			  	if($actual_balance < 0)
			  	{
			  		$response['message'] = 'success';
				 	$response['result'] = 'Insufficient money in your account. Please watch as many ads as possible';
				 	$delivery_message = "You have insufficient money in your account. Please watch as more ads to get rewarded";
					$this->advertising_model->sms($job_seeker_phone,$delivery_message);
			  	}
			  	else
			  	{
					$return = $this->profile_model->update_request_detail($job_seeker_id);
					//var_dump($return);
					if($return == TRUE)
					{
						$response['message'] = 'success';
					 	$response['result'] = 'Your transaction is being processed. Please wait for your reward shortly';

					 	$delivery_message = "Your transaction is being processed. Please wait for your reward shortly";

						$this->advertising_model->sms($job_seeker_phone,$delivery_message);
					}
					else
					{
						$response['message'] = 'fail';
					 	$response['result'] = $return['message'];
					}
				}
		  	}
		  	
		}
		else
		{
			$validation_errors = validation_errors();
			
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				$response['message'] = 'fail';
			 	$response['result'] = $validation_errors;
			}
			
			//populate form data on initial load of page
			else
			{
				$response['message'] = 'fail';
				$response['result'] = 'Ensure that you have entered all the values in the form provided';
			}
		}
		echo json_encode($response);
	}
	public function reset_password()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('email_address', 'Email', 'trim|valid_email|required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->profile_model->reset_password())
			{
				$response['message'] = 'success';
			 	$response['result'] = 'Password reset to 123456';
			}
			else
			{
				$response['message'] = 'fail';
			 	$response['result'] = 'Something went wrong. Please try again';
			}

		}
		else
		{
			$validation_errors = validation_errors();
			
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				$response['message'] = 'fail';
			 	$response['result'] = $validation_errors;
			}
			
			//populate form data on initial load of page
			else
			{
				$response['message'] = 'fail';
				$response['result'] = 'Ensure that you have entered all the values in the form provided';
			}
		}
		echo json_encode($response);
	}
	
	public function get_client_transactions($seeker_id)
	{
		$v_data['transactions_details'] = $this->profile_model->get_client_transactions($seeker_id);

		$response['message'] = 'success';
		$v_data['member_id'] = $seeker_id;
		$response['member_id'] = $seeker_id;
		$response['result'] = $this->load->view('job_seeker/transactions', $v_data, true);

		echo json_encode($response);
	}
	
	public function test_sms()
	{
		$job_seeker_phone = '726149351';
		
		$delivery_message = "Hello Alvar0, Thank you for registering to Choto. Please watch as many ads as possible to be reward. Note that all transactions shall be made to 726149351. Enjoy";

		$response = $this->advertising_model->sms($job_seeker_phone,$delivery_message);
		
		var_dump($response);
	}
	
	public function get_terms()
    {
        $response['result'] = $this->load->view('job_seeker/terms', '', true);

        echo json_encode($response);
    }
	
	public function send_registration_sms()
	{
		$this->db->where('welcome_sms', 0);
		$query = $this->db->get('job_seeker');
		echo '
			<table>
				<tr>
					<th>Result</th>
					<th>Message</th>
				</tr>
		';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$job_seeker_phone = $res->job_seeker_phone;
				$fullname = $res->job_seeker_first_name;
				
				$delivery_message = "Hello $fullname, Thank you for registering to Choto. Please watch as many ads as possible to be reward. Note that all transactions shall be made to $job_seeker_phone. Enjoy";
		
				$result = $this->advertising_model->sms($job_seeker_phone, $delivery_message);
				
				if($result == 'Success')
				{
					$this->db->where('job_seeker_id', $return['job_seeker_id']);
					$this->db->update('job_seeker', array('welcome_sms' => 1));
				}
				echo '
					<tr>
						<td>'.$result.'</td>
						<td>'.$delivery_message.'</td>
					</tr>
				';
			}
		}
		
		echo
		
		'
			</table>
		';
	}
}