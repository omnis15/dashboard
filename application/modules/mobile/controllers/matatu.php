<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Matatu extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		
		$this->load->model('matatu_model');
	}
	
	public function get_days_trips($personnel_id) 
	{
		$query = $this->matatu_model->get_days_trips($personnel_id);
		$vehicle_plate = $this->matatu_model->get_assigned_matatu($personnel_id);
		$v_data['personnel_id'] = $personnel_id;
		$v_data['query'] = $query;
		$response['message'] = 'success';
		$response['vehicle_plate'] = $vehicle_plate;

		$v_data2['routes_query'] = $this->matatu_model->get_assigned_matatu_routes($vehicle_plate);
		$response['routes'] = $this->load->view('matatu/matatu_routes', $v_data2, true);
		$response['result'] = $this->load->view('matatu/trip_days', $v_data, true);
		
		echo json_encode($response);
	}

	public function add_trip($personnel_id) 
	{
		$trip_id = $this->input->post('trip_name');
		if($this->matatu_model->add_trip($personnel_id))
		{
			$query = $this->matatu_model->get_days_trips($personnel_id);
			
			$v_data['personnel_id'] = $personnel_id;
			$v_data['query'] = $query;
			$response['message'] = 'success';
			$response['result'] = $this->load->view('matatu/trip_days', $v_data, true);
			
			echo json_encode($response);
		}
	}
	public function add_trip_detail($personnel_id)
	{
		
		if($this->matatu_model->add_trip_detail($personnel_id))
		{
			$query = $this->matatu_model->get_days_trips($personnel_id);
			
			$v_data['personnel_id'] = $personnel_id;
			$v_data['query'] = $query;
			$response['message'] = 'success';
			$response['result'] = $this->load->view('matatu/trip_days', $v_data, true);
			
			echo json_encode($response);
		}
	}
	
	public function get_trip_routes($trip_id) 
	{
		$query = $this->matatu_model->get_trip_routes($trip_id);
		
		$v_data['trip_id'] = $trip_id;
		$v_data['query'] = $query;
		$response['message'] = 'success';
		$response['result'] = $this->load->view('matatu/trip_routes', $v_data, true);
		
		echo json_encode($response);
	}



	public function get_transys_expense($personnel_id)
	{
		$query = $this->matatu_model->get_days_expense($personnel_id);
		$vehicle_plate = $this->matatu_model->get_assigned_matatu($personnel_id);
		$v_data['personnel_id'] = $personnel_id;
		$v_data['query'] = $query;
		$response['message'] = 'success';
		$response['vehicle_plate'] = $vehicle_plate;

		$v_data2['expense_query'] = $query;
		$response['expenses_select'] = $this->load->view('matatu/matatu_expenses', $v_data2, true);
		$response['result'] = $this->load->view('matatu/trip_expense', $v_data, true);

		echo json_encode($response);
	}
	public function add_expense_detail($personnel_id)
	{
		
		if($this->matatu_model->add_trip_expense_detail($personnel_id))
		{
			$query = $this->matatu_model->get_days_expense($personnel_id);
			
			$v_data['personnel_id'] = $personnel_id;
			$v_data['query'] = $query;
			$response['message'] = 'success';
			$response['result'] = $this->load->view('matatu/trip_expense', $v_data, true);
			
			echo json_encode($response);
		}
	}
	public function get_transys_matatus($personnel_id)
	{
		$query = $this->matatu_model->get_transys_matatus();
		$v_data['personnel_id'] = $personnel_id;
		$v_data['query'] = $query;
		$response['message'] = 'success';
		$response['result'] = $this->load->view('matatu/transys_matatus', $v_data, true);
		
		echo json_encode($response);
	}
	public function get_admin_matatu_expense($vehicle_id, $personnel_id)
	{
		$query = $this->matatu_model->get_transys_admin_expenses($personnel_id);
		$v_data['personnel_id'] = $personnel_id;
		$v_data['vehicle_id'] = $vehicle_id;

		$vehicle_plate = $this->matatu_model->get_plate_number($vehicle_id);
		$response['vehicle_plate'] = $vehicle_plate;

		$v_data['query'] = $query;
		$response['message'] = 'success';
		$response['result'] = $this->load->view('matatu/admin_matatu_expense', $v_data, true);
		
		echo json_encode($response);
	}
}