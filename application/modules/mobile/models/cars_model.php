<?php
class Cars_model extends CI_Model
{
	public function get_car_makes()
	{
		$this->db->where('brand_status', 1);
		$this->db->order_by('brand_name');
		return $this->db->get('brand');
	}
	
	public function get_car_models($brand_name)
	{
		$this->db->where('brand_model_status = 1 AND brand.brand_id = brand_model.brand_id AND brand.brand_name = "'.$brand_name.'"');
		$this->db->order_by('brand_model_name');
		return $this->db->get('brand, brand_model');
	}
}