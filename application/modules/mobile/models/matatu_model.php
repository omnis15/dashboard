<?php

class Matatu_model extends CI_Model 
{
	/*
	*	Get day's trips
	*
	*/
	public function get_days_trips($personnel_id)
	{
		$today = date('Y-m-d');
	 	$this->db->order_by('trip_id','ASC');
	 	$this->db->where('vehicle.vehicle_id = trip.vehicle_id AND trip.trip_date = "'.$today.'" AND (vehicle.vehicle_driver = '.$personnel_id.' OR vehicle.vehicle_conductor = '.$personnel_id.')');
		$query = $this->db->get('vehicle, trip');
		
		return $query;
	}


	/*
	*	Get day's trips
	*
	*/
	public function get_days_expense($personnel_id)
	{
	 	$this->db->order_by('expense_id','ASC');
	 	$this->db->where('expense_type_id = 1');
		$query = $this->db->get('expense');
		
		return $query;
	}
	public function get_transys_admin_expenses($personnel_id)
	{
	 	$this->db->order_by('expense_id','ASC');
	 	$this->db->where('expense_type_id = 2');
		$query = $this->db->get('expense');
		
		return $query;
	}
	/*
	*	Get trip routes
	*
	*/
	public function get_trip_routes($trip_id)
	{
	 	$this->db->order_by('trip_route.trip_route_invert','ASC');
	 	$this->db->where('trip_route.trip_id = '.$trip_id.' AND trip_route.route_id = route.route_id');
		$query = $this->db->get('trip_route, route');
		
		return $query;
	}
	
	/*
	*	Get total trip amount
	*
	*/
	public function get_total_trip_amount($trip_id)
	{
		$this->db->select('SUM(trip_amount) AS total_amount');
	 	$this->db->where('trip.trip_id = '.$trip_id);
		$query = $this->db->get('trip');
		
		$total_amount = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$total_amount = $row->total_amount;
		}
		
		return $total_amount;
	}
	public function get_total_expense_today($expense_id,$personnel_id)
	{
		$today = date('Y-m-d');
		$this->db->select('SUM(trip_expense_amount) AS total_amount');
	 	$this->db->where('trip_expense.expense_id = '.$expense_id.' AND trip_expense.expense_date = "'.$today.'" AND vehicle.vehicle_id = trip_expense.vehicle_id AND expense.expense_id = trip_expense.expense_id AND expense.expense_type_id = 1 AND (vehicle.vehicle_driver = '.$personnel_id.' OR vehicle.vehicle_conductor = '.$personnel_id.')');
		$query = $this->db->get('trip_expense,vehicle,expense');
		
		$total_amount = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$total_amount = $row->total_amount;
		}
		
		return $total_amount;
	}
	public function get_admin_total_expense_today($expense_id,$personnel_id,$vehicle_id)
	{
		$today = date('Y-m-d');
		$this->db->select('SUM(trip_expense_amount) AS total_amount');
	 	$this->db->where('trip_expense.expense_id = '.$expense_id.' AND trip_expense.expense_date = "'.$today.'" AND vehicle.vehicle_id = trip_expense.vehicle_id AND expense.expense_id = trip_expense.expense_id AND expense.expense_type_id = 2 AND (vehicle.vehicle_driver = '.$personnel_id.' OR vehicle.vehicle_conductor = '.$personnel_id.')');
		$query = $this->db->get('trip_expense,vehicle,expense');
		
		$total_amount = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$total_amount = $row->total_amount;
		}
		
		return $total_amount;
	}
	public function add_trip($personnel_id)
	{
		$today = date('Y-m-d');
		//get vehicle id
		$this->db->where('vehicle_status = 1 AND vehicle_conductor = '.$personnel_id.' OR vehicle_driver = '.$personnel_id);
		$query = $this->db->get('vehicle');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$vehicle_id = $row->vehicle_id;
			
			//get total trips
			$this->db->where('vehicle_id = '.$vehicle_id.' AND trip_date = \''.$today.'\'');
			$this->db->select('trip_id');
			$query2 = $this->db->get('trip');
			
			$total_trips = $query2->num_rows();
			$total_trips++;
			
			$data = array(
				'trip_name' => $total_trips,
				'trip_date' => $today,
				'vehicle_id' => $vehicle_id,
				'created' => date('Y-m-d H:i:s'),
				'created_by' => $personnel_id,
				'modified_by' => $personnel_id
			);
			
			if($this->db->insert('trip', $data))
			{
				return TRUE;
			}
			
			else
			{
				return FALSE;
			}
		}
	}

	public function add_trip_detail($personnel_id)
	{
		$today = date('Y-m-d');
		//get vehicle id
		$route_id = $this->input->post('route_id');
		$trip_amount = $this->input->post('amount_made');

		$this->db->where('vehicle_status = 1 AND vehicle_conductor = '.$personnel_id.' OR vehicle_driver = '.$personnel_id);
		$query = $this->db->get('vehicle');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$vehicle_id = $row->vehicle_id;
			
			//get total trips
			$this->db->where('vehicle_id = '.$vehicle_id.' AND trip_date = \''.$today.'\'');
			$this->db->select('trip_id');
			$query2 = $this->db->get('trip');
			
			$total_trips = $query2->num_rows();


			$this->db->where('route_id = '.$route_id.'');
			$this->db->select('route_start,route_end');
			$route_query = $this->db->get('route');
			if($route_query->num_rows() >0)
			{
				foreach ($route_query->result() as $key_route) {
					# code...
					$route_start = $key_route->route_start;
					$route_end = $key_route->route_end;
				}
			}
			$route_name = $route_start.'-'.$route_end;
			$total_trips++;
			
			$data = array(
				'trip_name' => $route_name,
				'trip_count' => $total_trips,
				'trip_date' => $today,
				'vehicle_id' => $vehicle_id,
				'route_id' => $route_id,
				'trip_amount' => $trip_amount,
				'created' => date('Y-m-d H:i:s'),
				'created_by' => $personnel_id,
				'modified_by' => $personnel_id
			);
			
			if($this->db->insert('trip', $data))
			{
				return TRUE;
			}
			
			else
			{
				return FALSE;
			}
		}
	}
	
	public function get_assigned_matatu($personnel_id)
	{
		$this->db->where('vehicle_conductor ='.$personnel_id);
		$query = $this->db->get('vehicle');
		$vehicle_plate = '';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$vehicle_plate = $key->vehicle_plate;
			}
		}
		return $vehicle_plate;
	}
	public function get_assigned_matatu_routes($vehicle_plate)
	{
		$this->db->where('vehicle_route.vehicle_id = vehicle.vehicle_id AND route.route_id = vehicle_route.route_id AND vehicle.vehicle_plate = "'.$vehicle_plate.'"');
		$query = $this->db->get('vehicle,vehicle_route,route');
		
		return $query;
	}
	public function add_trip_expense_detail($personnel_id)
	{
		$today = date('Y-m-d');
		//get vehicle id
		$expense_id = $this->input->post('expense_id');
		$trip_amount = $this->input->post('amount_used');

		$this->db->where('vehicle_status = 1 AND vehicle_conductor = '.$personnel_id.' OR vehicle_driver = '.$personnel_id);
		$query = $this->db->get('vehicle');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$vehicle_id = $row->vehicle_id;
			
			//get total trips
		
			
			$data = array(
				'expense_id' => $expense_id,
				'trip_expense_amount' => $trip_amount,
				'expense_date' => $today,
				'vehicle_id' => $vehicle_id,
				'created' => date('Y-m-d H:i:s'),
				'created_by' => $personnel_id,
				'modified_by' => $personnel_id
			);
			
			if($this->db->insert('trip_expense', $data))
			{
				return TRUE;
			}
			
			else
			{
				return FALSE;
			}
		}
	}
	public function get_personnel_name($personnel_id)
	{
		$this->db->where('personnel_id = '.$personnel_id);
		$query = $this->db->get('personnel');
		$personnel_name =  '';
		if($query->num_rows() == 1)
		{
			foreach ($query->result() as $key_query) {
				# code...
				$personnel_fname = $key_query->personnel_fname;
				$personnel_onames = $key_query->personnel_onames;


			}
			$personnel_name = $personnel_fname.' '.$personnel_onames;
		}

		return $personnel_name;
	}
	public function get_plate_number($vehicle_id)
	{
		$this->db->where('vehicle_id = '.$vehicle_id);
		$query = $this->db->get('vehicle');
		$vehicle_plate =  '';
		if($query->num_rows() == 1)
		{
			foreach ($query->result() as $key_query) {
				# code...
				$vehicle_plate = $key_query->vehicle_plate;


			}
		}

		return $vehicle_plate;
	}
	public function get_transys_matatus()
	{
		$query = $this->db->get('vehicle');

		return $query;
	}
}