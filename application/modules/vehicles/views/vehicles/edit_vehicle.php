
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>vehicles" class="btn btn-info pull-right">Back to Vehicles</a>
                     </div>
                    </div>
                    <!-- Adding Errors -->
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
            
			//the vehicle details
			$vehicle_name = $vehicle[0]->vehicle_name;
			$vehicle_plate = $vehicle[0]->vehicle_plate;
			$vehicle_capacity = $vehicle[0]->vehicle_capacity_id;
			
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
				$vehicle_name = set_value('vehicle_name');
				$vehicle_plate = set_value('vehicle_plate');
				$vehicle_capacity = set_value('vehicle_capacity_id');
						
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
          <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
        
         <div class="form-group">
            <label class="col-lg-5 control-label">Vehicle Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="vehicle_name" placeholder="Vehicle Name" value="<?php echo $vehicle_name;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Vehicle Plate: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="vehicle_plate" placeholder="Vehicle Plate" value="<?php echo $vehicle_plate;?>">
            </div>
        </div>
        
        
    </div>
    
    <div class="col-md-6">
        
       
       <div class="form-group">
            <label class="col-lg-5 control-label">Vehicle Capacity: </label>
            
            <div class="col-lg-7">
            	<select name="vehicle_capacity_id" class="form-control">
            	<?php 
				if($capacities->num_rows() > 0)
				{
					foreach($capacities->result() as $res)
					{
						$vehicle_capacity_id = $res->vehicle_capacity_id;
						$vehicle_capacity_name = $res->vehicle_capacity_name;
						
						if($vehicle_capacity_id == $vehicle_capacity)
						{
							?>
							<option value="<?php echo $vehicle_capacity_id;?>" selected="selected"><?php echo $vehicle_capacity_name;?></option>
							<?php
						}
						
						else
						{
							?>
							<option value="<?php echo $vehicle_capacity_id;?>"><?php echo $vehicle_capacity_name;?></option>
							<?php
						}
					}
				}
				?>
                </select>
            </div>
        </div>
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Edit Vehicle
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>