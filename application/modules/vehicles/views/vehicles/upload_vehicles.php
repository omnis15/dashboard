<?php
//personnel data

$document_name = set_value('document_name');
$document_type_id = set_value('document_type_id');
#$vehicle_capacity = set_value('vehicle_capacity');


?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>vehicles" class="btn btn-info pull-right">Back to Vehicles</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
         <!-- Image -->
                    <div class="form-group">
                        <label class="col-lg-5 control-label">Select Document</label>
                        <div class="col-lg-7">
                            
                            <div class="row">
                            
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                            <img src="http://placehold.it/200x200">
                                        </div>
                                        <div>
                                            <span class="btn btn-file btn_pink"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="post_image"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
      
       
        
        
    </div>
    
    <div class="col-md-6">
        
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Document Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="document_name" placeholder="Document Name" value="<?php echo $document_name;?>">
            </div>
        </div>
          <div class="form-group">
            <label class="col-lg-5 control-label">Document Type: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="document_type_id">
                    <option value="">--Select Type--</option>
                    <?php
                        if($document_types->num_rows() > 0)
                        {
                            $documents = $document_types->result();
                            
                            foreach($documents as $res)
                            {
                                $document_type_id = $res->document_id;
                                $document_name = $res->document_name;
                                
                                if($document_type_id == $document_name)
                                {
                                    echo '<option value="'.$document_type_id.'" selected>'.$document_name.'</option>';
                                }
                                
                                else
                                {
                                    echo '<option value="'.$document_type_id.'">'.$document_name.'</option>';
                                }
                            }
                        }
                    ?>
                </select>
             </div>
        </div>
        
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Document
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
               
           
            <?php
        
        $result = '';
        
        //if users exist display them
        if ($document_details->num_rows() > 0)
        {
            $count = 0;
            
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Document Name</th>
                        <th>Document Type</th>
                        <th>Status</th>
                        <th colspan="2">Actions</th>
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
            //get all administrators
            $administrators = $this->users_model->get_active_users();
            if ($administrators->num_rows() > 0)
            {
                $admins = $administrators->result();
            }
            
            else
            {
                $admins = NULL;
            }
            
            foreach ($document_details->result() as $row)
            {

                $doc_name = $row->document_name;
                $upload_id = $row->vehicle_upload_id;
                $vehicle_id = $row->vehicle_id;
                $file_name = $row->file_name;
                $document_type_id = $row->document_type_id;
                $query2 = $this->vehicle_model->get_vehicle_document_name($document_type_id);
                if ($query2->num_rows() > 0)
                {
                    foreach ($query2->result() as $row2)
                        {
                              $docu_name = $row2->document_name;
                             
                        }
                }else{
                    $docu_name = 'First Picture';
                }
                $upload_status = $row->upload_status;
                
             
               
                
                //status
                if($upload_status == 1)
                {
                    $status = 'Active';
                }
                else
                {
                    $status = 'Disabled';
                }
                
                
                //create deactivated status display
                if($upload_status == 0)
                {
                    $status = '<span class="label label-default">Deactivated</span>';
                    $button = '<a class="btn btn-info" href="'.site_url().'customers/activate_company_contact/'.$upload_id.'" onclick="return confirm(\'Do you want to activate '.$doc_name.'?\');" title="Activate '.$doc_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';

                }
                //create activated status display
                else if($customer_contacts_status == 1)
                {
                    $status = '<span class="label label-success">Active</span>';
                    $button = '<a class="btn btn-default" href="'.site_url().'customers/deactivate_company_contact/'.$upload_id.'" onclick="return confirm(\'Do you want to deactivate '.$doc_name.'?\');" title="Deactivate '.$doc_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
                }
                
                
                
                
                $count++;
                $result .= 
                '
                    <tr>
                        <td>'.$count.'</td>
                        <td>'.$doc_name.'</td>
                        <td>'.$docu_name.'</td>
                         <td>'.$status.'</td>
                        <td><a href="'.site_url().'vehicles/uploaded_docs/'.$file_name.'" class="btn btn-sm btn-default" title="View '.$doc_name.'"><i class="fa fa-paperclip"></i> View</a></td>
                       
                        
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no Documents";
        }
        echo $result;
?>
 
 </div>
            </section>
