<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Vehicle Plate</th>
						<th>Capacity</th>
						<th>Name</th>
						<th>Status</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				
				$vehicle_id = $row->vehicle_id;
				$vehicle_name = $row->vehicle_name;
				$vehicle_plate = $row->vehicle_plate;
				$vehicle_capacity = $row->vehicle_capacity_name;
				$vehicle_status = $row->vehicle_status;
				
				//status
				if($vehicle_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($vehicle_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'vehicles/activate_vehicles/'.$vehicle_id.'" onclick="return confirm(\'Do you want to activate '.$vehicle_plate.'?\');" title="Activate '.$vehicle_plate.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($vehicle_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'vehicles/deactivate_vehicles/'.$vehicle_id.'" onclick="return confirm(\'Do you want to deactivate '.$vehicle_plate.'?\');" title="Deactivate '.$vehicle_plate.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$vehicle_plate.'</td>
						<td>'.$vehicle_capacity.'</td>
						<td>'.$vehicle_name.'</td>
						
						<td>'.$status.'</td>
			            <td><a href="'.site_url().'vehicles/upload_vehicles/'.$vehicle_id.'" class="btn btn-sm btn-default" title="Profile '.$vehicle_plate.'"><i class="fa fa-paperclip"></i> Uploads</a></td>
						
						<td><a href="'.site_url().'vehicles/edit_vehicles/'.$vehicle_id.'" class="btn btn-sm btn-success" title="Profile '.$vehicle_plate.'"><i class="fa fa-pencil"></i> Edit</a></td>
						<td>'.$button.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no vehicles";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                                    <div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>vehicles/add_vehicles" class="btn btn-sm btn-info pull-right">Add Vehicles</a>
                                     </div>
                                     
                            </div>       
				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

