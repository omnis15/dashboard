<?php

class Vehicle_model extends CI_Model 
{
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_vehicle_document_name($document_type_id)
	{
		//retrieve all users
		$this->db->from('vehicle_document_types');
		$this->db->select('*');
		$this->db->where('document_id = '.$document_type_id);
		$query = $this->db->get();
		
		return $query;
		
	}
	
	public function get_capacities()
	{
		//retrieve all users
		$this->db->from('vehicle_capacity');
		$this->db->order_by('vehicle_capacity_name', 'ASC');
		$query = $this->db->get();
		
		return $query;
		
	}
	public function get_vehicle_document_types()
	{
		//retrieve all users
		$this->db->from('vehicle_document_types');
		$this->db->select('*');
		$this->db->where('document_status = 1');
		$query = $this->db->get();
		
		return $query;
		
	}
	public function get_vehicle_document_details($vehicle_id)
	{
		//retrieve all users
		$this->db->from('vehicle_upload');
		$this->db->select('*');
		$this->db->where('vehicle_id = '.$vehicle_id);
		$query = $this->db->get();
		
		return $query;
		
	}
	public function get_all_vehicles($table, $where, $per_page, $page, $order = 'vehicle_plate', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
    public function get_all_vehicles_drivers()
	{
		//retrieve all users
		$this->db->from('vehicle');
		$this->db->select('*');
		$this->db->where('vehicle_status = 1 AND vehicle_id NOT IN (SELECT vehicle_id FROM personnel_vehicle WHERE personnel_vehicle_status = 1)');
		$this->db->order_by('vehicle_plate', 'ASC');
		$query = $this->db->get('');
		
		return $query;
	}

	
	public function get_all_customer_contacts($table, $where, $per_page, $page, $order = 'customer_contacts_first_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	

/*
	*	Add a new vehicle
	*	@param string $image_name
	*
	*/
	public function add_vehicle_upload($file_name, $vehicle_id)
	{
		$data = array(
				'document_name'=>$this->input->post('document_name'),
				'document_type_id'=>$this->input->post('document_type_id'),
				'vehicle_id'=>$vehicle_id,
				'file_name' => $file_name
				
				
				
				
				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('vehicle_upload', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	/*
	*	Add a new vehicle
	*	@param string $image_name
	*
	*/
	public function add_vehicle()
	{
		$data = array(
				'vehicle_name'=>$this->input->post('vehicle_name'),
				'vehicle_plate'=>$this->input->post('vehicle_plate'),
				'vehicle_capacity_id'=>$this->input->post('vehicle_capacity_id')
				
				
				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('vehicle', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_vehicle($vehicle_id)
	{
		$data = array(

				'vehicle_name'=>$this->input->post('vehicle_name'),
				'vehicle_plate'=>$this->input->post('vehicle_plate'),
				'vehicle_capacity_id'=>$this->input->post('vehicle_capacity_id')
				
				
			);
			
		$this->db->where('vehicle_id', $vehicle_id);
		if($this->db->update('vehicle', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_vehicle($vehicle_id)
	{
		//retrieve all users
		$this->db->from('vehicle');
		$this->db->select('*');
		$this->db->where('vehicle_id = '.$vehicle_id);
		$query = $this->db->get();
		
		return $query;
	}
	public function get_vehicle_plate($vehicle_id)
	{
		$vehicle_plate=0;
		$this->db->select('vehicle_plate');
		$this->db->where('vehicle.vehicle_id = '.$vehicle_id);
		$query2 = $this->db->get('vehicle');
		$total2 = $query2->row();
		$vehicle_plate = $total2->vehicle_plate;
		return $vehicle_plate;
	}
	


	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_vehicle($vehicle_id)
	{
		$data = array(
				'vehicle_status' => 1
			);
		$this->db->where('vehicle_id', $vehicle_id);
		
		if($this->db->update('vehicle', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_vehicle($vehicle_id)
	{
		$data = array(
				'vehicle_status' => 0
			);
		$this->db->where('vehicle_id', $vehicle_id);
		
		if($this->db->update('vehicle', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	
}
?>