<?php
class Reports_model extends CI_Model
{
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_rides($table, $where, $per_page, $page, $order = 'customer_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('ride.*, ride_status.ride_status_name, personnel.personnel_fname, personnel.personnel_onames, vehicle.vehicle_plate, customer.customer_first_name');
		$this->db->join('personnel', 'ride.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('vehicle', 'ride.vehicle_id = vehicle.vehicle_id', 'left');
		$this->db->join('customer', 'ride.customer_id = customer.customer_id', 'left');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->order_by('start_time', 'DESC');
		$this->db->order_by('end_time', 'DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_scheduled_rides($table, $where, $per_page, $page, $order = 'customer_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('ride.*, ride_status.ride_status_name, personnel.personnel_fname, personnel.personnel_onames, vehicle.vehicle_plate, customer.customer_first_name');
		$this->db->join('personnel', 'ride.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('vehicle', 'ride.vehicle_id = vehicle.vehicle_id', 'left');
		$this->db->join('customer', 'ride.customer_id = customer.customer_id', 'left');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->order_by('start_time', 'DESC');
		$this->db->order_by('end_time', 'DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_active_drivers($ride_id, $ride_type_id, $ride_date_scheduled)
	{
		//retrieve all users
		// $this->db->from('personnel_vehicle, vehicle, personnel');
		// $this->db->select('vehicle_plate, personnel_fname, personnel_onames, personnel.personnel_id');
		// $this->db->where('personnel_vehicle.vehicle_id = vehicle.vehicle_id AND personnel_vehicle_status =1
		// 	AND personnel.personnel_status =1
		// 	AND personnel.personnel_id = personnel_vehicle.personnel_id
		// 	AND vehicle.vehicle_capacity_id = '.$ride_type_id.'
		// 	AND personnel.personnel_id NOT
		// 	IN (
		// 	SELECT personnel_id
		// 	FROM ride
		// 	WHERE ride.ride_date_scheduled >= '.$ride_date_scheduled.'
		// 	AND ride.personnel_id IS NOT NULL
		// 	AND (ride.ride_status_id = 2 
		// 	OR ride.ride_status_id = 3 )
		// 	AND ride.ride_book = 0

		// 	)');

		$this->db->from('vehicle, personnel');
		$this->db->select('vehicle_plate, personnel_fname, personnel_onames, personnel.personnel_id');
		$this->db->where('vehicle.vehicle_driver = personnel.personnel_id 
			AND personnel.personnel_status =1
			AND vehicle.vehicle_capacity_id = '.$ride_type_id.'
			AND personnel.personnel_id NOT
			IN (
			SELECT personnel_id
			FROM ride
			WHERE ride.ride_date_scheduled >= '.$ride_date_scheduled.'
			AND ride.personnel_id IS NOT NULL
			AND (ride.ride_status_id = 2 
			OR ride.ride_status_id = 3 )
			AND ride.ride_book = 0

			)');
		$query = $this->db->get('');
		
		return $query;
	}
	public function get_ride_type_id($ride_id)
	{
		$ride_type_id='0';
		$this->db->select('ride_type_id');
		$this->db->where('ride_id = '.$ride_id);
		$query = $this->db->get('ride');
		$total = $query->row();
		$ride_type_id = $total->ride_type_id;
		return $ride_type_id;
	}
	public function get_ride_date_scheduled($ride_id)
	{
		$ride_date_scheduled='0';
		$this->db->select('ride_date_scheduled');
		$this->db->where('ride_id = '.$ride_id);
		$query = $this->db->get('ride');
		$total = $query->row();
		$ride_date_scheduled = $total->ride_date_scheduled;
		return $ride_date_scheduled;
	}
	public function update_personnel_rides($ride_id)
	{
		$personnel_id = $this->input->post('personnel_id');
		$vehicle_id = $this->get_personnel_vehicle($personnel_id);
		$data = array(

				'personnel_id'=>$this->input->post('personnel_id'),
				'vehicle_id'=>$vehicle_id,
				'ride_book'=>3
			);
			
		$this->db->where('ride_id', $ride_id);
		if($this->db->update('ride', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_personnel_vehicle($personnel_id)
	{
		$this->db->where('vehicle_driver',$personnel_id);
		$query= $this->db->get('vehicle');

		foreach ($query->result() as $key) {
			# code...
			$vehicle_id = $key->vehicle_id;
		}

		return $vehicle_id;
	}

	public function notify_ride_rider($message_title = 'Ubiker', $message = NULL,$personnel_id)
	{
		if($message != NULL)
		{
			//get riders
			$where = 'personnel_status.personnel_id = personnel.personnel_id AND personnel_status.personnel_status_status = 1 AND personnel_status.personnel_status_availability = 1 AND personnel.registration_id IS NOT NULL AND personnel.personnel_id = '.$personnel_id;
			$this->db->where($where);
			$query = $this->db->get('personnel, personnel_status');
			
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $res)
				{
					$to = $res->registration_id;
					$title = $message_title;
					$result = $this->send_push_notification($to, $title, $message);
				}
			}
		}
	}
	
	 
}