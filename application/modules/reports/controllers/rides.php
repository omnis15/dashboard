<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rides extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('customers/customers_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('drivers/driver_model');
		$this->load->model('admin/admin_model');
		$this->load->model('accounts/ride_model');
		$this->load->model('vehicles/vehicle_model');
		$this->load->model('reports/reports_model');
		
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
	
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index($order = 'ride_date', $order_method = 'DESC') 
	{
		/*$where = 'ride.ride_status_id = ride_status.ride_status_id AND ride.vehicle_id = vehicle.vehicle_id AND ride.personnel_id = personnel.personnel_id';
		$table = 'ride, ride_status, vehicle, personnel';*/
		$where = 'ride.ride_status_id = ride_status.ride_status_id AND ride_book != 1';
		$where_two = 'ride.ride_status_id = ride_status.ride_status_id AND ride_book = 1';
		$table = 'ride, ride_status';
		//pagination
		$segment = 7;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'reports/rides/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_rides($table, $where, $config["per_page"], $page, $order, $order_method);
		$query_scheduled = $this->reports_model->get_all_scheduled_rides($table, $where_two, $config["per_page"], $page, $order, $order_method);
		$v_data["query_scheduled"] = $query_scheduled;
		//drivers
		$all_drivers = $this->driver_model->get_drivers();
		$drivers = '';
		
		if($all_drivers->num_rows() > 0)
		{
			foreach ($all_drivers->result() as $row) :
				$personnel_id = $row->personnel_id;
				$personnel_number = $row->personnel_number;
				$personnel_fname = $row->personnel_fname;
				$personnel_onames = $row->personnel_onames;
				$personnel_phone = $row->personnel_phone;
				
				//$name = $personnel_first_name.' '.$personnel_surname.' | '.$personnel_phone." | ".$personnel_number;
				$name = $personnel_fname.' '.$personnel_onames;
				$drivers .= "<option value='".$personnel_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data["drivers"] = $drivers;
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		//ride statuses
		$ride_statuses = $this->ride_model->get_ride_statuses();
		$statuses = '';
		
		if($ride_statuses->num_rows() > 0)
		{
			foreach ($ride_statuses->result() as $row) :
				$ride_status_id = $row->ride_status_id;
				$ride_status_name = $row->ride_status_name;
				
				$name = $ride_status_name;
				$statuses .= "<option value='".$ride_status_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data['statuses'] = $statuses;
		
		//vehicles
		$vehicles_query = $this->ride_model->get_vehicles();
		$vehicles = '';
		
		if($vehicles_query->num_rows() > 0)
		{
			foreach ($vehicles_query->result() as $row) :
				$vehicle_id = $row->vehicle_id;
				$vehicle_plate = $row->vehicle_plate;
				
				$name = $vehicle_plate;
				$vehicles .= "<option value='".$vehicle_id."'>".$name."</option>";
	
			endforeach;
		}
		$v_data['vehicles'] = $vehicles;
		
		$data['title'] = 'Rides';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('rides', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function cancel_ride($ride_id)
	{
		$this->db->where('ride_id', $ride_id);
		if($this->db->update('ride', array('ride_status_id' => 5)))
		{
			$this->session->set_userdata('success_message', 'Ride cancelled successfully');
		}
		
		else
		{
			$this->session->set_userdata('success_message', 'Ride could not be cancelled');
		}
		
		redirect('reports/rides');
	}
	
	public function delete_ride($ride_id)
	{
		$this->db->where('ride_id', $ride_id);
		if($this->db->delete('ride'))
		{
			$this->session->set_userdata('success_message', 'Ride deleted successfully');
		}
		
		else
		{
			$this->session->set_userdata('success_message', 'Ride could not be deleted');
		}
		
		redirect('reports/rides');
	}
	public function assign_the_booking_ride($ride_id) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('personnel_id', 'personnel_id', 'required|xss_clean');
		

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($result = $this->reports_model->update_personnel_rides($ride_id)){

				// get ride detail
				$this->db->where('ride_id',$ride_id);
				$query = $this->db->get('ride');

				foreach ($query->result() as $key) {
					# code...
					$from_route = $key->from_route;
					$to_route = $key->to_route;
				}
				$personnel_id = $this->input->post('personnel_id');

				
				$title = 'New delivery requested';
				$message = 'A new request has come from '.$from_route.' to '.$to_route;
				$this->reports_model->notify_ride_rider($title, $message,$personnel_id);


				$this->session->set_userdata('success_message', 'Driver Successfully allocated Ride');
			    redirect('reports/rides');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Driver Could not be  allocated to the Ride');

			}
		}
		else
			{
				$this->session->set_userdata('error_message', validation_errors());
			}
		
		
		//open the add new Customers
		
		$data['title'] = 'Allocate Driver to Ride';
		$v_data['title'] = $data['title'];
		$ride_type_id = $this->reports_model->get_ride_type_id($ride_id);
		$ride_date_scheduled = $this->reports_model->get_ride_date_scheduled($ride_id);

		$v_data['active_drivers'] = $this->reports_model->get_all_active_drivers($ride_id, $ride_type_id, $ride_date_scheduled);
		
		$data['content'] = $this->load->view('reports/allocate_driver', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
}