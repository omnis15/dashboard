<?php

class Customers_model extends CI_Model 
{	
	/*
	*	Retrieve all customers
	*
	*/
	public function all_customers()
	{
		$this->db->where('customer_status = 1');
		$query = $this->db->get('customer');
		
		return $query;
	}
	
	/*
	*	Retrieve all ride types
	*
	*/
	public function get_ride_types()
	{
		$this->db->order_by('ride_type_name');
		$query = $this->db->get('ride_type');
		
		return $query;
	}
	
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_customers($table, $where, $per_page, $page, $order = 'customer_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	public function get_all_customer_contacts($table, $where, $per_page, $page, $order = 'customer_contacts_first_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	

	/*
	*	Add a new customer
	*	@param string $image_name
	*
	*/
	public function add_company_contact($company_id)
	{
		$data = array(
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				'customer_id'=>$company_id
				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	function get_payment_methods()
	{
		$table = "payment_method";
		$where = "payment_method_id > 0";
		$items = "*";
		$order = "payment_method";
		
		$this->db->order_by($order);
		$result = $this->db->get($table);
		
		return $result;
	}
	

	function create_cat_number()
	{
		//select product code
		$preffix = "JOP-RN_";
		$this->db->from('utu_payments');
		$this->db->where("reciept_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(reciept_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	function create_invoice_number()
	{
		//select product code
		$preffix = "UTU-INV-";
		$this->db->from('invoice');
		$this->db->where("invoive_number LIKE '".$preffix."%' AND invoice_status = 1");
		$this->db->select('MAX(invoive_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	public function get_invoices($customer_id)
	{
		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get('invoice');
		
		return $query;
	}
	public function get_cust_rides($invoice_id,$customer_id)
	{
		$this->db->where('customer_id = '.$customer_id.' AND invoice_id = '.$invoice_id);
		$query = $this->db->get('ride');
		
		return $query;
	}

	public function cat_payment($customer_id)
	{
		$amount = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');
		
		
		
		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}
		$receipt_number = $this->customers_model->create_cat_number();

		$data = array(
			'payment_method_id'=>$payment_method,
			'amount'=>$amount,
			'customer_id'=>$customer_id,
			'transaction_code'=>$transaction_code,
			'reciept_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_status'=>1,
			'status'=>1
			
		);
		if ($this->db->insert('utu_payments',$data)){


				
				return TRUE;
			}
			else{
				return FALSE;
			}
	}

	public function add_invoice($customer_id)
	{
		$invoice_number = $this->customers_model->create_invoice_number();
		$data = array(
				'invoice_date'=>$this->input->post('invoice_date'),
				'customer_id'=>$customer_id,
				'invoive_number'=>$invoice_number,
				'invoice_status'=>1,
				'created_by'=>$this->session->userdata('personnel_id'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
			
		//retrieve all users
		$this->db->from('ride');
		$this->db->select('*');
		$this->db->where('((invoice_id = 0) || (invoice_id IS NULL)) AND (ride_date >= "'.$start_date.'" OR ride_date <= "'.$end_date.'")');
		$query = $this->db->get();
			
		if ($query->num_rows() > 0)
		{
			if($this->db->insert('invoice', $data))
			{
				$last_id = $this->db->insert_id();
				$data2 = array(
					'invoice_id' => $last_id
				);
				
				foreach ($query->result() as $row)
				{
					$ride_id = $row->ride_id;
					$invoice_id = $row->invoice_id;
					
					$this->db->where('ride_id', $ride_id);
					if($this->db->update('ride', $data2))
					{
						
					}
				
				}

				$invoice_amount='0';
				$this->db->select('SUM(ride.cost) AS total_cost');
				$this->db->where('ride.ride_status_id = 4 AND ride.invoice_id = '.$last_id);
				$query2 = $this->db->get('ride');
				$total2 = $query2->row();
				$invoice_amount = $total2->total_cost;

				//var_dump($invoice_amount);die();
				$data3 = array(
					'amount' => $invoice_amount
				);
				
				$this->db->where('invoice_id', $last_id);
				if($this->db->update('invoice', $data3))
				{
					return TRUE;
				}

				
			}
		}
		else
		{
			return FALSE;
		}
		
		
		
	}
	
	/*
	*	Add a new customer
	*	@param string $image_name
	*
	*/
	public function add_customer($is_company)
	{
		if($is_company == 0){
			$one = 1;
		}
		else{
			$one = $this->input->post('customer_type_id');
		}
		$data = array
		(
			'customer_surname'=>$this->input->post('customer_surname'),
			'customer_first_name'=>$this->input->post('customer_first_name'),
			'customer_phone'=>$this->input->post('customer_phone'),
			'customer_email'=>$this->input->post('customer_email'),
			'customer_post_code'=>$this->input->post('customer_post_code'),
			'customer_address'=>$this->input->post('customer_address'),
			'customer_rate'=>$this->input->post('customer_rate'),
			'customer_van_rate'=>$this->input->post('customer_van_rate'),
			'customer_number'=>$this->create_customer_number(),
			'customer_created'=>date('Y-m-d H:i:s'),
			'customer_type_id'=>$one


			#'created_by'=>$this->session->userdata('personnel_id'),
			#'modified_by'=>$this->session->userdata('personnel_id')
		);
			
		if($this->db->insert('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	
	/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_customer($customer_id, $is_company)
	{
		if($is_company == 0){
			$one = 1;
		}
		else
		{
			$one = $this->input->post('customer_type_id');
		}
		$data = array(
			'customer_surname'=>$this->input->post('customer_surname'),
			'customer_first_name'=>$this->input->post('customer_first_name'),
			'customer_phone'=>$this->input->post('customer_phone'),
			'customer_email'=>$this->input->post('customer_email'),
			'customer_post_code'=>$this->input->post('customer_post_code'),
			'customer_address'=>$this->input->post('customer_address'),
			'customer_rate'=>$this->input->post('customer_rate'),
			'customer_van_rate'=>$this->input->post('customer_van_rate'),
			'customer_type_id'=>$one
		);
		
		$this->db->where('customer_id', $customer_id);
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer($customer_id)
	{
		//retrieve all users
		$this->db->from('customer');
		$this->db->select('*');
		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customers()
	{
		//retrieve all users
		$this->db->from('customer');
		$this->db->select('*');
		$this->db->order_by('customer_first_name', 'ASC');
		$this->db->order_by('customer_surname', 'ASC');
		$this->db->order_by('customer_phone', 'ASC');
		$query = $this->db->get();
		
		return $query;
	}

/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_company_contact($customer_contact_id) 
	{

		$data = array(
			
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				
				
			);
			
		$this->db->where('customer_contacts_id', $customer_contact_id);
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer_contact($customer_contact_id)
	{
		//retrieve all users
		$this->db->from('customer_contacts');
		$this->db->select('*');
		$this->db->where('customer_contacts_id = '.$customer_contact_id);
		$query = $this->db->get();
		
		return $query;
	}
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 1
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 0
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Delete an existing customer
	*	@param int $customer_id
	*
	*/
	public function delete_customer($customer_id)
	{
		if($this->db->delete('customer', array('customer_id' => $customer_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 1
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 0
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Retrieve personnel
	*
	*/
	public function get_personnel_type($personnel_type_id)
	{
		$this->db->where(array('personnel_status' => 1, 'personnel_type_id' => $personnel_type_id));
		$query = $this->db->get('personnel');
		
		return $query;
	}
	
	/*
	*	Retrieve personnel
	*
	*/
	public function get_customer_types()
	{
		$this->db->where('customer_type_id > 1');
		$query = $this->db->get('customer_type');
		
		return $query;
	}
	
	//import template
	function import_customers_template()
	{
		$this->load->library('Excel');
		
		$title = 'Customers Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Customer number';
		$report[$row_count][1] = 'First name';
		$report[$row_count][2] = 'Last name';
		$report[$row_count][3] = 'Email Address';
		$report[$row_count][4] = 'Phone Number';
		$report[$row_count][5] = 'Address';
		$report[$row_count][6] = 'City';
		$report[$row_count][7] = 'Post Code';
		$report[$row_count][8] = 'Rate';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	
	//import Company template
	function import_companies_template()
	{
		$query = $this->get_customer_types();
		$types = '(';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$customer_type_id = $res->customer_type_id;
				$customer_type_name = $res->customer_type_name;
				
				$types .= $customer_type_name." - ".$customer_type_id.", ";
			}
		}
		$types .= ')';
		
		$this->load->library('Excel');
		
		$title = 'Companies Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Customer number';
		$report[$row_count][1] = 'Company name';
		$report[$row_count][2] = 'Registration Number';
		$report[$row_count][3] = 'Email Address';
		$report[$row_count][4] = 'Phone Number';
		$report[$row_count][5] = 'Address';
		$report[$row_count][6] = 'City';
		$report[$row_count][7] = 'Post Code';
		$report[$row_count][8] = 'Rate';
		$report[$row_count][9] = 'Customer Type '.$types;
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	//import customers
	public function import_csv_customers($upload_path, $company_status)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_customers_data($array, $company_status);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	//sort customers imported data
	public function sort_customers_data($array, $company_status)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		
		//if products exist in array
		if(($total_rows > 0) && (($total_columns == 9) || ($total_columns == 10)))
		{
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Customer Number</th>
						  <th>First Name</th>
						  <th>Last Name</th>
						  <th>Email</th>
						  <th>Phone</th>
						  <th>Address</th>
						  <th>City</th>
						  <th>Post Code</th>
						  <th>Comment</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				//$items['personnel_onames'] = $items['personnel_nssf_number'] = $items['personnel_fname'] = $items['created']= $items['modified_by'] = $items['modified_by'] = $items['created_by'] = $items['personnel_nhif_number']=$items['personnel_kra_pin']=$items['gender_id']=$items['personnel_national_id_number']=$items['bank_branch_id']=$items['bank_account_number']=$items['personnel_email']=$items['staff_id']=$items['cost_center']=$items['engagement_date']=$items['date_of_exit']='';
				$items = array();
				$items['company_status'] = $company_status;
				$customer_number = $items['customer_number'] = $array[$r][0];
				$customer_phone = '';
				if(empty($customer_number))
				{
					$customer_number = $items['customer_number'] = $this->customers_model->create_customer_number();
				}
				if(!empty($array[$r][1]))
				{
					$items['customer_first_name'] = mysql_real_escape_string(ucwords(strtolower($array[$r][1])));
				}
				
				if(!empty($array[$r][2]))
				{
					$items['customer_surname'] = mysql_real_escape_string(ucwords(strtolower($array[$r][2])));
				}
				
				if(!empty($array[$r][3]))
				{
					$items['customer_email']  = mysql_real_escape_string(ucwords(strtolower($array[$r][3])));
				}
				
				if(!empty($array[$r][4]))
				{
					$customer_phone = $items['customer_phone']=$array[$r][4];
				}
				
				if(!empty($array[$r][5]))
				{
					$items['customer_address']=$array[$r][5];
				}
				
				if(!empty($array[$r][6]))
				{
					$items['customer_city']=$array[$r][6];
				}
				
				if(!empty($array[$r][7]))
				{
					$items['customer_post_code']=$array[$r][7];
				}
				
				if(!empty($array[$r][8]))
				{
					$items['customer_rate'] = $array[$r][8];
				}
				
				if(isset($array[$r][9]))
				{
					$customer_type_id = $array[$r][9];
					//echo $customer_type_id; die();
				
					if($customer_type_id > 0)
					{
						$items['customer_type_id'] = $customer_type_id;
					}
				}
				
				else
				{
					$items['customer_type_id'] = 1;
				}
				$items['customer_created'] = date('Y-m-d H:i:s');
				/*$items['modified_by'] = $this->session->userdata('personnel_id');
				$items['created_by'] = $this->session->userdata('personnel_id');*/
				
				$comment = '';
				if(!empty($customer_number))
				{
					// check if the number already exists
					if(($this->check_current_customer_exisits($customer_number, $customer_phone)) == TRUE)
					{
						//number exists then update existing data
						$data = array(
							'customer_number' => $customer_number
						);
						$this->db->where($data);
						$this->db->update('customer', $items);
						$comment .= '<br/>Duplicate customer number entered, personnel data updated successfully';
						$class = 'warning';
					}
					else
					{
						// number does not exisit
						//save product in the db
						//var_dump($items);die();
						if($this->db->insert('customer', $items))
						{
							$comment .= '<br/>Customer successfully added to the database';
							$class = 'success';
						}
						
						else
						{
							$comment .= '<br/>Internal error. Could not add personnel to the database. Please contact the site administrator';
							$class = 'danger';
						}
					}
				}
				
				else
				{
					$comment .= '<br/>Not saved ensure you have a member number entered';
					$class = 'danger';
				}
				
				
				$response .= '
					
						<tr class="'.$class.'">
							<td colspan="7">'.$comment.'</td>
						</tr> 
				';
			}
			
			$response .= '</table>';
			
			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	
	public function create_customer_number()
	{
		//select product code
		$preffix = "UTU-";
		$this->db->from('customer');
		$this->db->where("customer_number LIKE '%".$preffix."%'");
		$this->db->select('MAX(customer_number) AS number');

		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number) * 1;
			$real_number++;
			$number = $preffix.sprintf('%04d', $real_number);
		}
		else
		{
			$number = $preffix.sprintf('%04d', 1);
		}
		return $number;
	}
	
	public function check_current_customer_exisits($customer_number, $customer_phone = NULL)
	{
		$this->db->where(array ('customer_number'=> $customer_number));
		
		$query = $this->db->get('customer');
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
		
	}
	function get_payments($customer_id)
	{
		$this->db->where('status = 1 AND customer_id = '.$customer_id);
		$query = $this->db->get('utu_payments');
		
		return $query;
	}
}
?>