<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Customer name</th>
						<th>Customer Number</th>
						<th>Phone Number</th>
						<th>Email</th>
						<th>Type</th>
						<th>Status</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$customer_id = $row->customer_id;
				$customer_type_id = $row->customer_type_id;
				$customer_first_name = $row->customer_first_name;
				$customer_number = $row->customer_number;
				$customer_surname = $row->customer_surname;
				$customer_phone = $row->customer_phone;
				$customer_email = $row->customer_email;
				$customer_status = $row->customer_status;
				$customer_created = $row->customer_created;
				$company_status = $row->company_status;
				$customer_type_name = $row->customer_type_name;
				$customer_name = $customer_first_name.' '.$customer_surname;
				
				//status
				if($customer_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				//status
				if($customer_type_id > 1)
				{
					$company_status_sent = 'Company';
					$button_sent = '<a href="customers/add_person/'.$customer_id.'" class="btn btn-sm btn-info pull-right">Contact Persons</a>';
			        $button_edit ='<a href="'.site_url().'customers/edit_company/'.$customer_id.'" class="btn btn-sm btn-success" title="Profile '.$customer_name.'"><i class="fa fa-pencil"></i> Edit</a>';
				}
				else
				{
					$company_status_sent = 'Individual';
					$button_sent = '';
					$button_edit ='<a href="'.site_url().'customers/edit_customer/'.$customer_id.'" class="btn btn-sm btn-success" title="Profile '.$customer_name.'"><i class="fa fa-pencil"></i> Edit</a>';
				}
				
				//create deactivated status display
				if($customer_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'customers/activate-customer/'.$customer_id.'" onclick="return confirm(\'Do you want to activate '.$customer_name.'?\');" title="Activate '.$customer_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($customer_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'customers/deactivate-customer/'.$customer_id.'" onclick="return confirm(\'Do you want to deactivate '.$customer_name.'?\');" title="Deactivate '.$customer_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$customer_first_name.'</td>
						<td>'.$customer_number.'</td>
						<td>'.$customer_phone.'</td>
						<td>'.$customer_email.'</td>
						<td>'.$customer_type_name.'</td>
						<td>'.$status.'</td>
						<td>'.$button_sent.'</td>
						<td>'.$button_edit.'</td>
						<td>'.$button.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no customer";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                    <div class="col-sm-2 col-lg-offset-4">
                        <a href="<?php echo site_url();?>customers/add_customer" class="btn btn-sm btn-info">Add Individual</a>
                     </div>
                    <div class="col-sm-2">
                        <a href="#" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#import_customer">Import Customer</a>
                    </div>
                    <div class="col-sm-2">
                        <a href="<?php echo site_url();?>customers/add_company" class="btn btn-sm btn-primary">Add Company</a>
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#import_company">Import Company</a>
                    </div>
                </div>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="import_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Import Customers</h4>
			</div>
			<div class="modal-body">
				<section class="panel">
                    <!-- Widget content -->
                    <div class="panel-body">
                    <div class="padd">
                        
                    <div class="row">
                    <div class="col-md-12">
                    
                        <?php echo form_open_multipart('customers/import-customers/0', array("class" => "form-horizontal", "role" => "form"));?>
                        <div class="row">
                            <div class="col-md-12">
                                <ul>
                                    <li>Download the import template <a href="<?php echo site_url().'customers/customers-template';?>">here.</a></li>
                                    
                                    <li>Save your file as a <strong>CSV (Comma Delimited)</strong> file before importing</li>
                                    <li>After adding your customers to the import template please import them using the button below</li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="row">
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <div class="fileUpload btn btn-primary">
                                    <span>Import Customers</span>
                                    <input type="file" class="upload"  name="import_csv"/>
                                </div>
                            </div>
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <input type="submit" onChange="this.form.submit();" class="btn btn-warning" onclick="return confirm('Do you really want to upload the selected file?')" value="Import">
                            </div>
                        </div>
                               
                                
                    </div>
                    </div>
                        <?php echo form_close();?>
                    </div>
                    </div>
            
            </section>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Companies Modal -->
<div class="modal fade" id="import_company" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Import Companies</h4>
			</div>
			<div class="modal-body">
				<section class="panel">
                    <!-- Widget content -->
                    <div class="panel-body">
                    <div class="padd">
                        
                    <div class="row">
                    <div class="col-md-12">
                    
                        <?php echo form_open_multipart('customers/import-customers/1', array("class" => "form-horizontal", "role" => "form"));?>
                        <div class="row">
                            <div class="col-md-12">
                                <ul>
                                    <li>Download the import template <a href="<?php echo site_url().'customers/companies-template';?>">here.</a></li>
                                    
                                    <li>Save your file as a <strong>CSV (Comma Delimited)</strong> file before importing</li>
                                    <li>After adding your customers to the import template please import them using the button below</li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="row">
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <div class="fileUpload btn btn-primary">
                                    <span>Import Companies</span>
                                    <input type="file" class="upload"  name="import_csv"/>
                                </div>
                            </div>
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <input type="submit" onChange="this.form.submit();" class="btn btn-warning" onclick="return confirm('Do you really want to upload the selected file?')" value="Import">
                            </div>
                        </div>
                               
                                
                    </div>
                    </div>
                        <?php echo form_close();?>
                    </div>
                    </div>
            
            </section>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>