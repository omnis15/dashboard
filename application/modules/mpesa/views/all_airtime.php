<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>human-resource/export-personnel" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <div class="col-lg-12">
                                    	<a href="<?php echo site_url();?>airtime/add-airtime" class="btn btn-sm btn-info pull-right">Add Campaign Airtime</a>
                                    </div>
                                </div>
<div class="row">
    <div class="col-lg-3">
        <div class="hpanel">
            <div class="panel-body text-center h-200">
                <i class="pe-7s-graph1 fa-4x"></i>

                <h1 class="m-xs"> kes.<?php echo $query_ten ?></h1>

                <h3 class="font-extra-bold no-margins text-success">
                    Kes. 10 Safaricom Airtime
                </h3>
                <small>Safaricom Airtime Totals of 10 Bob.</small>
            </div>
            <div class="panel-footer">
                Campaign Airtime
            </div>
        </div>
    </div>
     <div class="col-lg-3">
        <div class="hpanel">
            <div class="panel-body text-center h-200">
                <i class="pe-7s-graph1 fa-4x"></i>

                <h1 class="m-xs"> kes.<?php echo $query_twenty ?></h1>

                <h3 class="font-extra-bold no-margins text-success">
                    Kes. 20 Safaricom Airtime
                </h3>
                <small>Safaricom Airtime Totals of 20 Bob.</small>
            </div>
            <div class="panel-footer">
                Campaign Airtime
            </div>
        </div>
    </div>
     <div class="col-lg-3">
        <div class="hpanel">
            <div class="panel-body text-center h-200">
                <i class="pe-7s-graph1 fa-4x"></i>

                <h1 class="m-xs"> kes.<?php echo $query_fifty ?></h1>

                <h3 class="font-extra-bold no-margins text-success">
                    Kes. 50 Safaricom Airtime
                </h3>
                <small>Safaricom Airtime Totals of 50 Bob.</small>
            </div>
            <div class="panel-footer">
                Campaign Airtime
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="hpanel">
            <div class="panel-body text-center h-200">
                <i class="pe-7s-graph1 fa-4x"></i>

                <h1 class="m-xs"> kes.<?php echo $query_hundred ?></h1>

                <h3 class="font-extra-bold no-margins text-success">
                    Kes.100 Safaricom 
                </h3>
                <small>Safaricom Airtime Totals of 100 Bob.</small>
            </div>
            <div class="panel-footer">
                Campaign Airtime
            </div>
        </div>
    </div>
 
</div>
