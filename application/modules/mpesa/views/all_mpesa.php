<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Created</th>
						<th>Name</th>
						<th>Contact</th>
						<th>Status</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$mpesa_contact_id = $row->mpesa_contact_id;
				$mpesa_contact_name = $row->mpesa_contact_name;
				$created = $row->created;
				$mpesa_contact_status = $row->mpesa_contact_status;
				$mpesa_contact_phone = $row->mpesa_contact_phone;
				//status
				if($mpesa_contact_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				$sms_id = $mpesa_contact_id;
				
				
				$button = '<a class="btn btn-sm btn-default" data-toggle="modal" data-target="#disburse_mpesa'.$sms_id.'" onclick="get_finders('.$sms_id.')"><i class="fa fa-money"></i> Disburse</a>
						<div class="modal fade" id="disburse_mpesa'.$sms_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header modal-success">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Mpesa Disbursement </h4>

									</div>
									'. form_open_multipart('disburse-financial/'.$sms_id, array("class" => "form-horizontal", "role" => "form")).'
									<div class="modal-body">
										<div class="row">
						                    <div class="col-md-12">
					                            <div class="col-lg-6 col-md-6 col-sm-6">
								                   <div class="form-group">
												        <label class="col-lg-5 control-label">Mpesa Amount: </label>
												        
												        <div class="col-lg-7">
												            <input type="text" class="form-control" name="amount" placeholder="Amount">
												        </div>
												    </div>
								                  
								                </div>
								                <div class="col-lg-6 col-md-6 col-sm-6">
								                 
								                </div>
						                    </div>
					                    </div>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-sm btn-success" >Disbuse Amount</button>
										<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
									</div>
									'.form_close().'
								</div>
							</div>
						</div>

						';
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$created.'</td>
						<td>'.$mpesa_contact_name.'</td>
						<td>'.$mpesa_contact_phone.'</td>
						<td>'.$status.'</td>
						<td>'.$button.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Mpesa Contacts";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>human-resource/export-personnel" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <div class="col-lg-12">
                                    	<a href="<?php echo site_url();?>mpesa/add-contact" class="btn btn-sm btn-info pull-right">Add Mpesa Contact</a>
                                    </div>
                                </div>
				
				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>
