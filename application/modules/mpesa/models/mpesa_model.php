<?php

class Mpesa_model extends CI_Model 
{	
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_mpesa_contacts($table, $where, $per_page, $page, $order = 'mpesa_contact_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_airtime_quantities($table, $where, $per_page, $page, $order = 'airtime_value', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('SUM(airtime_value)');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_ten_quantities($table, $where, $per_page, $page, $order = 'airtime_value', $order_method = 'ASC'){
		$batch_ten='0';
		$this->db->select('SUM(airtime_value) as ten');
		$this->db->where($where);
		$query = $this->db->get('airtime');
		$total = $query->row();
		$batch_ten = $total->ten;

		return $batch_ten;
	}
	public function get_all_twenty_quantities($table, $where, $per_page, $page, $order = 'airtime_value', $order_method = 'ASC'){
		$batch_ten='0';
		$this->db->select('SUM(airtime_value) as ten');
		$this->db->where($where);
		$query = $this->db->get('airtime');
		$total = $query->row();
		$batch_ten = $total->ten;

		return $batch_ten;
	}
	public function get_all_fifty_quantities($table, $where, $per_page, $page, $order = 'airtime_value', $order_method = 'ASC'){
		$batch_ten='0';
		$this->db->select('SUM(airtime_value) as ten');
		$this->db->where($where);
		$query = $this->db->get('airtime');
		$total = $query->row();
		$batch_ten = $total->ten;

		return $batch_ten;
	}
	public function get_all_hundred_quantities($table, $where, $per_page, $page, $order = 'airtime_value', $order_method = 'ASC'){
		$batch_ten='0';
		$this->db->select('SUM(airtime_value) as ten');
		$this->db->where($where);
		$query = $this->db->get('airtime');
		$total = $query->row();
		$batch_ten = $total->ten;

		return $batch_ten;
	}
	public function get_items_list($table, $where, $order,$join = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');
		
		return $query;
	}


	
	
	
	public function add_contact()
	{
		
		$data = array
		(
			'mpesa_contact_name'=>$this->input->post('contact_name'),
			'mpesa_contact_phone'=>$this->input->post('contact_phone'),
			'mpesa_contact_status'=>1


			#'created_by'=>$this->session->userdata('personnel_id'),
			#'modified_by'=>$this->session->userdata('personnel_id')
		);
			
		if($this->db->insert('mpesa_contact', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function add_airtime()
	{
		
		$data = array
		(
			'airtime_value'=>$this->input->post('airtime_value'),
			'airtime_code'=>$this->input->post('airtime_code'),
			'airtime_status'=>0


			#'created_by'=>$this->session->userdata('personnel_id'),
			#'modified_by'=>$this->session->userdata('personnel_id')
		);
			
		if($this->db->insert('airtime', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_phone_contact($mpesa_contact_id){
		$total_batch='0';
		$this->db->select('mpesa_contact_phone');
		$this->db->where('mpesa_contact_status = 1 AND mpesa_contact_id = '.$mpesa_contact_id.'');
		$query = $this->db->get('mpesa_contact');
		$total = $query->row();
		$total_batch = $total->mpesa_contact_phone;

		return $total_batch;
	}
	public function clean_phone_number($phone_number)
	{
		//remove forward slash
		$numbers = explode("/",$phone_number);
		$phone_number = $numbers[0];

		//remove hyphens
		$phone_number = str_replace("-","",$phone_number);

		//remove spaces
		$phone_number = str_replace(" ","",$phone_number);

		if (substr($phone_number, 0, 1) === '0') 
		{
			$phone_number = ltrim($phone_number, '0');
		}
		
		if (substr($phone_number, 0, 3) === '254') 
		{
			$phone_number = ltrim($phone_number, '254');
		}
		
	
		return $phone_number;
	}
	public function disburse_mpesa($mpesa_contact_id)
	{
		$phone = $this->get_phone_contact($mpesa_contact_id);
		$phone_number = $this->clean_phone_number($phone);
		//var_dump($phone_number);die();

		$fields_ser = array
		(
			'api_key' => urlencode("1000"),
			'phone_number' => urlencode($phone_number),
			'transaction_id' => urlencode($mpesa_contact_id),
			'amount' => urlencode("10")
		);

		$base_url = 'https://www.omnis.co.ke/omnis_gateway/';
		$service_url = $base_url.'disburse-payment';
		$response2 = $this->rest_service($service_url, $fields_ser);
		$message = json_decode($response2);
		$message = json_decode($message);
		
		//var_dump($message);die();
		
		if($message->result == 0)
		{
			$data_insert = array(
					'mpesa_contact_id' => $mpesa_contact_id,
					'mpesa_amount' => 10,
					'date_transacted' => date('Y-m-d'),
				);
			$this->db->insert('mpesa_disbursement', $data_insert);
			
			return TRUE;
			
		}
		else
		{
			return FALSE;

		}
		
	}
	public function rest_service($service_url, $fields)
	{
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');
		
		// a. initialize
		try{
			$ch = curl_init();
			
			// b. set the options, including the url
			curl_setopt($ch, CURLOPT_URL, $service_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, count($fields));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			
			// c. execute and fetch the resulting HTML output
			$output = curl_exec($ch);
			
			//in the case of an error save it to the database
			if ($output === FALSE) 
			{
				$return['result'] = 0;
				$return['message'] = curl_error($ch);
				
				$return = json_encode($response);
			}
			
			else
			{
				$return = $output;
				$return = json_encode($output);
			}
		}
		
		//in the case of an exceptions save them to the database
		catch(Exception $e)
		{
			$response['result'] = 0;
			$response['message'] = $e->getMessage();
			
			$return = json_encode($response);
		}
		
		return $return;
	}
	public function get_all_counties_table($table, $where, $per_page, $page, $order = 'county_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_county()
	{
		$data = array(
				'location_name'=>$this->input->post('county_name'),
				'location_status'=>1

				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('location', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		
		}
	}
	public function deactivate_county($county_id)
	{
		$data = array(
				'location_status' => 0
			);
		$this->db->where('location_id', $county_id);
		
		if($this->db->update('location', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_all_keywords_table($table, $where, $per_page, $page, $order = 'keyword_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_keyword()
	{
		$data = array(
				'keyword_name'=>$this->input->post('keyword_name'),
				'keyword_type'=>0,
				'keyword_status'=>1

				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('keyword', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		
		}
	}
	
	
}
?>