<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpesa extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('mpesa_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index() 
	{
		
		$where = 'mpesa_contact.mpesa_contact_delete = 0';
		$table = 'mpesa_contact';


		$order = 'created';
		$order_method = 'ASC';
		
		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'mpesa';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_all_mpesa_contacts($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Mpesa Contact\'s';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;

		


		$data['content'] = $this->load->view('all_mpesa', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function airtime_index() 
	{
		
		$where = 'airtime_status = 0';
		$where_ten = 'airtime.airtime_status = 0 and airtime_value = 10 ';
		$where_twenty = 'airtime.airtime_status = 0 and airtime_value = 20 ';
		$where_fifty = 'airtime.airtime_status = 0 and airtime_value = 50 ';
		$where_hundred = 'airtime.airtime_status = 0 and airtime_value = 100 ';
		$table = 'airtime';


		$order = 'airtime_value';
		$order_method = 'ASC';
		
		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'airtime';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();


		$query_ten = $this->mpesa_model->get_all_ten_quantities($table, $where_ten, $config["per_page"], $page, $order, $order_method);
		$query_twenty = $this->mpesa_model->get_all_twenty_quantities($table, $where_twenty, $config["per_page"], $page, $order, $order_method);
		$query_fifty = $this->mpesa_model->get_all_fifty_quantities($table, $where_fifty, $config["per_page"], $page, $order, $order_method);
		$query_hundred = $this->mpesa_model->get_all_hundred_quantities($table, $where_hundred, $config["per_page"], $page, $order, $order_method);
		
		
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Airtime Batch Total\'s';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;

		$v_data['query_ten'] = $query_ten;
		$v_data['query_twenty'] = $query_twenty;
		$v_data['query_fifty'] = $query_fifty;
		$v_data['query_hundred'] = $query_hundred;

		
	
		$v_data['page'] = $page;

		


		$data['content'] = $this->load->view('all_airtime', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function add_contact() 
	{
		
		//form validation rules
		$this->form_validation->set_rules('contact_name', 'Name', 'xss_clean');
		$this->form_validation->set_rules('contact_phone', 'Phone', 'required|xss_clean');
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->mpesa_model->add_contact())
			{
				$this->session->set_userdata('success_message', 'Contact added successfully');
				redirect('mpesa');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Contact. Please try again');
			}
		}
		
		//open the add new Customers
		$data['title'] = 'Add Mpesa Contact';
		$v_data['title'] = $data['title'];
	
		$data['content'] = $this->load->view('mpesa/add_mpesa_contact', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_airtime() 
	{
		
		//form validation rules
		$this->form_validation->set_rules('airtime_code', 'Code', 'required|xss_clean');
		$this->form_validation->set_rules('airtime_value', 'Value', 'required|xss_clean');
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->mpesa_model->add_airtime())
			{
				$this->session->set_userdata('success_message', 'Airtime code added successfully');
				redirect('airtime');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Airtime. Please try again');
			}
		}
		
		//open the add new Customers
		$data['title'] = 'Add Airtime';
		$v_data['title'] = $data['title'];
	
		$data['content'] = $this->load->view('mpesa/add_airtime', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function disburse_mpesa($mpesa_contact_id)
	{
		if (!empty($mpesa_contact_id))
		{
			if($this->mpesa_model->disburse_mpesa($mpesa_contact_id))
			{
				$this->session->set_userdata('success_message', 'Successfully Disbursed the amount');
				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not disburse amount. Please try again');
			}
		}

		redirect('mpesa');
	}

	public function allocate_sms($sms_id)
	{
		if (!empty($sms_id))
		{
			if($this->sms_model->add_sms_allocation($sms_id))
			{
				$this->session->set_userdata('success_message', 'Successfully allocated the sms');
				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not allocate sms. Please try again');
			}
		}

		redirect('sms');
	}
	public function county_index($order = 'location_name', $order_method = 'ASC') 
	{

		$where = 'location.location_status > 0 ';
		$table = ' location';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_all_counties_table($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Counties';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('mpesa/all_counties', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Add a new project
	*
	*/
	public function add_county(){
		
		//form validation rules
		$this->form_validation->set_rules('county_name', 'Location Name', 'required|xss_clean');
		

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->mpesa_model->add_county()){
				$this->session->set_userdata('success_message', 'Location added successfully');
				redirect('settings/county-locations');
			}
			
			else{
				$this->session->set_userdata('error_message', 'Could not add Location. Please try again');
			}
		}
		
	
		
		$data['title'] = 'Add Location';
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('mpesa/add_location', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function delete_county($county_id)
	{
		if($this->mpesa_model->deactivate_county($county_id))
		{
			$this->session->set_userdata('success_message', 'County Deleted successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'County could not be Deleted. Please try again');
		}
		redirect('settings/county-locations');
	}
	public function keyword_index($order = 'keyword_name', $order_method = 'ASC') 
	{

		$where = 'keyword_status > 0 ';
		$table = ' keyword';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_all_keywords_table($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Keywords';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('mpesa/all_keyword', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}



	public function add_keyword(){
		
		//form validation rules
		$this->form_validation->set_rules('keyword_name', 'Name', 'required|xss_clean');
	

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->mpesa_model->add_keyword()){
				$this->session->set_userdata('success_message', 'Keyword added successfully');
				redirect('settings/key-words');
			}
			
			else{
				$this->session->set_userdata('error_message', 'Could not add Keyword. Please try again');
			}
		}
		
	
		
		$data['title'] = 'Add Key Word';
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('mpesa/add_keyword', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function demote_keyword($keyword_id)
	{
		if($this->mpesa_model->demote_keyword($keyword_id))
		{
			$this->session->set_userdata('success_message', 'Keyword changed successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Keyword could not be Changed. Please try again');
		}
		redirect('settings/key-words');
	}

}
?>